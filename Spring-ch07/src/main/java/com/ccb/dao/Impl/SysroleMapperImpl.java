package com.ccb.dao.Impl;

import com.ccb.dao.SysroleMapper;
import com.ccb.pojo.Sysrole;
import org.mybatis.spring.support.SqlSessionDaoSupport;

import java.util.List;

/**
 * @author CCB
 * date: 27/9/2022 下午4:49
 * Description: 描述
 */
public class SysroleMapperImpl  extends SqlSessionDaoSupport implements SysroleMapper {
    @Override
    public List<Sysrole> selectSysroleList(Sysrole sysrole) {
        return null;
    }

    @Override
    public boolean addSysrole(Sysrole sysrole) {
        return false;
    }
}
