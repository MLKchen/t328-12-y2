import com.ccb.entity.Dialogue;
import com.ccb.entity.TestEntity;
import com.taskPlus.entity.Equip;
import com.taskPlus.entity.Player;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author CCB
 * date: 24/9/2022 下午3:04
 * Description: 实战任务
 */
public class Task {

    @Test//实战任务1-2
    public void Tast01(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Dialogue dialogue = (Dialogue) context.getBean("Dialogue");
        dialogue.Say();
    }

    @Test
    public void Task02(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Player player = (Player) context.getBean("zhangsan");

        Equip equip = new Equip();
        equip.setName("紫色梦幻"+player.getRing().getName());
        equip.setAttackPlus(player.getRing().getAttackPlus() + 6);
        equip.setSpeedPlus(player.getRing().getSpeedPlus());
        equip.setDefencePlus(player.getRing().getDefencePlus() + 6);
        equip.setType(player.getRing().getType());

        System.out.println(player.getRing().getName());
        player.updateEquip(equip);
    }

}
