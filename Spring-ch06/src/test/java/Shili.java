import com.ccb.dao.impl.UserDaoImpl;
import com.ccb.entity.ErrorLogger;
import com.ccb.entity.TestEntity;
import com.ccb.entity.User;
import com.ccb.service.UserService;
import com.ccb.service.impl.UserServiceImpl;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

/**
 * @author CCB
 * date: 23/9/2022 下午2:42
 * Description: 描述
 */
public class Shili {

    @Test//测试示例2
    public void Test01(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserService userService = (UserService) context.getBean("UserService");
        User user = new User();
        userService.sava(user);
    }

    @Test
    public void Test02(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        TestEntity testEntity = (TestEntity) context.getBean("entity");
        System.out.println(
                testEntity.getSpecialCharacter1()+"\n"+testEntity.getSpecialCharacter2()+"\n"+
                testEntity.getUser()+"\n"+testEntity.getInnerBean()+"\n"+testEntity.getList()+"\n"+
                testEntity.getArray()+"\n"+testEntity.getSet()+"\n"+testEntity.getMap()+"\n"+
                testEntity.getPros()+"\n"+testEntity.getEmptyValue()+"\n"+testEntity.getNullValue()+"\n"
        );
    }
}