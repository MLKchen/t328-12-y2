package com.ccb.entity;

/**
 * @author CCB
 * date: 24/9/2022 下午2:49
 * Description: 描述
 */
public class Dialogue {
    private String zhugeliang;      //诸葛亮
    private String lufei;           //路飞

    public void Say(){
        System.out.println("路飞："+this.getLufei()+"\n诸葛亮："+this.getZhugeliang());
    }

    public Dialogue() {
    }

    public Dialogue(String zhugeliang, String lufei) {
        this.zhugeliang = zhugeliang;
        this.lufei = lufei;
    }

    public String getZhugeliang() {
        return zhugeliang;
    }

    public void setZhugeliang(String zhugeliang) {
        this.zhugeliang = zhugeliang;
    }

    public String getLufei() {
        return lufei;
    }

    public void setLufei(String lufei) {
        this.lufei = lufei;
    }
}
