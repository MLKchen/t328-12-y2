package com.ccb.entity;

/**
 * @author CCB
 * date: 20/9/2022 上午10:02
 * Description: 描述
 */
public class User {
    private String UserName;
    private String UserNo;
    private String age;
    private String evalluate;

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserNo() {
        return UserNo;
    }

    public void setUserNo(String userNo) {
        UserNo = userNo;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getEvalluate() {
        return evalluate;
    }

    public void setEvalluate(String evalluate) {
        this.evalluate = evalluate;
    }
}
