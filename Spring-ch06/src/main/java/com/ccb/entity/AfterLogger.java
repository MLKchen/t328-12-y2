package com.ccb.entity;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Around;

/**
 * @author CCB
 * date: 23/9/2022 下午3:04
 * Description: 最终增强
 */
public class AfterLogger {
    private static final Logger log = Logger.getLogger(AfterLogger.class);
    @Around("execution(* service.Uservice.*(..))")
    public void afterLogger(JoinPoint jp){
        log.info(jp.getSignature().getName()+"方法结束执行");
    }
}