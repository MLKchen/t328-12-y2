package com.ccb.entity;

import org.apache.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;

import java.util.Arrays;

/**
 * @author CCB
 * date: 23/9/2022 下午4:45
 * Description: 描述
 */
public class AroundLogger {
    private static final Logger log = Logger.getLogger(AfterLogger.class);

    @After("execution(* service.Uservice.*(..))")
    public Object aroundLogger(ProceedingJoinPoint jp)throws Throwable{
        System.out.println("环绕开始");
        log.info("调用"+jp.getTarget()+"的"+jp.getSignature().getName()+"方法。方法入参："+ Arrays.toString(jp.getArgs()));
        try {
            Object result = jp.proceed();
            log.info("调用"+jp.getTarget()+"的"+jp.getSignature().getName()+"方法。返回值：" +result);
            return result;
        }catch (Throwable throwable){
            log.info(jp.getSignature().getName()+"方法返回异常"+throwable);
            throw  throwable;
        }finally {
            log.info(jp.getSignature().getName()+"方法执行结束");
            System.out.println("环绕结束");
        }
    }
}
