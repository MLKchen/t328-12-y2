package com.ccb.entity;


import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;

/**
 * @author CCB
 * date: 23/9/2022 下午2:31
 * Description: 定义包含增强方法的JavaBean
 */
public class ErrorLogger {
    private static final Logger log = Logger.getLogger(ErrorLogger.class);
    @AfterThrowing(pointcut = "execution(* service.Uservice.*(..))",throwing = "e")
    public void afterThrowing(JoinPoint jp, RuntimeException e){
        log.error(jp.getSignature().getName() + "方法发生异常："+e);
    }
}
