package com.ccb.dao.impl;

import com.ccb.dao.UserDao;
import com.ccb.entity.User;
import org.springframework.stereotype.Component;

/**
 * @author CCB
 * date: 20/9/2022 上午10:03
 * Description: 用户模块数据层实现
 */
@Component("UserDaoImpl")
public class UserDaoImpl implements UserDao  {

    @Override
    public void sava(User user) {
        System.out.println("保存用户信息到数据库");
    }
}
