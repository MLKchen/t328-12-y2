package com.ccb.service.impl;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

import java.util.Arrays;

/**
 * @author CCB
 * date: 20/9/2022 下午3:16
 * Description: 提取公共日志代码
 */
@Aspect
public class UserServiceLogger {
    private static final Logger log = Logger.getLogger(UserServiceLogger.class);

    @Pointcut("execution(* com.ccb.service.impl.UserServiceImpl.sava(..))")
    public void pointcut(){}

    /**
     * 前置代码
     * @param jp
     */
    @Before("pointcut()")
    public void before(JoinPoint jp){
        log.info("调用"+jp.getTarget()+"的"+jp.getSignature().getName()+"方法.方法入参："+ Arrays.toString(jp.getArgs()));
    }

    @AfterReturning(pointcut = "pointcut()",returning = "result")
    public void afterReturning(JoinPoint jp,Object result){
        log.info("调用"+jp.getTarget()+"的"+jp.getSignature().getName()+"方法.方法返回值："+ result);
    }
}
