package com.ccb.service.impl;

import com.ccb.dao.UserDao;
import com.ccb.dao.impl.UserDaoImpl;
import com.ccb.entity.User;
import com.ccb.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author CCB
 * date: 20/9/2022 上午10:03
 * Description: 用户模块数据层实现
 */
@Service("UserService")
public class UserServiceImpl implements UserService {
    //通过工厂获取所依赖的UserDao对象
    @Resource(name = "UserDaoImpl")
    private UserDao dao;

    public UserServiceImpl() {
    }

    public UserServiceImpl(UserDao dao) {
        this.dao = dao;
    }

    @Override
    public void sava(User user) {
        //调用UserDao的方法保存用户信息
        dao.sava(user);
    }

    public void setDao(UserDao dao) {
        this.dao = dao;
    }


}
