package com.ccb.service.impl;

import com.taskPlus.entity.Equip;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

/**
 * @author CCB
 * date: 22/9/2022 下午2:42
 * Description: 升级判断的前置
 */
@Aspect
public class Upgrade {

    @Around("execution(* com.taskPlus.entity.Player.updateEquip(com.taskPlus.entity.Equip))")
    public Object around(ProceedingJoinPoint jp){
        Object result = null;
        try {
            Equip equip = (Equip) jp.getArgs()[0];
            if (equip.getType().equals("指环")){
                result = jp.proceed();
            }else {
                System.out.println("您提交的不是指环！");
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return result;
    }
}
