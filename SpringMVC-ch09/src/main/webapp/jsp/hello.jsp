<%--
  Created by IntelliJ IDEA.
  User: MLK
  Date: 9/10/2022
  Time: 下午1:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>

<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>我的的一个SpringMVC程序，运行成功啦</h1>
<h2>realName(key:realName)-->${realName}</h2>
<h2>realName(key:String)-->${string}</h2>
<h2>realName(key:currentUser)-->${currentUser.username}</h2>
<h2>realName(key:String)-->${sysUser.username}</h2>

<form action="${pageContext.request.contextPath}/hello/show">
    请输入account:<input name="name" type="text">
    <input type="submit">
</form>
</body>
</html>
