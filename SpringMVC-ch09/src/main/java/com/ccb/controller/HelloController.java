package com.ccb.controller;

import com.ccb.pojo.SysUser;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

/**
 * @author CCB
 * date: 8/10/2022 下午7:01
 * Description: 描述
 */
@Controller
@RequestMapping(value = "/hello")
public class HelloController{
    Logger logger = Logger.getLogger(HelloController.class);
//    @Override
//    protected ModelAndView handleRequestInternal(HttpServletRequest httpServletRequest,
//                                                 HttpServletResponse httpServletResponse) throws Exception {
//        System.out.println("Spring MVC框架成功。");
//        return null;
//    }
    /**
    * @author CCB
    * date: 2022-10-09 下午2:24
    * Description: 这里是入参的示例代码
    */
//    @RequestMapping(value = "/hello")
//    public String hello(@RequestParam(value = "username",defaultValue = "MLK",required = false) String realName) throws Exception{
//        logger.info("你好"+realName+"欢迎来到Spring MVC课堂");
//        return "hello";
//    }

    @RequestMapping(value = "/hello")
    public ModelAndView hello(@RequestParam String realName) throws Exception{
        logger.info("你好"+realName+"欢迎来到Spring MVC课堂");
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("realName",realName);
        modelAndView.setViewName("hello");
        return modelAndView;
    }

    @RequestMapping(value = "/hello1")
    public String hello1() throws Exception{
        logger.info("Spring搭建成功");
        return "hello";
    }
    @RequestMapping(value = "/hello2")
    public String hello2(Model model,@RequestParam String realName) throws Exception{
        logger.info("你好"+realName+"欢迎来到Spring MVC课堂");
        model.addAttribute("realName",realName);
        model.addAttribute(realName);

        SysUser sysUser = new SysUser();
        sysUser.setUsername(realName);
        model.addAttribute("currentUser",sysUser);
        model.addAttribute(sysUser);
        return "hello";
    }

    @RequestMapping(value = "/hello3")
    public String hello3(Map<String,Object> map,@RequestParam String realName){
        logger.info("你好"+realName+"欢迎来到Spring MVC课堂");
        map.put("realName",realName);
        return "hello";
    }

    @RequestMapping(value = "/Task")
    public String Task(){
        return "hello";
    }

    @RequestMapping(value = "/show")
    public String show(Model model,@RequestParam String name){
        model.addAttribute("name",name);
        return "show";
    }
}
