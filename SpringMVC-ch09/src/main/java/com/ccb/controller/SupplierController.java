package com.ccb.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author CCB
 * date: 9/10/2022 下午2:10
 * Description: 描述
 */
@Controller
@RequestMapping(value = "/supplier")
public class SupplierController {
    Logger logger = Logger.getLogger(HelloController.class);
    //程序供货商
    @RequestMapping(value = "/view")
    public String view(){
        logger.info("程序供货商详情");
        return null;
    }
}
