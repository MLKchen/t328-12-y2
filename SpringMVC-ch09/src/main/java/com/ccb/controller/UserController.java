package com.ccb.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author CCB
 * date: 9/10/2022 下午2:12
 * Description: 描述
 */
@Controller
@RequestMapping(value = "/user")
public class UserController {
    Logger logger = Logger.getLogger(HelloController.class);

    /**
     * 查询用户详情
     * @return
     */
//    @RequestMapping(value = "/view",method = RequestMethod.GET)
    @RequestMapping(value = "/view")
    public String view(){
        logger.info("查询用户详情");
        return null;
    }

//    @RequestMapping(value = "/save",method = RequestMethod.POST)
    @RequestMapping(value = "/save")
    public String save(){
        logger.info("保存用户信息");
        return null;
    }
}
