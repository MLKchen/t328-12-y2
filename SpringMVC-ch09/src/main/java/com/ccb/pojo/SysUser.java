package com.ccb.pojo;

/**
 * @author CCB
 * date: 9/10/2022 下午2:56
 * Description: 用户
 */
public class SysUser {
    private Integer id;
    private String username;
    private String phone;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
