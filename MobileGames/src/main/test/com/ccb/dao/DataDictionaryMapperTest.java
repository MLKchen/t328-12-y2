package com.ccb.dao;

import com.ccb.pojo.DataDictionary;
import com.ccb.service.DataDictionaryService;
import com.ccb.service.DevUserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

import static org.junit.Assert.*;

public class DataDictionaryMapperTest {
    ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
    DevUserService devUserService = (DevUserService) ctx.getBean("DevUserService");
    DataDictionaryService dataDictionaryService = (DataDictionaryService) ctx.getBean("DataDictionaryService");

    @Test
    public void getTest() {
        DataDictionary dictionary = dataDictionaryService.getValueNameByAppid(58);
        System.out.println(dictionary.getValueId());
    }

}