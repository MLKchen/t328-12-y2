package com.ccb.dao;

import com.ccb.pojo.AppInfo;
import com.ccb.pojo.DevUser;
import com.ccb.service.AppInfoService;
import com.ccb.service.BackendUserService;
import com.ccb.service.DevUserService;
import com.ccb.utils.Constants;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

import static org.junit.Assert.*;

public class AppInfoMapperTest {
    ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
    AppInfoService appInfoService = (AppInfoService) ctx.getBean("AppInfoService");
    DevUserService devUserService = (DevUserService) ctx.getBean("DevUserService");

    @Test
    public void getAppInfoList() {
        List<AppInfo> appInfoList = devUserService.getAppinfoList(null, null,
            null, null, null,
            null, null, 0, 10);
        System.out.println(appInfoList.get(0).getAPKName());
    }
}