package com.ccb.dao;

import com.ccb.pojo.AppInfo;
import com.ccb.service.DevUserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.ResourceUtils;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

import static org.junit.Assert.*;

public class DevUserMapperTest {
    ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
    DevUserService devUserService = (DevUserService) ctx.getBean("DevUserService");


    @Test
    public void getUser() {
        System.out.println(devUserService.getUser("test001","123456").getDevName());
    }

    @Test
    public void getAppinfoList(){
        List<AppInfo> appInfoList = devUserService.getAppinfoList(null,"3",null,
                null,null,null,null,0,5);
        System.out.println(appInfoList.size());
        for (AppInfo appInfo : appInfoList){
            System.out.println(appInfo.getSoftwareName());
        }
    }

    @Test
    public void getCount(){
        System.out.println(devUserService.getCount(null,null,null,
                null,null,null,null));
    }

    @Test
    public void del(){
        File file = new File("C:\\Users\\Administrator\\IdeaProjects\\t328-12-y2\\MobileGames\\target\\MobileGames\\statics\\uploadfiles\\1666422585784_Personal.png");
//        if(file.exists()){
//            file.delete();
//        }

//        File f = new File(this.getClass().getResource("/").getPath());
//        String fq = String.valueOf(f);
//        String path = fq.substring(0,fq.lastIndexOf("\\"))+"\\statics\\uploadfiles\\1666424897172_Personal.png";
//        System.out.println("C:\\Users\\Administrator\\IdeaProjects\\t328-12-y2\\MobileGames\\target\\MobileGames\\statics\\uploadfiles\\1666424897172_Personal.png");
//        File fu = new File(path);
//        fu.delete();

//        System.out.println(System.getProperty("user.dir"));

        try {
            String paths = ResourceUtils.getURL("classpath:\\statics\\uploadfiles").getPath();
            System.out.println(paths);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }
}