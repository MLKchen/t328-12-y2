package com.ccb.dao;

import com.ccb.pojo.BackendUser;
import com.ccb.service.BackendUserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.*;

public class BackendUserMapperTest {
    ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
    BackendUserService backendUserService = (BackendUserService) ctx.getBean("BackendUserService");

    @Test
    public void getUser() {
        BackendUser backendUser = backendUserService.getUser("111","111");
        System.out.println(backendUser);
    }
}