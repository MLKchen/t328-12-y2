package com.ccb.controller;

import com.ccb.pojo.*;
import com.ccb.service.*;
import com.ccb.utils.Constants;
import com.ccb.utils.PageUtil;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author CCB
 * date: 18/10/2022 下午4:13
 * Description: 管理员控制器
 */
@Controller
@RequestMapping("/manager")
public class AdminController {
    private Logger logger = Logger.getLogger(AdminController.class);
    ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
    BackendUserService backendUserService = (BackendUserService) ctx.getBean("BackendUserService");
    AppInfoService appInfoService = (AppInfoService) ctx.getBean("AppInfoService");
    DevUserService devUserService = (DevUserService) ctx.getBean("DevUserService");
    AppCategoryService appCategoryService = (AppCategoryService) ctx.getBean("AppCategoryService");
    DataDictionaryService dictionaryService = (DataDictionaryService) ctx.getBean("DataDictionaryService");
    AppVersionService getAppVersion = (AppVersionService) ctx.getBean("AppVersionService");

    //跳转至登录页面
    @RequestMapping("/login")
    public String tologin(){
        logger.info("跳转至登录页面");
        return "backendlogin";
    }

    //登录/校验用户名和密码
    @PostMapping("/dologin")
    public String dologin(Model model,HttpSession session, String userCode, String userPassword){
        BackendUser backendUser = backendUserService.getUser(userCode,userPassword);
        logger.info("校验登录用户--账号："+userCode+"密码："+userPassword);
        if(backendUser!=null){
            logger.info(backendUser.getUserName());
            logger.info("验证成功跳转至主页面");
            //将管理员信息放入session
            session.setAttribute(Constants.USER_SESSION,backendUser);
            //跳转至主页面
            return "backend/main";
        }
        logger.info("验证失败，跳转至登录页面");
        model.addAttribute("error","用户名或密码不正确！");
        //跳转至登陆页面
        return "backendlogin";
    }

    @RequestMapping("/backend/app/list")
    public String list(Model model, HttpSession session, String querySoftwareName, String queryCategoryLevel1, String queryCategoryLevel2, String queryCategoryLevel3, String queryStatus, String queryFlatformId, @RequestParam(defaultValue = "1") Integer pageIndex) {
        List<AppCategory> appCategoryList = appCategoryService.getcategoryLevelList("1");
        model.addAttribute("categoryLevel1List", appCategoryList);
        List<DataDictionary> flatFormList = dictionaryService.getList("APP_FLATFORM");
        model.addAttribute("flatFormList", flatFormList);
        List<AppInfo> appInfoList = null;
        try {
            //设置页面容量
            int pageSize = Constants.pageSize;
            //总数量
            int totalCount = devUserService.getCount(querySoftwareName, queryStatus,queryCategoryLevel1, queryCategoryLevel2, queryCategoryLevel3,  queryFlatformId,null);
            //总页数
            PageUtil pages = new PageUtil();
            pages.setNowPage(pageIndex);
            pages.setShowSize(pageSize);
            pages.setCountPage(totalCount);
            int totalPageCount = pages.getTotalPageCount();
            //控制首页和尾页
            if (pageIndex > totalPageCount && totalPageCount != 0) {
                pageIndex = totalPageCount;
                throw new RuntimeException("页码不正确");
            }
            appInfoList = devUserService.getAppinfoList(querySoftwareName,queryStatus,
                    queryCategoryLevel1,
                    queryCategoryLevel2,
                    queryCategoryLevel3,
                    queryFlatformId,null,
                    pages.getOffset(),
                    pages.getShowSize());
            model.addAttribute("appInfoList", appInfoList);
            logger.info(appInfoList);
            //存储条件
            model.addAttribute("querySoftwareName",querySoftwareName);
            model.addAttribute("queryStatus",queryStatus);
            model.addAttribute("queryFlatformId",queryFlatformId);
            session.setAttribute("queryCategoryLevel1",queryCategoryLevel1);
            session.setAttribute("queryCategoryLevel2",queryCategoryLevel2);
            session.setAttribute("queryCategoryLevel3",queryCategoryLevel3);
            //分页存储
            model.addAttribute("pages", pages);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("手游列表接口访问失败");
        }
        return "backend/applist";
    }

    /**
     * 进入审核页面
     * @param model
     * @param aid
     * @param vid
     * @return
     */
    @RequestMapping("/check")
    public String check(Model model,@RequestParam String aid,Integer vid){
        logger.info("aid:"+aid+",vid:"+vid);
        AppInfo appInfo = appInfoService.getAppByid(aid);
        appInfo.setAppInfo(appInfo.getAppInfo().trim());
        AppVersion appVersion = getAppVersion.getNewVersionByid(Integer.valueOf(aid));
        logger.info("进入审核页面,手游基本数据："+appInfo+",版本信息："+appVersion);
        model.addAttribute("appInfo",appInfo);
        model.addAttribute("appVersion",appVersion);
        return "backend/appcheck";
    }

    /**
     * 审核手游状态
     * @param status
     * @return
     */
    @PostMapping("/checksave")
    public String checksave(@RequestParam Integer status, Integer id,HttpSession session){
        Integer mid = ((BackendUser)session.getAttribute(Constants.USER_SESSION)).getId();
        if (appInfoService.ModifyStatus(id,status,mid)==1) {
            return "redirect:/manager/backend/app/list";//重定向至列表
        }
        return "backend/main";
    }

    /**
     * 退出系统
     *
     * @param session
     * @return
     */
    @RequestMapping("/logout")
    public String logout(HttpSession session) {
        session.removeAttribute(Constants.USER_SESSION);
        return "backendlogin";
    }
}
