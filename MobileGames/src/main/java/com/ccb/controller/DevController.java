package com.ccb.controller;

import com.alibaba.fastjson.JSON;
import com.ccb.pojo.*;
import com.ccb.service.*;
import com.ccb.utils.Constants;
import com.ccb.utils.PageUtil;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * @author CCB
 * date: 19/10/2022 上午11:40
 * Description: 描述
 */
@Controller
@RequestMapping("/dev")
public class DevController {
    private Logger logger = Logger.getLogger(AdminController.class);
    ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
    DevUserService devUserService = (DevUserService) ctx.getBean("DevUserService");
    DataDictionaryService dataDictionaryService = (DataDictionaryService) ctx.getBean("DataDictionaryService");
    AppCategoryService appCategoryService = (AppCategoryService) ctx.getBean("AppCategoryService");
    AppInfoService appInfoService = (AppInfoService) ctx.getBean("AppInfoService");
    AppVersionService appVersionService = (AppVersionService) ctx.getBean("AppVersionService");

    //跳转至登录页面
    @RequestMapping("/login")
    public String login() {
        return "devlogin";
    }

    //登录验证
    @RequestMapping("/dologin")
    public String dologin(Model model, HttpSession session, String devCode, String devPassword) {
        DevUser devUser = devUserService.getUser(devCode, devPassword);
//        logger.info("校验登录用户--账号："+devCode+"密码："+devPassword);

        if (devUser != null) {
            logger.info(devUser.getDevName());
            logger.info("验证成功跳转至主页面");
            //将管理员信息放入session
            session.setAttribute(Constants.DEV_USER_SESSION, devUser);
            //跳转至主页面
            return "developer/main";
        }
        logger.info("验证失败，跳转至登录页面");
        model.addAttribute("error", "用户名或密码不正确！");
        //跳转至登陆页面
        return "devlogin";
    }

    //根据条件查询数据
    @RequestMapping("/flatform/app/list")
    public String appList(HttpSession session, Model model, String querySoftwareName, String queryStatus, String queryCategoryLevel1,
                          String queryCategoryLevel2, String queryCategoryLevel3, String queryFlatformId,
                          @RequestParam(defaultValue = "1") String pageIndex) {
        logger.info("进入app信息获取页面");

//        logger.info("softwareName:"+querySoftwareName);
//        logger.info("status:"+queryStatus);
//        logger.info("categoryLevel1:"+queryCategoryLevel1);
//        logger.info("categoryLevel2:"+queryCategoryLevel2);
//        logger.info("categoryLevel3:"+queryCategoryLevel3);
//        logger.info("flatformId:"+queryFlatformId);
//        logger.info("devId:"+devId);
        logger.info("获取状态列表");
        List<DataDictionary> StatusList = dataDictionaryService.getList("APP_STATUS");
        List<DataDictionary> formList = dataDictionaryService.getList("APP_FLATFORM");
        List<AppCategory> categoryLevel1List = appCategoryService.getcategoryLevelList("1");

        int totalCount = devUserService.getCount(querySoftwareName, queryStatus,
                queryCategoryLevel1, queryCategoryLevel2, queryCategoryLevel3,
                queryFlatformId, ((DevUser)session.getAttribute(Constants.DEV_USER_SESSION)).getId());
        logger.info("获取到总条数：" + totalCount);
        int pageSize = Constants.pageSize;//5
        PageUtil page = new PageUtil();
        page.setNowPage(Integer.valueOf(pageIndex));
        page.setShowSize(pageSize);
        page.setCountPage(totalCount);
        logger.info("当前页：" + page.getNowPage());
        logger.info("总页数页：" + page.getTotalPageCount());
        //根据条件获取数据
        List<AppInfo> appInfoList = devUserService.getAppinfoList(querySoftwareName, queryStatus,
                queryCategoryLevel1, queryCategoryLevel2, queryCategoryLevel3,
                queryFlatformId, ((DevUser)session.getAttribute(Constants.DEV_USER_SESSION)).getId(), page.getOffset(), pageSize);
        logger.info("获取到信息条数：" + appInfoList.size());
        model.addAttribute("appInfoList", appInfoList);
        model.addAttribute("pages", page);
        model.addAttribute("statusList", StatusList);
        model.addAttribute("flatFormList", formList);
        model.addAttribute("categoryLevel1List", categoryLevel1List);
//        model.addAttribute("categoryLevel2List", categoryLevel2List);
//        model.addAttribute("categoryLevel3List", categoryLevel3List);

        //存储条件
        model.addAttribute("querySoftwareName", querySoftwareName);
        model.addAttribute("queryStatus", queryStatus);
        model.addAttribute("queryFlatformId", queryFlatformId);

        model.addAttribute("queryCategoryLevel1", queryCategoryLevel1);
        model.addAttribute("queryCategoryLevel2", queryCategoryLevel2);
        model.addAttribute("queryCategoryLevel3", queryCategoryLevel3);
        return "developer/appinfolist";
    }

    //获取分类列表
    @ResponseBody
    @GetMapping(value = "/getcategoryLevelList", produces = {"aplication/json;charset=UTF-8"})
    public Object categoryLevelList(String pid) {
        List<AppCategory> List = appCategoryService.getcategoryLevelList(pid);
        return JSON.toJSONString(List);
    }

    //跳转至添加页面
    @RequestMapping("/flatform/app/appinfoadd")
    public String toAdd() {
        return "/developer/appinfoadd";
    }

    //获取平台列表
    @ResponseBody
    @GetMapping(value = "/flatform", produces = {"aplication/json;charset=UTF-8"})
    public Object flatform(String tcode) {
        List<DataDictionary> formList = dataDictionaryService.getList(tcode);
        return JSON.toJSONString(formList);
    }

    //Ajax检查输入的apk是否重复
    @ResponseBody
    @GetMapping(value = "/checkAPKName", produces = {"aplication/json;charset=UTF-8"})
    public Object checkAPKName(Model model, String APKName) {
        int num = appInfoService.checkAPKName(APKName);
        if (APKName == null || APKName == "") {
            model.addAttribute("APKName", "empty");
        }
        if (num > 0) {
            model.addAttribute("APKName", "exist");
        }
        if (num == 0) {
            model.addAttribute("APKName", "noexist");
        }
        return JSON.toJSONString(model);
    }

    //新增软件信息
    @RequestMapping("/flatform/app/appinfoaddsave")
    public String AddAppInfo(HttpServletRequest request, HttpSession session, AppInfo appInfo, MultipartFile[] attachs) {
        String workPicPath = null;//这是处理后文件所保存的位置
        String errorInfo = null;
        boolean flag = true;
        String logoLocPath = null;
        DevUser devUser = (DevUser) session.getAttribute(Constants.DEV_USER_SESSION);
        appInfo.setCreatedBy(devUser.getId());
        appInfo.setCreationDate(new Date());

        String path = request.getSession().getServletContext().getRealPath("statics" + File.separator + "uploadfiles");
        logger.info("文件存储路径：" + path);
        String fileName = null;
        for (int i = 0; i < attachs.length; i++) {
            MultipartFile attach = attachs[i];

            //判断文件是否为空
            if (!attach.isEmpty()) {
                errorInfo = "uploadFileError";
                String originalFile = attach.getOriginalFilename();//原文件名
                logger.info("原文件名名：" + attach.getName());
                String prefix = FilenameUtils.getExtension(originalFile);//原文件后缀
                logger.info("原文件后缀为：" + prefix);
                int filesize = 15000000;
                logger.info("文件大小：" + attach.getSize());

                if (attach.getSize() > filesize) {//上传大小不得超过500kb
                    request.setAttribute("fileUploadError", "*上传大小不得超过14.5mb");
                    flag = false;
                } else if (prefix.equalsIgnoreCase("jpg")
                        || prefix.equalsIgnoreCase("png")
                        || prefix.equalsIgnoreCase("jpeg")
                        || prefix.equalsIgnoreCase("pneg")) {//上传图片不正确
                    fileName = System.currentTimeMillis() + RandomUtils.nextInt(1000000) + "_Personal." + prefix;
                    logger.info("新文件名称：" + fileName);
                    File targetFIle = new File(path);
                    if (!targetFIle.exists()) {
                        targetFIle.mkdirs();
                    }
                    //保存
                    try {
                        attach.transferTo(new File(targetFIle, fileName));
                    } catch (Exception e) {
                        e.printStackTrace();
                        request.setAttribute(errorInfo, "* 上传失败！");
                        flag = false;
                    }
                    workPicPath = File.separator + "statics" + File.separator + "uploadfiles" + File.separator + fileName;

                    logger.info("workPicPath:" + workPicPath);
                } else {
                    request.setAttribute(errorInfo, "* 上传图片格式不正确");
                }
            }
        }
        logoLocPath = path+"\\"+fileName;
        appInfo.setLogoPicPath(workPicPath);
        appInfo.setLogoLocPath(logoLocPath);
        appInfo.setCreationDate(new Date());
        appInfo.setCreatedBy(((DevUser)session.getAttribute(Constants.DEV_USER_SESSION)).getId());
        appInfo.setDevId(((DevUser)session.getAttribute(Constants.DEV_USER_SESSION)).getId());
        if (appInfoService.addsoftware(appInfo) > 0) {
            logger.info("添加成功");
            return "redirect:/dev/flatform/app/list";
        }
        return "";
    }

    //进入软件详情
    @RequestMapping("/flatform/app/appview/{appinfoid}")
    public String view(Model model, @PathVariable String appinfoid) {
        List<AppInfo> appInfoList = appInfoService.getVersionById(Integer.valueOf(appinfoid));
        AppInfo appInfo = appInfoService.getAppByid(appinfoid);
        model.addAttribute("appInfo", appInfo);
        model.addAttribute("appVersionList", appInfoList);
        return "developer/appinfoview";
    }

    //跳转至软件版本添加
    @RequestMapping("/flatform/app/appversionadd")
    public String toVersionAdd(Model model, String id) {
        List<AppInfo> appInfoList = appInfoService.getVersionById(Integer.valueOf(id));
        AppInfo appInfo = appInfoService.getAppByid(id);
        model.addAttribute("appVersion", appInfo);
        model.addAttribute("appVersionList", appInfoList);
        return "developer/appversionadd";
    }

    //新增版本信息
    @RequestMapping("/flatform/app/addversionsave")
    public String addVersionSave(AppVersion appVersion, HttpSession session, HttpServletRequest request,
                                 @RequestParam(value = "a_downloadLink", required = false) MultipartFile attach) {
        String downloadLink = null;
        String apkLocPath = null;
        String apkFileName = null;
        String logoLocPath = null;

        String path = request.getSession().getServletContext().getRealPath("statics" + File.separator + "uploadfiles");
        logger.info("uploadFile path:" + path);
        String oldFileName = attach.getOriginalFilename();//原文件名
        String prefix = FilenameUtils.getExtension(oldFileName);//原文件后缀

        if (prefix.equalsIgnoreCase("apk")) {//apk命名：apk名称+版本号+.apk
            String apkName = null;
            try {
                apkName = appInfoService.getAppByid(String.valueOf(appVersion.getId())).getAPKName();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (apkName == null || "".equals(apkName)) {
                return "redirect:/dev/flatform/app/appversionadd?id=" + appVersion.getId() + "&error=error1";
            }
            apkFileName = apkName + "-" + appVersion.getVersionNo() + ".apk";
            File targetFile = new File(path, apkFileName);
            if (!targetFile.exists()) {
                targetFile.mkdirs();
            }
            try {
                attach.transferTo(targetFile);
            } catch (IOException e) {
                e.printStackTrace();
                return "redirect:/dev/flatform/app/appversionadd?id=" + appVersion.getId() + "&error=error2";
            }

            downloadLink = request.getContextPath() + "/statics/uploadfiles/" + apkFileName;
            apkLocPath = path + File.separator + apkFileName;
            appVersion.setDownloadLink(downloadLink);
            appVersion.setApkLocPath(apkLocPath);
            appVersion.setAppId(appVersion.getId());
            appVersion.setApkFileName(apkFileName);
            DevUser devUser = (DevUser) session.getAttribute(Constants.DEV_USER_SESSION);
            appVersion.setCreatedBy(devUser.getId());
            appVersion.setCreationDate(new Date());
        } else {
            return "redirect:/dev/flatform/app/appversionadd?id=" + appVersion.getAppId() + "&error=error3";
        }
        //新增版本信息至版本信息表
        appVersionService.addVersion(appVersion);
        //获取新增版本的版本号
        AppVersion Version = appVersionService.getNewVersionByid(appVersion.getAppId());
        //新增信息覆盖原来信息
        appInfoService.updateVersionIdById(appVersion.getId(), String.valueOf(Version.getId()), appVersion.getVersionSize());
        //测试代码
        logger.info("版本号：" + appVersion.getVersionNo());
        logger.info("版本大小：" + appVersion.getVersionSize());
        logger.info("发布状态：" + appVersion.getPublishStatus());
        logger.info("版本简介：" + appVersion.getVersionInfo());
        logger.info("版本存放路径：" + appVersion.getApkLocPath());
        logger.info("版本下载链接：" + appVersion.getDownloadLink());
        return "redirect:/dev/flatform/app/list";
    }

    //跳转至修改手游基本信息
    @RequestMapping("/flatform/app/appinfomodify")
    public String toModify(Model model, String id) {
        AppInfo appInfo = appInfoService.getAppByid(id);
        model.addAttribute("appInfo", appInfo);
        return "developer/appinfomodify";
    }

    //同步修改页面的平台列表
    @ResponseBody
    @GetMapping(value = "/getPlatform", produces = {"aplication/json;charset=UTF-8"})
    public Object getPlatform(String tcode) {
        List<DataDictionary> dictionaryList = dataDictionaryService.getList(tcode);
        return JSON.toJSONString(dictionaryList);
    }

    //删除图片
    @ResponseBody
    @GetMapping("/delimg")
    public Object delimg(Model model, String id) throws Exception {
        String LogoPicPath = appInfoService.getAppByid(id).getLogoPicPath();
        System.out.println(LogoPicPath);
        File file = new File(LogoPicPath);
        if (file.exists()) {
            file.delete();
        }

        if (appInfoService.delimg(Integer.valueOf(id)) > 0) {
            model.addAttribute("result", "success");
        } else {
            model.addAttribute("result", "failed");
        }
        return JSON.toJSONString(model);
    }

    //修改软件基本信息
    //修改软件基本信息
    @RequestMapping("/appinfomodifysave")
    public String modify(HttpSession session, HttpServletRequest request, AppInfo appInfo, MultipartFile attach) {
        String workPicPath = null;//这是处理后文件所保存的位置
        String errorInfo = null;
        boolean flag = true;

        String path = request.getSession().getServletContext().getRealPath("statics" + File.separator + "uploadfiles");
        logger.info("文件存储路径：" + path);
        //判断文件是否为空
        if (!attach.isEmpty()) {
            errorInfo = "uploadFileError";
            String originalFile = attach.getOriginalFilename();//原文件名
            logger.info("原文件名名：" + attach.getName());
            String prefix = FilenameUtils.getExtension(originalFile);//原文件后缀
            logger.info("原文件后缀为：" + prefix);
            int filesize = 15000000;
            logger.info("文件大小：" + attach.getSize());

            if (attach.getSize() > filesize) {//上传大小不得超过500kb
                request.setAttribute("fileUploadError", "*上传大小不得超过14.5mb");
                flag = false;
            } else if (prefix.equalsIgnoreCase("jpg")
                    || prefix.equalsIgnoreCase("png")
                    || prefix.equalsIgnoreCase("jpeg")
                    || prefix.equalsIgnoreCase("pneg")) {//上传图片不正确
                String fileName = System.currentTimeMillis() + RandomUtils.nextInt(1000000) + "_Personal." + prefix;
                logger.info("新文件名称：" + fileName);
                File targetFIle = new File(path);
                if (!targetFIle.exists()) {
                    targetFIle.mkdirs();
                }
                //保存
                try {
                    attach.transferTo(new File(targetFIle, fileName));
                } catch (Exception e) {
                    e.printStackTrace();
                    request.setAttribute(errorInfo, "* 上传失败！");
                    flag = false;
                }
                workPicPath = File.separator + "statics" + File.separator + "uploadfiles" + File.separator + fileName;
                logger.info("workPicPath:" + workPicPath);
            } else {
                request.setAttribute(errorInfo, "* 上传图片格式不正确");
            }
        }

        DevUser devUser = (DevUser) session.getAttribute(Constants.DEV_USER_SESSION);
        int id = devUser.getId();
        appInfo.setModifyBy(id);
        appInfo.setLogoLocPath(workPicPath);
        appInfoService.modifyApp(appInfo);
        return "redirect:/dev/flatform/app/list";
    }

    //新增版本信息
    @RequestMapping("/flatform/app/appversionmodify")
    public String toModifyVersion(Model model, Integer aid, Integer vid) {
        List<AppInfo> appVersionList = appInfoService.getVersionById(aid);
        AppVersion appVersion = appVersionService.getNewVersionByid(aid);
        model.addAttribute("appVersionList", appVersionList);
        model.addAttribute("appVersion", appVersion);
        return "/developer/appversionmodify";
    }

    //删除apk文件
    @ResponseBody
    @RequestMapping("/delApk")
    public Object delapk(Model model, Integer id) {
        if (appVersionService.delApk(id) > 0) {
            model.addAttribute("result", "success");
        } else {
            model.addAttribute("result", "failed");
        }
        return JSON.toJSONString(model);
    }



    //修改版本信息
    @RequestMapping("/flatform/app/appversionmodifysave")
    public String Modify(HttpSession session, AppVersion appVersion,
                         @RequestParam( required = false) MultipartFile attach) {
       //判断是否修改apk文件
        if(attach != null && attach.getSize()!=0) {
              String oldFileName = attach.getOriginalFilename();//原文件名
              String prefix = FilenameUtils.getExtension(oldFileName);//原文件后缀
              if ((oldFileName != null || oldFileName != "") && prefix.equalsIgnoreCase("apk")) {//apk命名：apk名称+版本号+.apk
                  try {

                      String path = appVersion.getApkLocPath().substring(0,appVersion.getApkLocPath().lastIndexOf(File.separator));
                      String apkFileName = appVersion.getApkLocPath().substring(appVersion.getApkLocPath().lastIndexOf(File.separator)+1);
                      File targetFile = new File(path, apkFileName);
                      logger.info("路径：" + path);
                      logger.info("文件名：" + apkFileName);

                      //上传
                      attach.transferTo(targetFile);
                  } catch (IOException e) {
                      e.printStackTrace();
                      return "redirect:/dev/flatform/app/appversionmodify?aid=" + appVersion.getAppId() + "&error=error2";
                  }
              } else {
                  return "redirect:/dev/flatform/app/appversionmodify?aid=" + appVersion.getAppId() + "&error=error3";
              }
        }

        DevUser devUser = (DevUser) session.getAttribute(Constants.DEV_USER_SESSION);
        appVersion.setModifyBy(devUser.getId());
        appVersion.setModifyDate(new Date());

        appVersionService.modifyVersion(appVersion);

        return "redirect:/dev/flatform/app/appversionmodify?aid=" + appVersion.getAppId();
    }

    //删除软件全部信息
    @ResponseBody
    @GetMapping("/delAll")
    public Object delAppAll(Model model, Integer id) {
        int num1 = appVersionService.delVersion(id);
        int num2 = appInfoService.delApp(id);
        if (num2 > 0) {
            model.addAttribute("delResult", "true");
        } else {
            model.addAttribute("delResult", "false");
        }
        return JSON.toJSONString(model);
    }

    //根据软件id修改发布状态
    @ResponseBody
    @RequestMapping("/ModifyStatus")
    public Object ModifyStatus(HttpSession session,Model model,Integer appId){
        int number=0;
        DataDictionary dictionary = dataDictionaryService.getValueNameByAppid(appId);
        int mid = ((DevUser)session.getAttribute(Constants.DEV_USER_SESSION)).getId();
        if(dictionary.getValueId().equals(5)){//现在是下架状态，进行上架操作
            number = appInfoService.ModifyStatus(appId,4,mid);
        }else {//现在是上架状态，进行下架操作
            number = appInfoService.ModifyStatus(appId,5,mid);
        }
        if(dictionary==null){
            model.addAttribute("errorCode","1");
        }else {
            model.addAttribute("errorCode","0");
        }
        if(number>0){
            model.addAttribute("resultMsg","success");
        }else {
            model.addAttribute("resultMsg","failed");
        }
        return JSON.toJSONString(model);
    }

    //退出系统
    @RequestMapping("/logout")
    public String logout(HttpSession session) {
        session.removeAttribute(Constants.DEV_USER_SESSION);
        return "devlogin";
    }

}
