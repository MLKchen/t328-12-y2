package com.ccb.dao;

import com.ccb.pojo.AppVersion;

/**
 * @author CCB
 * date: 22/10/2022 上午11:01
 * Description: 版本接口
 */
public interface AppVersionMapper {

    /**
     * 新增版本信息
     * @param appVersion
     * @return
     */
    int addVersion(AppVersion appVersion);

    /**
     * 根据id获取最新版本信息
     * @param appId
     * @return
     */
    AppVersion getNewVersionByid(Integer appId);

    /**
     * 根据id删除apk路径
     * @param id
     * @return
     */
    int delApk(Integer id);

    /**
     * 修改版本信息
     * @return
     */
    int modifyVersion(AppVersion appVersion);

    /**
     * 删除软件版本信息
     * @param appid
     * @return
     */
    int delVersion(Integer appid);
}
