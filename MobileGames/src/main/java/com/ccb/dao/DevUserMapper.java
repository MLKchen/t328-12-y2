package com.ccb.dao;

import com.ccb.pojo.AppInfo;
import com.ccb.pojo.DevUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CCB
 * date: 19/10/2022 上午11:29
 * Description: 开发者接口
 */
public interface DevUserMapper {

    /**
     * 根据用户名密码验证登录用户
     * @return
     */
    DevUser getUser(@Param("devCode") String devCode, @Param("devPassword")String devPassword);

    /**
     * 获取app应用信息
     * @return
     */
    List<AppInfo> getAppinfoList(@Param("softwareName")String softwareName,@Param("status")String status,
                                 @Param("categoryLevel1")String categoryLevel1,@Param("categoryLevel2")String categoryLevel2,
                                 @Param("categoryLevel3")String categoryLevel3,
                                 @Param("flatformId")String flatformId,@Param("devId")Integer devId,
                                 @Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize);

    /**
     * 根据条件获取数量
     * @param softwareName
     * @param status
     * @param categoryLevel1
     * @param categoryLevel2
     * @param categoryLevel3
     * @param flatformId
     * @param devId
     * @return
     */
    int getCount(@Param("softwareName")String softwareName,@Param("status")String status,
                 @Param("categoryLevel1")String categoryLevel1,@Param("categoryLevel2")String categoryLevel2,
                 @Param("categoryLevel3")String categoryLevel3,
                 @Param("flatformId")String flatformId,@Param("devId")Integer devId);

}
