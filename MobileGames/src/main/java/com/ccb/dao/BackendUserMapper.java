package com.ccb.dao;

import com.ccb.pojo.BackendUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CCB
 * date: 19/10/2022 上午8:58
 * Description: 管理员接口
 */
public interface BackendUserMapper {

    /**
     * 根据用户名密码验证登录用户
     * @return
     */
    BackendUser getUser(@Param("userCode") String userCode, @Param("userPassword")String userPassword);
}
