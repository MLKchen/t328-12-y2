package com.ccb.dao;

import com.ccb.pojo.DataDictionary;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CCB
 * date: 19/10/2022 下午4:43
 * Description: 数据字典信息获取
 */
public interface DataDictionaryMapper {

    /**
     * 获取状态列表
     * @return
     */
    List<DataDictionary> getList(@Param("typeCode") String typeCode);


    /**
     * 获取软件发布状态
     * @param appid
     * @return
     */
    DataDictionary getValueNameByAppid(Integer appid);

}
