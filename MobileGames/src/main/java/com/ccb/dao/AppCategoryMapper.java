package com.ccb.dao;

import com.ccb.pojo.AppCategory;
import com.ccb.pojo.AppInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CCB
 * date: 21/10/2022 下午2:50
 * Description: 分类等级接口
 */
public interface AppCategoryMapper {
    /**
     * 根据父级id获等级列表
     *
     * @param pid
     * @return
     */
    List<AppCategory> getcategoryLevelList(@Param("pid") String pid);
}
