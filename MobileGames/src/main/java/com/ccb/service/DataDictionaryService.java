package com.ccb.service;

import com.ccb.pojo.DataDictionary;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CCB
 * date: 19/10/2022 下午4:54
 * Description: 描述
 */
public interface DataDictionaryService {

    /**
     * 获取状态列表
     * @return
     */
    List<DataDictionary> getList(@Param("typeCode") String typeCode);

    /**
     * 获取软件发布状态
     * @param appid
     * @return
     */
    DataDictionary getValueNameByAppid(Integer appid);


}
