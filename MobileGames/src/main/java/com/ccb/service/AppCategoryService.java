package com.ccb.service;

import com.ccb.pojo.AppCategory;
import com.ccb.pojo.AppInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CCB
 * date: 21/10/2022 下午2:51
 * Description: 描述
 */
public interface AppCategoryService {
    /**
     * 根据父级id获等级列表
     * @param pid
     * @return
     */
    List<AppCategory> getcategoryLevelList(@Param("pid")String pid);
}
