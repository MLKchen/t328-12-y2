package com.ccb.service;

import com.ccb.pojo.AppInfo;
import com.ccb.pojo.DevUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CCB
 * date: 19/10/2022 上午11:36
 * Description: 描述
 */
public interface DevUserService {
    /**
     * 根据用户名密码验证登录用户
     * @return
     */
    DevUser getUser(String devCode, String devPassword);

    /**
     * 获取app应用信息
     * @return
     */
    List<AppInfo> getAppinfoList(String softwareName, String status,
                                 String categoryLevel1, String categoryLevel2,
                                 String categoryLevel3,
                                 String flatformId, Integer devId,
                                 Integer pageIndex, Integer pageSize);

    /**
     * 根据条件获取数量
     * @param softwareName
     * @param status
     * @param categoryLevel1
     * @param categoryLevel2
     * @param categoryLevel3
     * @param flatformId
     * @param devId
     * @return
     */
    int getCount(@Param("softwareName")String softwareName,@Param("status")String status,
                 @Param("categoryLevel1")String categoryLevel1,@Param("categoryLevel2")String categoryLevel2,
                 @Param("categoryLevel3")String categoryLevel3,
                 @Param("flatformId")String flatformId,@Param("devId")Integer devId);

}
