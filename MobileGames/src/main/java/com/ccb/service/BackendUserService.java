package com.ccb.service;

import com.ccb.pojo.BackendUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

/**
 * @author CCB
 * date: 19/10/2022 上午9:09
 * Description: 描述
 */
public interface BackendUserService {

    /**
     * 根据用户名密码验证登录用户
     * @return
     */
    BackendUser getUser(String userCode,String userPassword);

}
