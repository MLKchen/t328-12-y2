package com.ccb.service.impl;

import com.ccb.dao.DataDictionaryMapper;
import com.ccb.pojo.DataDictionary;
import com.ccb.service.DataDictionaryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author CCB
 * date: 19/10/2022 下午4:54
 * Description: 描述
 */
@Service("DataDictionaryService")
public class DataDictionaryServiceImpl implements DataDictionaryService {

    @Resource
    public DataDictionaryMapper dataDictionaryMapper;

    @Override
    public List<DataDictionary> getList(String typeCode) {
        return dataDictionaryMapper.getList(typeCode);
    }

    @Override
    public DataDictionary getValueNameByAppid(Integer appid) {
        return dataDictionaryMapper.getValueNameByAppid(appid);
    }
}
