package com.ccb.service.impl;

import com.ccb.dao.DevUserMapper;
import com.ccb.pojo.AppInfo;
import com.ccb.pojo.DevUser;
import com.ccb.service.DevUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author CCB
 * date: 19/10/2022 上午11:36
 * Description: 描述
 */
@Service("DevUserService")
public class DevUserServiceImpl implements DevUserService {

    @Resource
    public DevUserMapper devUserMapper;

    @Override
    public DevUser getUser(String userCode, String devPassword) {
        return devUserMapper.getUser(userCode,devPassword);
    }

    @Override
    public List<AppInfo> getAppinfoList(String softwareName, String status,
                                        String categoryLevel1, String categoryLevel2, String categoryLevel3,
                                        String flatformId, Integer devId, Integer pageIndex, Integer pageSize) {
        return devUserMapper.getAppinfoList(softwareName,status,categoryLevel1,categoryLevel2,categoryLevel3,flatformId,devId,pageIndex,pageSize);
    }

    @Override
    public int getCount(String softwareName, String status, String categoryLevel1, String categoryLevel2, String categoryLevel3, String flatformId, Integer devId) {
        return devUserMapper.getCount(softwareName,status,categoryLevel1,categoryLevel2,categoryLevel3,flatformId,devId);
    }
}
