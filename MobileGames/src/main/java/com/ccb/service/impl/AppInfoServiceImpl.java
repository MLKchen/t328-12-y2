package com.ccb.service.impl;

import com.ccb.dao.AppInfoMapper;
import com.ccb.pojo.AppInfo;
import com.ccb.service.AppInfoService;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author CCB
 * date: 21/10/2022 下午2:39
 * Description: 描述
 */
@Service("AppInfoService")
public class AppInfoServiceImpl implements AppInfoService {

    @Resource
    public AppInfoMapper appInfoMapper;

    @Override
    public int checkAPKName(String APKName) {
        return appInfoMapper.checkAPKName(APKName);
    }

    @Override
    public int addsoftware(AppInfo appInfo) {
        return appInfoMapper.addsoftware(appInfo);
    }

    @Override
    public AppInfo getAppByid(String id) {
        return appInfoMapper.getAppByid(id);
    }

    @Override
    public List<AppInfo> getVersionById(Integer id) {
        return appInfoMapper.getVersionById(id);
    }

    @Override
    public int updateVersionIdById(Integer id, String versionId,String softwareSize) {
        return appInfoMapper.updateVersionIdById(id,versionId,softwareSize);
    }

    @Override
    public int delimg(Integer id) {
        return appInfoMapper.delimg(id);
    }

    @Override
    public int modifyApp(AppInfo appInfo) {
        return appInfoMapper.modifyApp(appInfo);
    }

    @Override
    public int delApp(Integer id) {
        return appInfoMapper.delApp(id);
    }

    @Override
    public int ModifyStatus(Integer id, Integer status,Integer mid) {
        return appInfoMapper.ModifyStatus(id,status,mid);
    }
}
