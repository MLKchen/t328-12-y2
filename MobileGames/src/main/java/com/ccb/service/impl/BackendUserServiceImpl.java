package com.ccb.service.impl;

import com.ccb.dao.BackendUserMapper;
import com.ccb.pojo.BackendUser;
import com.ccb.service.BackendUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author CCB
 * date: 19/10/2022 上午9:11
 * Description: 描述
 */
@Service("BackendUserService")
public class BackendUserServiceImpl implements BackendUserService {

    @Resource
    public BackendUserMapper backendUserMapper;

    @Override
    public BackendUser getUser(String userCode, String userPassword) {
        return backendUserMapper.getUser(userCode,userPassword);
    }
}
