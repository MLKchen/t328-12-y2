package com.ccb.service.impl;

import com.ccb.dao.AppCategoryMapper;
import com.ccb.pojo.AppCategory;
import com.ccb.pojo.AppInfo;
import com.ccb.service.AppCategoryService;
import com.ccb.service.AppInfoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author CCB
 * date: 21/10/2022 下午2:51
 * Description: 描述
 */
@Service("AppCategoryService")
public class AppCategoryServiceImpl implements AppCategoryService {

    @Resource
    public AppCategoryMapper appCategoryMapper;

    @Override
    public List<AppCategory> getcategoryLevelList(String pid) {
        return appCategoryMapper.getcategoryLevelList(pid);
    }
}
