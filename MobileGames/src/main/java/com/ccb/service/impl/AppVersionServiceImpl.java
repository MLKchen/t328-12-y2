package com.ccb.service.impl;

import com.ccb.dao.AppVersionMapper;
import com.ccb.pojo.AppVersion;
import com.ccb.service.AppVersionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author CCB
 * date: 22/10/2022 上午11:10
 * Description: 版本信息控制服务实现类
 */
@Service("AppVersionService")
public class AppVersionServiceImpl implements AppVersionService {

    @Resource
    public AppVersionMapper appVersionMapper;

    @Override
    public int addVersion(AppVersion appVersion) {
        return appVersionMapper.addVersion(appVersion);
    }

    @Override
    public AppVersion getNewVersionByid(Integer appId) {
        return appVersionMapper.getNewVersionByid(appId);
    }

    @Override
    public int delApk(Integer id) {
        return appVersionMapper.delApk(id);
    }

    @Override
    public int modifyVersion(AppVersion appVersion) {
        return appVersionMapper.modifyVersion(appVersion);
    }

    @Override
    public int delVersion(Integer appid) {
        return appVersionMapper.delVersion(appid);
    }
}
