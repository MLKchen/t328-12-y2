package com.ccb.service;

import com.ccb.pojo.AppVersion;

/**
 * @author CCB
 * date: 22/10/2022 上午11:10
 * Description: 版本信息控制服务
 */
public interface AppVersionService {


    /**
     * 新增版本信息
     * @param appVersion
     * @return
     */
    int addVersion(AppVersion appVersion);

    /**
     * 根据id获取最新版本信息
     * @param appId
     * @return
     */
    AppVersion getNewVersionByid(Integer appId);

    /**
     * 根据id删除阿帕克路径
     * @param id
     * @return
     */
    int delApk(Integer id);

    /**
     * 修改版本信息
     * @return
     */
    int modifyVersion(AppVersion appVersion);

    /**
     * 删除软件版本信息
     * @param appid
     * @return
     */
    int delVersion(Integer appid);

}
