package com.ccb.service;

import com.ccb.pojo.AppInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CCB
 * date: 21/10/2022 下午2:38
 * Description: 分类描述
 */
public interface AppInfoService {

    /**
     * 检查apkname
     * @param APKName
     * @return
     */
    int checkAPKName(String APKName);

    /**
     * 新增软件商店
     * @param appInfo
     * @return
     */
    int addsoftware(AppInfo appInfo);

    /**
     * 根据id获取软件信息
     * @param id
     * @return
     */
    AppInfo getAppByid(String id);

    /**
     * 根据id获取历史版本信息
     * @param id
     * @return
     */
    List<AppInfo> getVersionById(Integer id);

    /**
     * 根据id修改软件版本id
     * @param id
     * @return
     */
    int updateVersionIdById(@Param("id") Integer id,@Param("versionId") String versionId,@Param("softwareSize") String softwareSize);

    /**
     * 根据id删除图片  注：修改表中的图片路径为空
     * @param id
     * @return
     */
    int delimg(Integer id);

    /**
     * 修改软件基本信息
     * @param appInfo
     * @return
     */
    int modifyApp(AppInfo appInfo);

    /**
     * 删除软件全部信息
     * @param id
     * @return
     */
    int delApp(Integer id);

    /**
     * 修改软件状态
     * @param id
     * @param status
     * @return
     */
    int ModifyStatus(@Param("id") Integer id,@Param("status") Integer status,@Param("mid") Integer mid);

}
