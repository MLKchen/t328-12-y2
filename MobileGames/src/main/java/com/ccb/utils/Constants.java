package com.ccb.utils;

import com.mysql.cj.Session;

/**
 * 常量类
 */
public class Constants {
	public final static String USER_SESSION = "userSession";
	public final static String DEV_USER_SESSION = "devUserSession";
	public final static String SYS_MESSAGE = "message";
	public final static int pageSize = 5;
}
