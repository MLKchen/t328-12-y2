package com.ccb.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author CCB
 * date: 10/10/2022 上午8:29
 * Description: 通过单例模式读取配置文件
 *
 */
public class ConfigManager {
    private static Properties properties;
    private static ConfigManager configManager = new ConfigManager();

    /**
     * 杜绝外界创建该类对象，私有构造方法
     */
    private ConfigManager(){
        String configFile = "database.properties";
        properties = new Properties();
        InputStream is = ConfigManager.class.getClassLoader().getResourceAsStream(configFile);

        try {
            properties.load(is);
            is.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    /**
     * 对外提供的获取ConfigManage实例方法
     * @return
     */
//    public static synchronized ConfigManager getInstance(){
//        if(configManager == null){
//            configManager = new ConfigManager();
//        }
//        return configManager;
//    }

    /**
     * 饿汉模式
     * @return
     */
    public static ConfigManager getInstance(){
        return configManager;
    }

    /**
     * 读取配置文件信息
     * @param key
     * @return
     */
    public String getValue(String key){
        return properties.getProperty(key);
    }
}
