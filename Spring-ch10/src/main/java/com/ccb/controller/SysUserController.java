package com.ccb.controller;

import com.ccb.pojo.SysUser;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Date;

/**
 * @author CCB
 * date: 10/10/2022 上午9:57
 * Description: 描述
 */
@Controller
@RequestMapping("/user")
public class SysUserController {
    private Logger logger = Logger.getLogger(SysUserController.class);
    private ArrayList<SysUser> sysUserList = new ArrayList<SysUser>();

    public SysUserController(){
        try {
            sysUserList.add(new SysUser(5,"zhapjing","赵静","5555555",1
                    ,"13054784445","上海市宝山区",1,1,new Date(),1,new Date()));
            sysUserList.add(new SysUser(4,"wanglin","王林","4444444",1
                    ,"18965652346","北京市学院路",1,1,new Date(),1,new Date()));
            sysUserList.add(new SysUser(1,"test001","测试用户001","1111111",1
                    ,"13566669998","北京市朝阳区",1,1,new Date(),1,new Date()));
            sysUserList.add(new SysUser(2,"zhaodao","赵燕","2222222",1
                    ,"18678786545","上海市海淀区",1,1,new Date(),1,new Date()));
            sysUserList.add(new SysUser(3,"test003","测试用户003","1111111",1
                    ,"13121334531","上海市海淀区",1,1,new Date(),1,new Date()));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private ArrayList<SysUser> querUserList = new ArrayList<SysUser>();
    //查询所有用户
    @GetMapping("/list")
    public String list(Model model){
        logger.info("当查询条件为空时，查询用户信息");
        querUserList.clear();
        querUserList.addAll(sysUserList);
        model.addAttribute("querUserList",querUserList);
        return "sysUSerList";
    }

    @PostMapping("/list")
    public String list(@RequestParam(value = "realName",required = false)String realName,Model model) {
    logger.info("查询条件：【realName】=" + realName + ",查询用户信息");
    querUserList.clear();
        if (realName != null && !realName.equals("")) {
            for (SysUser user : sysUserList) {
                if (user.getRealName().indexOf(realName) != -1) {
                    querUserList.add(user);
                }
            }
        } else {
            querUserList.addAll(sysUserList);
        }
        model.addAttribute("querUserList",querUserList);
        return "sysUSerList";
    }

    @RequestMapping(value = "/toLogin")
    public String login(){
        logger.debug("跳转到登录页面");
        return "login";
    }

    public String doLogin(@RequestParam String account,@RequestParam String password){
        logger.debug("登录方法");
        //调用service方法，进行用户匹配
        return "login";
    }
}
