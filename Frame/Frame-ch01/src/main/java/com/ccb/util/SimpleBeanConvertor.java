package com.ccb.util;

import com.mchange.v1.db.sql.UnsupportedTypeException;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.channels.UnsupportedAddressTypeException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.logging.Logger;

/**
 * @author CCB
 * date: 6/9/2022 下午3:43
 * Description: 描述
 */
public class SimpleBeanConvertor {
    private static Logger logger = Logger.getLogger(String.valueOf(SimpleBeanConvertor.class));


    public static <T> T convert(HttpServletRequest request, Class<T> targetType) throws IllegalAccessException, InstantiationException, ParseException, ParseException
            , InstantiationException, UnsupportedTypeException {
        //获取所有请求参数
        Map<String, String[]> params = request.getParameterMap();
        if (params.size() == 0) return null;
        T target = targetType.newInstance();//创建JavaBean实例
        //获取JavaBean中所有方法
        Method[] allMethods = targetType.getMethods();
        if (allMethods != null && allMethods.length > 0) {
            for (Method method : allMethods) {
                if (method.getName().startsWith("set")) {
                    Class[] args = method.getParameterTypes();
                    if (args.length == 1) {
                        String paramName = method.getName().substring(3, 4).toLowerCase() + method.getName().substring(4);
                        if (params.containsKey(paramName)) {
                            try {
                                Object value = parseValue(params.get(paramName), args[0]);
                                method.invoke(target, value);
                            } catch (InvocationTargetException e) {
                                e.printStackTrace();
                            }
                        } else if (Boolean.class == args[0] || boolean.class == args[0]) {
                            try {
                                method.invoke(target, false);
                            } catch (InvocationTargetException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
        return target;
    }

    private static Object parseValue(String[] value, Class type) throws ParseException, UnsupportedTypeException {
        if (String.class == type)
            return value[0];
        if (java.util.Date.class == type)
            return new SimpleDateFormat("yyyy-MM-dd").parse(value[0]);
        if (boolean.class == type || Boolean.class == type)
            return true;
        if (char.class == type || Character.class == type)
            return value[0].charAt(0);
        if (int.class == type || Integer.class == type)
            return Integer.valueOf(value[0]);
        if (short.class == type || byte.class == type || long.class == type || float.class == type || double.class == type)
            try {
                type = Class.forName("java." + type.getName().substring(0, 1).toUpperCase() + type.getName().substring(1));
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        if (type.getName().startsWith("java.lang.") || type.getName().startsWith("java.math"))
            try {
                return type.getConstructor(String.class).newInstance(value[0]);
            } catch (Exception e) {
                e.printStackTrace();
            }
            throw new UnsupportedTypeException();
    }
}
