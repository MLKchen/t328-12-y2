package com.ccb.Task;

import java.io.Serializable;

/**
 * @author CCB
 * date: 6/9/2022 下午2:07
 * Description: 描述
 */
public class ClassName extends Student implements Serializable {
    private String stuNo;//学号
    private String ClassName = "T328";//班级

    public ClassName() {
    }

    public ClassName(String name, Integer age, String sex, String stuNo, String className) {
        super(name, age, sex);
        this.stuNo = stuNo;
        ClassName = className;
    }

    public String getStuNo() {
        return stuNo;
    }

    public void setStuNo(String stuNo) {
        this.stuNo = stuNo;
    }

    public String getClassName() {
        return ClassName;
    }

    public void setClassName(String className) {
        ClassName = className;
    }

    public void show(){
        System.out.println("我是一个方法\n你调用成功了！！！");
    }
}
