package com.ccb.Task;

/**
 * @author CCB
 * date: 6/9/2022 下午2:05
 * Description: 学生基本信息
 */
public class Student {
    private String name;//姓名
    private Integer age = 20;//年龄
    private String sex;//性别

    public Student() {
    }

    public Student(String name, Integer age, String sex) {
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
