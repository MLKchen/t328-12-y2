package com.ccb.entity;

import java.util.Date;

/**
 * @author CCB
 * date: 6/9/2022 下午8:50
 * Description: 描述
 */
public class User implements java.io.Serializable {
    private int id;
    private String username;
    private Boolean ok;
    private char sex;
    private Date birthday;
    private Float salary;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getOk() {
        return ok;
    }

    public void setOk(Boolean ok) {
        this.ok = ok;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Float getSalary() {
        return salary;
    }

    public void setSalary(Float salary) {
        this.salary = salary;
    }

    @Override
    public String toString(){
        return "User{"+
                "id="+id+
                ",username="+username+"\"+" +
                ",sex="+sex+
                ",salary="+salary+
                ",ok="+ok+"}";
    }
}
