package com.ccb.shili.entity;

import java.io.IOException;

/**
 * @author CCB
 * date: 5/9/2022 上午8:29
 * Description: 描述
 */
public final class Person extends BaseClass implements java.io.Serializable {
    private String name;
    static final int age = 30;
    protected String address;
    public String message;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static int getAge() {
        return age;
    }

    public String getAddress() {
        return address;
    }

    private void silentMethod() throws IOException,NullPointerException{
        System.out.println("这是悄悄话");
    }

    public Person(){}

    private Person(String name){this.name = name;}

    protected Person(String name,String address,String message){
        this.name = name;
        this.address = address;
        this.message = message;

    }

    @Override
    public String toString(){
        return "{name:"+name+",age:"+age+",address:"+address+",message:"+message+"}";
    }
}