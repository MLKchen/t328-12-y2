package com.ccb.Task;

import org.junit.Test;

import java.lang.reflect.*;

import static org.junit.Assert.*;

/**
* @author CCB
* date: 2022-09-06 下午2:10
* Description: Frame-反射-ch01
*/
public class ClassNameTest {

    /**
    * @author CCB
    * date: 2022-09-06 下午2:18
    * Description: Task01
    */
    @Test
    public void Task01(){
        Class clz = ClassName.class;

        //获取类名
        System.out.println("类名："+clz.getSimpleName());

        //获取修饰符
        int modifier = clz.getModifiers();//获取修饰符对应的int类型
        if((modifier & Modifier.PUBLIC) == Modifier.PUBLIC)
            System.out.println("访问修饰符是：public");
        else
            System.out.println("访问修饰符是：default(package)");
        if((modifier & Modifier.FINAL) == Modifier.FINAL)
            System.out.println("这个类是final的");
        if((modifier & Modifier.ABSTRACT) == Modifier.ABSTRACT)
            System.out.println("这是一个抽象类");
        if((modifier & Modifier.INTERFACE) == Modifier.INTERFACE)
            System.out.println("这是一个接口");

        //获取父类
        System.out.println("超类："+clz.getSimpleName());

        //获取构造方法
        Constructor[] cons = clz.getDeclaredConstructors();
        for (Constructor constructor : cons){
            Class[] params = constructor.getParameterTypes();
                System.out.print("构造方法有[");
                for (int i=0;i<params.length;i++){
                    System.out.print(params[i].getName());
                }
                System.out.println("]");
            }

        //获取全部属性
        Field[] fields = clz.getDeclaredFields();
        System.out.println("==========属性展示开始==========");
        for (Field field : fields){
            System.out.println("属性名："+field.getName());
            System.out.println("类型："+field.getType().getName());
            System.out.println("-----我是一条分割线-----");
        }
        System.out.println("==========属性展示结束==========");

        //获取全部方法
        Method[] methods = clz.getDeclaredMethods();

        System.out.println("==========方法展示开始==========");
        for (Method method : methods){
            System.out.println("方法名："+method.getName());
            System.out.println("返回值类型："+method.getReturnType().getName());
            System.out.println("方法声明在："+method.getReturnType().getName());
            Class declaringClass = method.getDeclaringClass();
            System.out.println("方法声明在："+declaringClass.getName()+"中");
            System.out.println("-----我是一条分割线-----");
        }
        System.out.println("==========方法展示结束==========");
    }

    /**
    * @author CCB
    * date: 2022-09-06 下午3:25
    * Description: Task02
    */
    @Test
    public void Task02(){
        try {
            Class clz = ClassName.class;
            Object obj = clz.newInstance();
            //获取属性
            Field ClassName = clz.getDeclaredField("ClassName");
            ClassName.setAccessible(true);
            System.out.println("赋值前的ClassName："+ClassName.get(obj));
            //赋值
            ClassName.set(obj,"T328-12-CCB");
            //赋值后
            System.out.println("赋值后的ClassName："+ClassName.get(obj));
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    /**
    * @author CCB
    * date: 2022-09-06 下午3:40
    * Description: Task03
    */
    @Test
    public void Task03(){
        try {
            Class clz = ClassName.class;
            Method show = clz.getDeclaredMethod("show",null);
            Object showTest = clz.newInstance();
            show.invoke(showTest,null);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }
}