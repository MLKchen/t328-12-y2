import com.ccb.shili.entity.Person;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * @author CCB
 * date: 5/9/2022 上午9:01
 * Description: 描述
 */
public class Test {

    /**
    * @author CCB
    * date: 2022-09-05 上午9:25
    * Description: 示例2
    */
   @org.junit.Test
    public void shili2(){
        Class clz = Person.class;
        String fillName = clz.getName();
        String simpleName = clz.getSimpleName();
        System.out.println("以下是："+fillName+"类的基本信息");
        System.out.println("----------------------------");

        //获取Person类所在的包
        Package pkg = clz.getPackage();
        System.out.println(simpleName+"定义在："+pkg.getName()+"包中");
        System.out.println("----------------------------");

        //获取clz所表示的实体的超类的Class
        //如果clz表示Object类，一个接口，一个基本类型或void，则返回null
        //如果clz表示一个数组，则返回表示Object类的Class实例
        Class superClass = clz.getSuperclass();
        System.out.println(simpleName+"类的父类是："+superClass.getName());
        System.out.println("----------------------------");

        //如果clz表示类的实现接口
        //如果clz表示一个不实现任何接口的类或接口，则此方法返回一个长度为0的数组
        //如果clz表示一个基本类型或void，则此方法返回一个长度为0的数组
        Class[] interfaces = clz.getInterfaces();
        System.out.println(simpleName+"类所实现的接口：");
        for (Class c : interfaces){
            System.out.println("\t"+c.getName());
        }
        System.out.println("----------------------------");

        //每个修饰符对应一个int常量，返回的修饰符信息将类所拥有的修饰符以"或"运算组合
        //通过与Modifier类的常量进行”与“运算即可判断该类型是否拥有莫格修饰符
        int modifier = clz.getModifiers();
        System.out.println(simpleName+"类的修饰符");
        if((modifier & Modifier.PUBLIC) == Modifier.PUBLIC)
            System.out.println("\t访问修饰符是：puble");
        else
            System.out.println("\t访问修饰符是：default(package)");
        if((modifier & Modifier.FINAL) == Modifier.FINAL){
            System.out.println("\t这个类是final的");
        }
        if((modifier & Modifier.ABSTRACT) == Modifier.ABSTRACT){
            System.out.println("\t这是一个抽象类");
        }
        if((modifier & Modifier.INTERFACE) == Modifier.INTERFACE){
            System.out.println("\t这是一个接口");
        }
        System.out.println("----------------------------");
    }

    /**
    * @author CCB
    * date: 2022-09-05 上午9:39
    * Description: 示例3
    */
    @org.junit.Test
    public void shili3(){
       //获取Person类声明的所有构造方法
        //它们是公共，保护，默认(包访问)和私有构造方法
        //如果此Class实例表示一个接口，一个基本类型，一个数组类或void，则
        //此方法返回一个长度为0的数组
        Constructor[] cons = Person.class.getDeclaredConstructors();

        System.out.println("=====构造方法=====");
        for (Constructor con : cons){
            System.out.println("访问修饰符");
            int modifier = con.getModifiers();
            //判断该构造方法的访问修饰符
            if((modifier & Modifier.PUBLIC) == Modifier.PUBLIC){
                System.out.println("public");
            }else if((modifier & Modifier.PROTECTED) == Modifier.PROTECTED){
                System.out.println("protected");
            }else if((modifier & Modifier.PRIVATE) == Modifier.PRIVATE){
                System.out.println("private");
            }else {
                System.out.println("default(package)");
            }

            //获取构造方法的参数列表
            Class[] params = con.getParameterTypes();
            if(params.length==0){
                System.out.println("该构造方法没有参数");
            }else {
                System.out.println("该构造方法的参数列表为：[");
                for (int i=0;i<params.length;i++){
                    if(i != 0)
                        System.out.println(", ");
                    System.out.println(params[i].getName());
                }
                System.out.println("]");
            }
            System.out.println("-----------------------------------------");
        }
    }

    /**
    * @author CCB
    * date: 2022-09-05 上午9:54
    * Description: 示例4
    */
    @org.junit.Test
    public void shili04(){
        //获取Person类声明的所有构造方法
        //包括公共，保护，默认访问和私有属性，但不包括继承的属性
        //如果该类或接口不声明任何属性，或者此Class实例表示一个基本类型，
        //一个数组或void，则此方法返回一个长度为0的数组
        Field[] fields = Person.class.getDeclaredFields();
        //展示属性的一些信息
        System.out.println("=====属性展示=====");
        for (Field field : fields){
            System.out.println("属性名："+field.getName());
            System.out.println("类型："+field.getType());

            System.out.println("访问修饰符：");
            int modifier = field.getModifiers();
            //判断该属性的访问修饰符
            //判断该属性是否有static修饰符
            if((modifier & Modifier.STATIC) == Modifier.STATIC){
                System.out.println("这是一个静态属性");
            }
            //判断该属性是否有final修饰符
            if((modifier & Modifier.FINAL) == Modifier.FINAL){
                System.out.println("这是一个final属性");
                System.out.println("-----------------------------");
            }
        }
    }

    @org.junit.Test
    public void shili05(){
        //获取Person类中的所以方法
        //包括公共，保护，默认访问和私有方法，但不包括继承的方法
        //如果该类或接口不声明任何方法，或者此Class实例表示一个基本类型，
        //一个数组或void，则此方法返回一个长度为0的数组
        Method[] methods = Person.class.getDeclaredMethods();

        //展示方法的一下信息
        System.out.println("==========方法展示==========");
        for (Method method : methods){
            System.out.println("方法名："+method.getName());
            System.out.println("返回值类型："+method.getReturnType().getName());

            //获取方法的参数列表
            Class[] params = method.getParameterTypes();

            System.out.println("访问修饰符：");
            int modifier = method.getModifiers();
            //判断该方法的访问修饰符
            //判断该方法是否有static修饰符
            if((modifier & Modifier.STATIC) == Modifier.STATIC){
                System.out.println("这是一个静态方法");
            }

            //判断该方法是否有final修饰符
            if((modifier & Modifier.FINAL) == Modifier.FINAL){
                System.out.println("这是一个final方法");
            }

            //判断该方法是否有abstract修饰符
            if((modifier & Modifier.ABSTRACT) == Modifier.ABSTRACT){
                System.out.println("这是一个抽象方法");
            }

            //判断该方法是否有synchronized修饰符
            if((modifier & Modifier.SYNCHRONIZED) == Modifier.SYNCHRONIZED){
                System.out.println("这是一个同步方法");
            }

            //获取方法所属的类或接口的Class实例
            Class declaringClass = method.getDeclaringClass();
            System.out.println("方法声明在："+declaringClass.getName()+"中");

            //获取方法抛出的异常类型，即throws子句中中声明的异常
            Class[] exceptions = method.getExceptionTypes();
            if(exceptions.length>0){
                System.out.println("该方法抛出的异常有：[");
                for (int i =0;i<exceptions.length;i++){
                    if(i != 0)
                        System.out.println(",");
                    System.out.println(exceptions[i].getName());
                }
                System.out.println("]");
            }
            System.out.println("--------------------------------");
        }
    }

    /**
    * @author CCB
    * date: 2022-09-05 上午10:33
    * Description: 示例06
    */
    @org.junit.Test
    public void shili06(){
        try {
            Class clz = Class.forName("com.ccb.shili.entity.Person");
            Object obj = clz.newInstance();
            System.out.println(obj);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
    * @author CCB
    * date: 2022-09-05 上午10:44
    * Description: 示例7
    */
    @org.junit.Test
    public void shili07(){
        try {
            Class clz = Class.forName("com.ccb.shili.entity.Person");
            //获取Person的无参构造：public Person()
            Constructor c1 = clz.getDeclaredConstructor();
            //Person的无参构造为public，这里可以直接访问
            Object obj = c1.newInstance();
            System.out.println(obj);

            //获取Person的单参构造：private Person(String)
            Constructor c2 = clz.getDeclaredConstructor(String.class);
            //Person的单参构造为private，这里已超出访问范围，不能直接访问
            //通过setAccessable方法，设定为可以访问
            c2.setAccessible(true);
            obj = c2.newInstance("New Person");
            System.out.println(obj);

            //获取Person的三参构造：protected Person(String,String,String)
            Constructor c3 = clz.getDeclaredConstructor(String.class,String.class,String.class);
            //Person的三参构造方法为protected,这里已超出其访问范围，不能直接访问
            //通过setAccessble方法，设定为可以访问
            c3.setAccessible(true);
            obj = c3.newInstance("New Person","beijing","Hello!");
            System.out.println(obj);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @org.junit.Test
    public void shili08(){
        try {
            Class clz = Class.forName("com.ccb.shili.entity.Person");
            Object person = clz.newInstance();

            //获取private String name属性
            Field name = clz.getDeclaredField("name");
            //name属性为private，这里已超出其访问范围，不能直接访问
            //通过setAccessable方法，设定为可以访问
            name.setAccessible(true);
            //先取值看一下
            System.out.println("赋值前的name："+name.get(person));
            //为name属性赋值
            name.set(person,"New Person");
            //展示赋值效果
            System.out.println("赋值后的name："+name.get(person));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
    * @author CCB
    * date: 2022-09-05 上午11:14
    * Description: 示例9
    */
    @org.junit.Test
    public void shili09(){
        //通过反射加载Person类
        try {
            Class clz = Class.forName("com.ccb.shili.entity.Person");
            //根据方法名和参数列表获取static final int getAge()方法，
            //没有参数可以不写或用null表示
            Method getAge = clz.getDeclaredMethod("getAge",null);
            //getAge方法为default(package),这里已超出其访问范围，不能直接访问
            //通过setAcccessable方法，设定为可以访问
            getAge.setAccessible(true);
            //调用getAge方法并传参，没有参数可以不写或用null表示
            //getAge方法为静态方法，调用时可以不指定具体Person实例
            Object returnAge = getAge.invoke(null,null);
            System.out.println("年龄是："+returnAge);

            Object person = clz.newInstance();

            //根据方法名和参数列表获取private void silentMethod()方法
            //没有参数可以不写或用null表示
            Method silentMethod = clz.getDeclaredMethod("silentMethod",null);
            //silentMethod方法为private，这里已超出其访问范围，不能直接访问
            //通过setAccessable方法，设定为可以访问
            silentMethod.setAccessible(true);
            //调用silentMethod方法并传参，没有参数可以不写或用null表示
            silentMethod.invoke(person,null);

            //根据方法名和参数列表获取public void setName(String)方法
            Method setName = clz.getDeclaredMethod("setName", String.class);
            //setName方法为public，这里可以直接访问
            //调用setName方法并传参
            setName.invoke(person,"New Person");
            //验证一下结果，调用public String getName()方法得到name的值
            Object returnName = clz.getDeclaredMethod("getName").invoke(person);
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
