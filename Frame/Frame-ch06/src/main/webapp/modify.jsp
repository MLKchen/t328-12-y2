<%--
  Created by IntelliJ IDEA.
  User: MLK
  Date: 16/9/2022
  Time: 下午3:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"  isELIgnored="false" %>
<html>
<head>
    <title>修改</title>
</head>
<body style="background-color: antiquewhite">
<form action="do/user/update">
<table border="1" style="margin: auto;text-align: center;padding: 0px">
    <input type="text" name="id" value="${sessionScope.USER.id}" style="display: none">
    <tr>
        <th colspan="2">修改员工</th>
    </tr>
    <tr>
        <td>姓名：</td>
        <td><input type="text" name="username" value="${sessionScope.USER.username}"></td>
    </tr>
    <tr>
        <td>工号：</td>
        <td><input type="text" name="userNO" value="${sessionScope.USER.userNO}"></td>
    </tr>
    <tr>
        <td  colspan="2">
            <input type="submit" value="提交">
            <input type="button" value="返回" onclick="javascript:window.history.back()">
        </td>
    </tr>
</table>
</form>
</body>
</html>

