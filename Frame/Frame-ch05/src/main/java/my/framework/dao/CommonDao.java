package my.framework.dao;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.*;

/**
 * @author CCB
 * date: 16/9/2022 下午7:38
 * Description: 描述
 */
public class CommonDao {
    public int executeUpdate(String sql, Object[] params) throws SQLException {
        int result = 0;
        PreparedStatement pstmt = null;

        try {
            pstmt = DatabaseUtil.getConnection().prepareStatement(sql);
            if (params != null && params.length != 0) {
                for(int i = 0; i < params.length; ++i) {
                    pstmt.setObject(i + 1, params[i]);
                }
            }
            result = pstmt.executeUpdate();
        } catch (SQLException var9) {
            var9.printStackTrace();
            throw var9;
        } finally {
            DatabaseUtil.closeStatement(pstmt, (ResultSet)null);
        }
        return result;
    }

    public int executeUpdate(String sql, Object entity) throws SQLException {
        Map<String, Object[]> mappingResult = this.handleParameterMapping(sql, entity);
        Map.Entry<String, Object[]> entry = (Map.Entry)mappingResult.entrySet().iterator().next();
        return this.executeUpdate((String)entry.getKey(), (Object[])entry.getValue());
    }

    protected Map<String, Object[]> handleParameterMapping(String sql, Object entity) throws SQLException {
        Map<String, Object[]> mappingResult = new HashMap();
        List<String> placeholders = new ArrayList();
        int offset = 0;
        while(true) {
            int start = sql.indexOf("#{", offset);
            int i;
            if (start >= offset) {
                if (placeholders.size() > 0) {
                    Object[] params = new Object[placeholders.size()];

                    for(i = 0; i < placeholders.size(); ++i) {
                        String placeholder = (String)placeholders.get(i);
                        sql = sql.replaceFirst("\\#\\{" + placeholder + "\\}", "?");

                        try {
                            Method getter = entity.getClass().getMethod("get" + placeholder.substring(0, 1).toUpperCase() + placeholder.substring(1));
                            Object param = getter.invoke(entity);
                            params[i] = param;
                        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NullPointerException var12) {
                            throw new RuntimeException("无法为占位符 #{" + placeholder + "} 赋值！", var12);
                        }
                    }

                    System.out.println("===================================SQL: " + sql);
                    String ps = "[\n";
                    Object[] var15 = params;
                    int var16 = params.length;

                    for(int var17 = 0; var17 < var16; ++var17) {
                        Object p = var15[var17];
                        ps = ps + "\t" + p + "\n";
                    }

                    ps = ps + "]";
                    System.out.println("===================================Params: \n" + ps);
                    mappingResult.put(sql, params);
                } else {
                    mappingResult.put(sql, new Object[0]);
                }

                return mappingResult;
            }

            i = sql.indexOf("}", start);
            placeholders.add(sql.substring(start + 2, i));
            offset = i + 1;
        }
    }

    public <T> List<T> executeQuery(Class<T> clz, String sql, Object[] params) throws SQLException {
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        List var14;
        try {
            pstmt = DatabaseUtil.getConnection().prepareStatement(sql);
            if (params != null && params.length != 0) {
                for(int i = 0; i < params.length; ++i) {
                    pstmt.setObject(i + 1, params[i]);
                }
            }

            rs = pstmt.executeQuery();
            var14 = this.handleResultMapping(clz, rs);
        } catch (SQLException var11) {
            var11.printStackTrace();
            throw var11;
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | InstantiationException var12) {
            throw new RuntimeException("封装查询结果时出现错误！", var12);
        } finally {
            DatabaseUtil.closeStatement(pstmt, rs);
        }

        return var14;
    }

    public <T> List<T> executeQuery(Class<T> clz, String sql, Object entity) throws SQLException {
        Map<String, Object[]> mappingResult = this.handleParameterMapping(sql, entity);
        Map.Entry<String, Object[]> entry = (Map.Entry)mappingResult.entrySet().iterator().next();
        return this.executeQuery(clz, (String)entry.getKey(), (Object[])entry.getValue());
    }

    @SuppressWarnings("unchecked")
    protected <T> List<T> handleResultMapping(Class<T> clz, ResultSet rs) throws SQLException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        if (clz.equals(Integer.class)) {
            ArrayList result = new ArrayList();

            while(rs.next()) {
                result.add(rs.getInt(1));
            }

            return result;
        } else {
            ResultSetMetaData metaData = rs.getMetaData();
            int count = metaData.getColumnCount();
            Class[] types;
            int i;
            if (clz.equals(Object[].class)) {
                List<Object[]> result = new ArrayList();
                types = null;

                while(rs.next()) {
                    Object[] row = new Object[count];

                    for(i = 0; i < count; ++i) {
                        Class<?> targetClz = TypeConstant.getJavaType(metaData.getColumnTypeName(i + 1));
                        if (targetClz.equals(Date.class)) {
                            row[i] = new Date(rs.getTimestamp(i + 1).getTime());
                        } else {
                            row[i] = rs.getObject(i + 1, targetClz);
                        }
                    }

                    result.add(row);
                }

                //return result;
            } else {
                Method[] setters = new Method[count];
                types = new Class[count];
                i = 0;

                for(int j = 1; i < count; ++j) {
                    String lable = metaData.getColumnLabel(j);
                    String type = metaData.getColumnTypeName(j);

                    try {
                        setters[i] = clz.getMethod("set" + lable.substring(0, 1).toUpperCase() + lable.substring(1), TypeConstant.getJavaType(type));
                        types[i] = TypeConstant.getJavaType(type);
                    } catch (SecurityException | NoSuchMethodException var12) {
                        var12.printStackTrace();
                        setters[i] = null;
                        types[i] = null;
                    }

                    ++i;
                }

                List<T> result = new ArrayList();
                Object instance = null;

                while(rs.next()) {
                    instance = clz.newInstance();

                    for(int k = 0; k < count; ++k) {
                        if (setters[k] != null) {
                            if (types[k].equals(Date.class)) {
                                setters[k].invoke(instance, new Date(rs.getTimestamp(k + 1).getTime()));
                            } else {
                                setters[k].invoke(instance, rs.getObject(k + 1, types[k]));
                            }
                        }
                    }
                    result.add((T) instance);
                }

                return result;
            }
        }
        return null;
    }
}
