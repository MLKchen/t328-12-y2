package my.framework.dao;

import java.sql.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author CCB
 * date: 16/9/2022 下午7:36
 * Description: 描述
 */
public class DatabaseUtil {
    private static String driver = ConfigManager.getProperty("driver");
    private static String url = ConfigManager.getProperty("url");
    private static String user = ConfigManager.getProperty("user");
    private static String password = ConfigManager.getProperty("password");
    private static final ThreadLocal<Connection> threadLocal;

    public DatabaseUtil() {
    }

    public static Connection getConnection() throws SQLException {
        Connection connection = (Connection)threadLocal.get();
        if (connection == null || connection.isClosed()) {
            try {
                connection = DriverManager.getConnection(url, user, password);
                if (connection.getAutoCommit()) {
                    connection.setAutoCommit(false);
                }

                threadLocal.set(connection);
            } catch (SQLException var2) {
                var2.printStackTrace();
                throw var2;
            }
        }

        return connection;
    }

    public static void closeStatement(Statement stmt, ResultSet rs) {
        try {
            if (rs != null && !rs.isClosed()) {
                rs.close();
            }
        } catch (Exception var4) {
            var4.printStackTrace();
        }

        try {
            if (stmt != null && !stmt.isClosed()) {
                stmt.close();
            }
        } catch (Exception var3) {
            var3.printStackTrace();
        }

    }

    public static void closeConnection() {
        Connection connection = (Connection)threadLocal.get();
        threadLocal.set((Connection) null);

        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException var2) {
            var2.printStackTrace();
        }

    }

    static {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException var1) {
            var1.printStackTrace();
        }

        threadLocal = new ThreadLocal();
    }
}
