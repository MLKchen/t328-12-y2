package my.framework.dao;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author CCB
 * date: 16/9/2022 下午7:38
 * Description: 描述
 */
public class TypeConstant {
    private static Map<String, Class<?>> typeMap = new HashMap();

    public TypeConstant() {
    }

    public static void addType(String columnType, Class<?> javaType) {
        typeMap.put(columnType, javaType);
    }

    public static Class<?> getJavaType(String columnType) {
        return (Class)typeMap.get(columnType);
    }

    static {
        typeMap.put("BIGINT", Long.class);
        typeMap.put("INT", Integer.class);
        typeMap.put("VARCHAR", String.class);
        typeMap.put("TEXT", String.class);
        typeMap.put("DATETIME", Date.class);
        typeMap.put("DECIMAL", Double.class);
        typeMap.put("TINYINT", Integer.class);
        typeMap.put("BIT", Boolean.class);
        typeMap.put("TIMESTAMP", Date.class);
    }
}
