package my.framework.web;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @author CCB
 * date: 16/9/2022 下午8:16
 * Description: 描述
 */
public class DispatcherServlet {
    private static final long serialVersionUID = -3619491573769301637L;
    private String configFileName = "myweb.xml";
    private ControllerMappingManager ctrlMappingMgr;
    protected Map<String, Object> controllers = new HashMap();
    protected Map<String, Method> methods = new HashMap();

    public DispatcherServlet() {
    }

    public void init() throws ServletException {
        String configFileName = this.getServletConfig().getInitParameter("configFileName");
        if (configFileName != null && !(configFileName = configFileName.trim()).equals("")) {
            this.configFileName = configFileName;
        }

        this.ctrlMappingMgr = new ControllerMappingManager(this.configFileName);
        synchronized(this.controllers) {
            this.controllers.clear();
        }

        synchronized(this.methods) {
            this.methods.clear();
        }
    }

    public void destroy() {
        super.destroy();
        synchronized(this.controllers) {
            this.controllers.clear();
        }

        synchronized(this.methods) {
            this.methods.clear();
        }

        this.ctrlMappingMgr.getControllerMappings().clear();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        this.execute(request, response);
    }

    protected void execute(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = null;
        path = (String)request.getAttribute("javax.servlet.include.path_info");
        if (path == null) {
            path = request.getPathInfo();
            if (path == null || (path = path.trim()).isEmpty()) {
                path = (String)request.getAttribute("javax.servlet.include.servlet_path");
                if (path == null) {
                    path = request.getServletPath();
                }

                int slash = path.lastIndexOf("/");
                int period = path.lastIndexOf(".");
                if (period >= 0 && period > slash) {
                    path = path.substring(0, period);
                }
            }
        }

        System.out.println("=============================Path: " + path);
        if (!this.ctrlMappingMgr.containsKey(path)) {
            throw new PathException("No Controller & Method is mapped with this path : " + path);
        } else {
            ControllerMapping controllerMapping = this.ctrlMappingMgr.getControllerMapping(path);

            try {
                Object instance = null;
                synchronized(this.controllers) {
                    instance = this.controllers.get(controllerMapping.getClassName());
                    if (instance == null) {
                        instance = Class.forName(controllerMapping.getClassName()).newInstance();
                        this.controllers.put(controllerMapping.getClassName(), instance);
                    }
                }

                Method method = null;
                synchronized(this.methods) {
                    method = (Method)this.methods.get(path);
                    if (method == null) {
                        method = instance.getClass().getMethod(controllerMapping.getMethodName(), HttpServletRequest.class, HttpServletResponse.class);
                        this.methods.put(path, method);
                    }
                }

                Object result = method.invoke(instance, request, response);
                this.toView(result, request, response);
            } catch (InstantiationException var11) {
                throw new RuntimeException(var11);
            } catch (IllegalAccessException var12) {
                throw new RuntimeException(var12);
            } catch (ClassNotFoundException var13) {
                throw new RuntimeException(var13);
            } catch (NoSuchMethodException var14) {
                throw new RuntimeException(var14);
            } catch (InvocationTargetException var15) {
                throw new RuntimeException(var15);
            }
        }
    }

    protected void toView(Object result, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (result != null) {
            if (result instanceof String) {
                boolean isRedirect = false;
                String url = (String)result;
                if (url.startsWith("redirect:")) {
                    isRedirect = true;
                    url = url.substring("redirect:".length());
                } else if (url.startsWith("forward:")) {
                    url = url.substring("forward:".length());
                }

                if (!(url = url.trim()).startsWith("/")) {
                    throw new ViewPathException();
                }

                if (isRedirect) {
                    response.sendRedirect(request.getContextPath() + url);
                } else {
                    request.getRequestDispatcher(url).forward(request, response);
                }
            } else {
                PrintUtil.write(result, response);
            }

        }
    }
}
