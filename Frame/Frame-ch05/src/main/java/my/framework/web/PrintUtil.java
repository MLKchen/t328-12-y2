package my.framework.web;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author CCB
 * date: 16/9/2022 下午8:17
 * Description: 描述
 */
public class PrintUtil {
    public static void write(Object obj, HttpServletResponse response) throws IOException {
        response.setContentType("text/html; charset=utf-8");
        PrintWriter out = response.getWriter();
        String json = JSONObject.toJSONString(obj);
        out.print(json);
        out.flush();
        out.close();
    }
}
