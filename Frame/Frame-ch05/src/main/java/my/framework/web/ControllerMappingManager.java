package my.framework.web;

import javax.lang.model.element.Element;
import javax.swing.text.Document;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * @author CCB
 * date: 16/9/2022 下午8:08
 * Description: 描述
 */
public class ControllerMappingManager {
    private Map<String, ControllerMapping> controllerMappings = new HashMap();

    public ControllerMappingManager(String configFileName) {
        this.init(configFileName);
    }

    public void init(String configFileName) {
        InputStream is = this.getClass().getResourceAsStream("/" + configFileName);

        Document doc;
        try {
            doc = (new SAXReader()).read(is);
        } catch (DocumentException var17) {
            throw new ConfigLoadingException(var17);
        }

        Element root = doc.getRootElement();
        Iterator<Element> controllersIt = root.elements("controllers").iterator();
        Element controllersEl = (Element)controllersIt.next();
        Iterator controllerIt = controllersEl.elementIterator("controller");

        while(controllerIt.hasNext()) {
            Element controller = (Element)controllerIt.next();
            String firstPath = controller.attributeValue("path");
            String className = controller.attributeValue("class");
            if (className.isEmpty()) {
                throw new ConfigException("Controller的映射信息不能为空！");
            }

            ControllerMapping mapping = null;
            Iterator methodIt = controller.elementIterator("method");

            while(methodIt.hasNext()) {
                Element method = (Element)methodIt.next();
                String secondPath = method.attributeValue("path");
                String methodName = method.getText();
                if (methodName.isEmpty()) {
                    throw new ConfigException("方法的映射信息不能为空！");
                }

                String fullPath = (firstPath + secondPath).replaceAll("//", "/");
                if (this.controllerMappings.containsKey(fullPath)) {
                    mapping = (ControllerMapping)this.controllerMappings.get(fullPath);
                    throw new ConfigException("不能将路径 " + fullPath + " 映射到 " + className + "." + methodName + "()\n路径 " + fullPath + " 已被 " + mapping.getClassName() + "." + mapping.getMethodName() + "() 映射");
                }

                mapping = new ControllerMapping(className, methodName);
                this.controllerMappings.put(fullPath, mapping);
                System.out.println("================================map " + fullPath + " to " + className + "." + methodName + "()");
            }
        }

    }

    public ControllerMapping getControllerMapping(String path) {
        return (ControllerMapping)this.controllerMappings.get(path);
    }

    public boolean containsKey(String path) {
        return this.controllerMappings.containsKey(path);
    }

    public Map<String, ControllerMapping> getControllerMappings() {
        return this.controllerMappings;
    }
}
