package com.ccb.shili.dao;

/**
 * @author CCB
 * date: 12/9/2022 下午12:42
 * Description: 买家业务接口
 */
public interface Buyer {
    /**
     * 查看房屋
     * @return 反馈
     */
    public String havealook();

    public String test();
}
