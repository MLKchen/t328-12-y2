package com.ccb.shili.dao;

import java.lang.reflect.Proxy;

/**
 * @author CCB
 * date: 13/9/2022 下午3:37
 * Description: 描述
 */
public class IntermediaryJdkProxyFactory {

    public static <T> T create(Object target){
        IntermediaryInvocationHandler handler = new IntermediaryInvocationHandler();
        handler.setTarget(target);
        return (T) Proxy.newProxyInstance(target.getClass().getClassLoader(),target.getClass().getInterfaces(),handler);
    }
}
