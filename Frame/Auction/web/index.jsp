<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: MLK
  Date: 15/9/2022
  Time: 上午10:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="js/jquery-3.5.1.js" type="text/javascript"></script>
<html>
  <head>
    <title>用户信息</title>
  </head>
  <body style="background-color: antiquewhite">
  <div style="text-align: center;margin-bottom: 15px">
    <input type="text" placeholder="输入查询的姓名或者工号" name="search">
    <input type="button" value="搜索">
    <input type="button" value="新增" onclick="javascript:window.location='add.jsp'">
  </div>
  <table border="1" cellspacing="1" style="margin: auto;width: 360px">
    <tr>
      <th>id</th>
      <th id="we">名称</th>
      <th>工号</th>
      <th>操作</th>
    </tr>
    <c:forEach var="user" items="${requestScope.userList}">
      <tr>
        <td>${user.id}</td>
        <td>${user.username}</td>
        <td>${user.userNO}</td>
        <td>
          <a href="javascript:void(0)" onclick="modify(${user.id})">修改</a>
          &nbsp;&nbsp;
          <a href="javascript:void(0)" onclick="del(this,${user.id})">删除</a>
        </td>
      </tr>
    </c:forEach>
  </table>
  </body>
<script>
  function modify(id) {
    window.location='modify?id='+id;
  }
  function del(obj,id) {
    var aa = $("#id").text();
    alert(aa)
    $.ajax({
      url:"del",
      data:"id="+id,
      success:function (data) {
        if (data==1){
          $(obj).parents("tr").remove();
        }
      },
      error:function () {
        alert("ajax请求失败！")
      }
    })
  }
</script>
</html>
