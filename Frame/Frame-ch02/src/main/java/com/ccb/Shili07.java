package com.ccb;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.Iterator;

/**
 * @author CCB
 * date: 7/9/2022 下午4:57
 * Description: 描述
 */
public class Shili07 {
    public static void main(String[] args) {
        Document doc = null;
        try {
            //加载DOM树
            SAXReader saxParser = new SAXReader();
            doc = saxParser.read(new File("Frame/Frame-ch02/src/main/java/com/ccb/shili02.xml"));
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        //获取XML的根目录
        Element root = doc.getRootElement();
        //遍历所有的Brand标签
        for (Iterator iterator = root.elementIterator();iterator.hasNext();){
            Element brand = (Element)iterator.next();
            //输出Type标签
            for (Iterator itType = brand.elementIterator();itType.hasNext();){
                Element type = (Element) itType.next();
                //输出标签的name值
                System.out.println("\t型号"+type.attributeValue("name"));
            }
        }
    }
}
