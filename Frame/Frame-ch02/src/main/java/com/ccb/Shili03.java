package com.ccb;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * @author CCB
 * date: 7/9/2022 下午3:55
 * Description: 描述
 */
public class Shili03 {
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
        //得到DOM解析器的工厂实例
        DocumentBuilderFactory bdf = DocumentBuilderFactory.newInstance();
        //从DOM工厂获得DOM解析器
        DocumentBuilder db = bdf.newDocumentBuilder();
        //解析XML文档，得到一个Document对象，即DOM树
        Document doc = db.parse("Frame/Frame-ch02/src/main/java/com/ccb/shili02.xml");
        //读取pubDate
        NodeList list = doc.getElementsByTagName("pubDate");
        //pubDate元素节点
        Element pubDateElement = (Element) list.item(0);
        //读取文本节点
        String pubDate = pubDateElement.getFirstChild().getNodeValue();
        System.out.println(pubDate);
    }
}
