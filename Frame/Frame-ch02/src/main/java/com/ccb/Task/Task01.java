package com.ccb.Task;

import org.dom4j.io.SAXReader;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author CCB
 * date: 8/9/2022 上午11:34
 * Description: Frame-XML-TASK
 */
public class Task01 {
    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException, TransformerException {
        //得到DOM解析器的工厂实例
        DocumentBuilderFactory bdf = DocumentBuilderFactory.newInstance();
        //从DOM工厂获得DOM解析器
        DocumentBuilder db = bdf.newDocumentBuilder();
        //解析XML文档，得到一个Document对象，即DOM树
        Document doc = db.parse("Frame/Frame-ch02/src/main/resources/xml/mybatis-config.xml");
        //读取properties
        NodeList propertieslist = doc.getElementsByTagName("properties");
        //properties元素节点
        Element propertiesElement = (Element) propertieslist.item(0);
        //读取文本节点
        String properties = propertiesElement.getAttribute("resource");
        System.out.println("properties的resource属性："+properties);
        //读取setting
        NodeList settinglist = doc.getElementsByTagName("setting");
        //setting元素节点
        Element settingElement = (Element) settinglist.item(0);
        //读取文本节点
        String settingName = settingElement.getAttribute("name");
        String settingValue = settingElement.getAttribute("value");
        System.out.println("setting的name:"+settingName+"\tvalue:"+settingValue);
        //遍历property的name属性
        NodeList dataSourcelist = doc.getElementsByTagName("dataSource");
        NodeList propertylist = dataSourcelist.item(0).getChildNodes();
        System.out.print("\n");
        for (int i = 0;i<propertylist.getLength();i++){
            Node propertyNode = propertylist.item(i);
            if(propertyNode.getNodeType()==3){
                continue;
            }
            Element propertyElement= (Element) propertyNode;
            System.out.println("name:"+propertyElement.getAttribute("name")+"\tvalue:"+propertyElement.getAttribute("value"));
        }
        System.out.print("\n");
        NodeList mapperlist = doc.getElementsByTagName("mapper");
        for (int i = 0;i<mapperlist.getLength();i++){
            Node mapperNode = mapperlist.item(i);
            Element mapperElement= (Element) mapperNode;
            System.out.println("resource:"+mapperElement.getAttribute("resource"));
        }
    }
}
