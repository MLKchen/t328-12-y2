package com.ccb.Task;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;

/**
 * @author CCB
 * date: 8/9/2022 下午4:46
 * Description: 描述
 */
public class Student02 {
    static Document doc = null;
    public static void main(String[] args) {
        try {
            SAXReader saxReader = new SAXReader();
            doc = saxReader.read(new File("Frame/Frame-ch02/src/main/resources/xml/Student.xml"));
        }catch (Exception e){
            e.printStackTrace();
        }
        Element root = doc.getRootElement();
       //lement student = root.element("student");
        System.out.println(root.getStringValue());
    }
}
