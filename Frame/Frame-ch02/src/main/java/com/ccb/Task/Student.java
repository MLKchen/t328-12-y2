package com.ccb.Task;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author CCB
 * date: 8/9/2022 下午3:32
 * Description: 描述
 */
public class Student {
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException, TransformerException {
        //jianda02();
        jianda03();
    }

    /**
    * @author CCB
    * date: 2022-09-08 下午4:20
    * Description: 简答02
    */
    public static void jianda02() throws ParserConfigurationException, IOException, SAXException {
        //得到DOM解析器的工厂实例
        DocumentBuilderFactory bdf = DocumentBuilderFactory.newInstance();
        //从DOM工厂获得DOM解析器
        DocumentBuilder db = bdf.newDocumentBuilder();
        //解析XML文档，得到一个Document对象，即DOM树
        Document doc = db.parse("Frame/Frame-ch02/src/main/resources/xml/Student.xml");
        //读取student
        NodeList studentlist = doc.getElementsByTagName("student");
        NodeList namelist = doc.getElementsByTagName("name");
        NodeList courselist = doc.getElementsByTagName("course");
        NodeList scorelist = doc.getElementsByTagName("score");
        for (int i =0;i<studentlist.getLength();i++){
            Node studentNode = studentlist.item(i);
            Node Node = studentlist.item(i);
            String nameNode = namelist.item(i).getTextContent();
            String coursetNode = courselist.item(i).getTextContent();
            String scoreNode = scorelist.item(i).getTextContent();
            if(studentNode.getNodeType()==3){
                continue;
            }
            Element studentElement = (Element) Node;
            System.out.println("id:"+studentElement.getAttribute("id"));
            System.out.println("name:"+nameNode+"\tcourse:"+coursetNode+"\tscore:"+scoreNode);
        }
    }

    public static void jianda03() throws TransformerException, IOException, ParserConfigurationException, SAXException {
        //得到DOM解析器的工厂实例
        DocumentBuilderFactory bdf = DocumentBuilderFactory.newInstance();
        //从DOM工厂获得DOM解析器
        DocumentBuilder db = bdf.newDocumentBuilder();
        //解析XML文档，得到一个Document对象，即DOM树
        Document doc = db.parse("Frame/Frame-ch02/src/main/resources/xml/Student.xml");
        //找到删除的节点

        NodeList student = doc.getElementsByTagName("student");
        NodeList score = doc.getElementsByTagName("score");
        for (int i =0;i<student.getLength();i++){
            Node studentNode = student.item(i);
            if(studentNode.getNodeType()==3){
                continue;
            }
            Element studentElement = (Element) studentNode;
            String id = studentElement.getAttribute("id");

            if(id.equals("1")){
                score.item(i).setTextContent(null);
            }
            if(id.equals("2")){
                score.item(i).setTextContent("60");
            }
        }
        Element student3 = doc.createElement("student");
        student3.setAttribute("id","3");
        Element name3 = doc.createElement("name");
        name3.setTextContent("王五");
        Element course3 = doc.createElement("course");
        course3.setTextContent("语文");
        Element score3 = doc.createElement("score");
        score3.setTextContent("145");
        student3.appendChild(name3);
        student3.appendChild(course3);
        student3.appendChild(score3);
        Element scores = (Element) doc.getElementsByTagName("scores").item(0);
        scores.appendChild(student3);
        //保存XML文件
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(doc);
        //设置编码类型
        transformer.setOutputProperty(OutputKeys.ENCODING,"UTF-8");
        StreamResult result = new StreamResult(new FileOutputStream("Frame/Frame-ch02/src/main/resources/xml/Student.xml"));
        //把DOM树转换为XML文件
        transformer.transform(domSource,result);
    }
}
