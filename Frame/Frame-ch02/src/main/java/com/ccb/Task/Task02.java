package com.ccb.Task;

import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.Iterator;

/**
 * @author CCB
 * date: 8/9/2022 下午3:15
 * Description: 使用DOM4J 对Mapper.xml解析文件解析
 */
public class Task02 {
    public static void main(String[] args) {
        Document doc = null;
        try {
            SAXReader saxReader = new SAXReader();
            doc = (Document) saxReader.read(new File("Frame/Frame-ch02/src/main/resources/xml/UserMapper.xml"));
        }catch (Exception e){
            e.printStackTrace();
        }
        Element root = doc.getRootElement();
        for (Iterator iterator = root.elementIterator();iterator.hasNext();){
            Element element = (Element) iterator.next();
            System.out.println("id:"+element.attributeValue("id")+"\tparameterType:"+element.attributeValue("parameterType")+"\tresultType:"+element.attributeValue("resultType")+"\n");
        }
    }
}
