package com.ccb;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

/**
 * @author CCB
 * date: 7/9/2022 下午4:30
 * Description: 描述
 */
public class Testshili01 {
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
        //得到DOM解析器的工厂实例
        DocumentBuilderFactory bdf = DocumentBuilderFactory.newInstance();
        //从DOM工厂获得DOM解析器
        DocumentBuilder db = bdf.newDocumentBuilder();
        //解析XML文档，得到一个Document对象，即DOM树
        Document doc = db.parse("Frame/Frame-ch02/src/main/java/com/ccb/shili01.xml");
        //读取pubDate
        NodeList list = doc.getElementsByTagName("Brand");
        //循环Brand信息
        for (int i=0;i<list.getLength();i++) {
            Node brand = (Element) list.item(i);
            Element element= (Element) brand;
            //读取文本节点
            String attrValue = element.getAttribute("name");
            //获取第一个i个Brand元素的所有子元素的name属性值
            NodeList types = element.getChildNodes();
            for (int j=0;j<types.getLength();j++){
                Element typeElement = (Element) types.item(j);
                    String type = typeElement.getAttribute("name");
                    System.out.println("手机：" + attrValue + type);
            }
        }
    }
}
