package com.ccb.service;

import com.ccb.entity.Employee;

/**
 * @author CCB
 * date: 13/9/2022 下午4:04
 * Description: 描述
 */
public interface EmployeeSevice {
    public Employee getEmployee();
}
