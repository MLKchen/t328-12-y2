package com.ccb.entity;

/**
 * @author CCB
 * date: 13/9/2022 下午3:50
 * Description: 描述
 */
public class Employee {
    private String employeeNo;
    private String name;
    private String email;

    public String getEmployeeNo() {
        return employeeNo;
    }

    public void setEmployeeNo(String employeeNo) {
        this.employeeNo = employeeNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
