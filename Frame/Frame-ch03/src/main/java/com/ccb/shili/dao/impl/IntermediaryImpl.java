package com.ccb.shili.dao.impl;

import com.ccb.shili.dao.Buyer;
import org.apache.log4j.Logger;

/**
 * @author CCB
 * date: 12/9/2022 下午12:46
 * Description: 描述
 */
public class IntermediaryImpl implements Buyer {
    private Logger logger = Logger.getLogger(IntermediaryImpl.class);

    /**
     * 被代理的目标对象
     */
    private Buyer target;

    public IntermediaryImpl(Buyer target) {
        this.target = target;
    }

    @Override
    public String havealook() {
        before();
        String feedback = target.havealook();
        after();
        return "看房记录：买家反馈“"+feedback+"“";
    }

    public void before(){
        logger.debug("前期准备。");
        logger.debug("查看房源。");
        logger.debug("和买家沟通时间。");
    }

    public void after(){
        logger.debug("后期跟踪");
        logger.debug("和买家沟通意见。");
    }
}
