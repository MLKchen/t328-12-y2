package com.ccb.shili.dao.impl;

import org.apache.log4j.Logger;

/**
 * @author CCB
 * date: 12/9/2022 下午12:57
 * Description: 示例7
 */
public class IntermediarySubclass extends RealBuyer {
    private Logger logger = Logger.getLogger(IntermediaryImpl.class);

    @Override
    public String havealook() {
        before();
        String feedback = super.havealook();
        after();
        return "看房记录：买家反馈”"+feedback+"“";
    }

    public void before(){
        logger.debug("前期准备。");
        logger.debug("查看房源。");
        logger.debug("和买家沟通时间。");
    }

    public void after(){
        logger.debug("后期跟踪");
        logger.debug("和买家沟通意见。");
    }
}
