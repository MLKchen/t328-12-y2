package com.ccb.shili.dao.impl;

import net.sf.cglib.proxy.MethodProxy;
import org.apache.log4j.Logger;

import java.lang.reflect.Method;

/**
 * @author CCB
 * date: 12/9/2022 下午1:28
 * Description: 描述
 */
public class IntermediaryMethodInterceptor implements MethodInterceptor{
    private org.apache.log4j.Logger logger = Logger.getLogger(IntermediaryMethodInterceptor.class);
/*
    @Override
    public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) {
        before();
        Object feedback = null;
        //feedback = methodProxy.invokeSuper(proxy,args);
        after();
        return "看房记录：买家反馈“"+feedback+"“";
    }*/

    @Override
    public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) {
        return null;
    }

    public void before(){
        logger.debug("前期准备。");
        logger.debug("查看房源。");
        logger.debug("和买家沟通时间。");
    }

    public void after(){
        logger.debug("后期跟踪");
        logger.debug("和买家沟通意见。");
    }
}
