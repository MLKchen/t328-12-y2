package com.ccb.shili.dao.impl;

import net.sf.cglib.proxy.Enhancer;

/**
 * @author CCB
 * date: 12/9/2022 下午1:34
 * Description: 描述
 */
public class IntermediaryCglibProxyFactory {
    //IntermediaryCglibProxyFactory中没有会引发线程安全问题的成员变量
    //故可声明为公共变量
    private static IntermediaryMethodInterceptor callback = new IntermediaryMethodInterceptor();

    public static <T> T create(Class<T> target){
        Enhancer enhancer = new Enhancer();
        //enhancer.setCallback(callback);
        enhancer.setSuperclass(target);
        return (T) enhancer.create();
    }
}
