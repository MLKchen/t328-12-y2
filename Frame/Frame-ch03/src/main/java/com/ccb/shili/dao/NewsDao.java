package com.ccb.shili.dao;

import com.ccb.shili.entity.News;

/**
 * @author CCB
 * date: 11/9/2022 下午6:34
 * Description: 新闻模块的Dao接口
 */
public interface NewsDao {
    /**
     * 保存新闻信息的方法
     */
    public void save(News news);
}
