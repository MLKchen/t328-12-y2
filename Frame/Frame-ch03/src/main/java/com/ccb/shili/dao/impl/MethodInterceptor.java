package com.ccb.shili.dao.impl;

import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @author CCB
 * date: 12/9/2022 下午1:24
 * Description: 描述
 */
public interface MethodInterceptor {
    Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy);
}
