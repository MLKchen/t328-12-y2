package com.ccb.shili.service.impl;

import com.ccb.shili.dao.NewsDao;
import com.ccb.shili.entity.News;
import com.ccb.shili.service.NewsService;

/**
 * @author CCB
 * date: 11/9/2022 下午6:41
 * Description: 描述
 */
public class NewsServiceImpl implements NewsService {
    //实例化所依赖的NewsDao对象
    private NewsDao dao;

    public void setDao(NewsDao dao) {
        this.dao = dao;
    }

    public void save(News news) {
        //调用NewsDao的方法保存新闻信息
        dao.save(news);
    }
}
