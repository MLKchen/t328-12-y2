package com.ccb.shili.dao.impl;

import com.ccb.shili.dao.NewsDao;
import com.ccb.shili.entity.News;

/**
 * @author CCB
 * date: 11/9/2022 下午7:00
 * Description: 用于演示
 */
public class NewsDaoResisImpl  implements NewsDao {
    public void save(News news) {
        System.out.println("Resis==>已保存至数据库");
    }
}
