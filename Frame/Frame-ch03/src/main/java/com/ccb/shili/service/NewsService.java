package com.ccb.shili.service;

import com.ccb.shili.entity.News;

/**
 * @author CCB
 * date: 11/9/2022 下午6:41
 * Description: 描述
 */
public interface NewsService {
    /**
     * 保存新闻信息的方法
     */
    public void save(News news);
}
