package com.ccb.shili.dao.impl;

import org.apache.log4j.Logger;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @author CCB
 * date: 12/9/2022 下午1:06
 * Description: 描述
 */
public class IntermediaryInvocationHandler implements InvocationHandler {
    private Logger logger = Logger.getLogger(IntermediaryInvocationHandler.class);

    /**
     * 被代理目标对象
     */
    private Object target;

    public void setTarget(Object target) {
        this.target = target;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws InvocationTargetException, IllegalAccessException {
        before();
        Object feedbock = method.invoke(target,args);
        after();
        return "看房记录：买家反馈”"+feedbock+"”";
    }

    public void before(){
        logger.debug("前期准备。");
        logger.debug("查看房源。");
        logger.debug("和买家沟通时间。");
    }

    public void after(){
        logger.debug("后期跟踪");
        logger.debug("和买家沟通意见。");
    }
}
