package com.ccb.shili.service.factory;

import com.ccb.shili.dao.NewsDao;
import com.ccb.shili.dao.impl.NewsDaoMySqlImpl;

/**
 * @author CCB
 * date: 11/9/2022 下午6:47
 * Description: 创建NewsDao的实体类-Examples-01
 */
public class SimpleDaoFactory {
    /**
     * 创建NewsDao实例的工厂方法
     */
    public static NewsDao getInstance(String key){
        switch (key){
            case "mysql":
                return new NewsDaoMySqlImpl();
            case "oracle":
                return new NewsDaoMySqlImpl();
            case "resid":
                return new NewsDaoMySqlImpl();
            default:
                throw new RuntimeException("无效的数据库类型："+key+",DAO获取失败");
        }
    }
}
