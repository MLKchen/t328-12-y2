package com.ccb.shili.dao.impl;

import com.ccb.shili.dao.NewsDao;
import com.ccb.shili.entity.News;
import org.apache.log4j.Logger;

/**
 * @author CCB
 * date: 11/9/2022 下午6:38
 * Description: 描述
 */
public class NewsDaoImpl implements NewsDao {
    private Logger logger = Logger.getLogger(NewsDaoImpl.class);
    public void save(News news) {
        logger.debug("已保存至数据库");
    }
}
