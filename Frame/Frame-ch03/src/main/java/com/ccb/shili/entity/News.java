package com.ccb.shili.entity;

/**
 * @author CCB
 * date: 11/9/2022 下午6:36
 * Description: 新闻实体类
 */
public class News {
    private String title;//标题
    private String content;//内容
    private String time;//时间

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
