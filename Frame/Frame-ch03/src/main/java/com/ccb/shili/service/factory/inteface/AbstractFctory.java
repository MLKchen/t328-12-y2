package com.ccb.shili.service.factory.inteface;

import com.ccb.shili.dao.NewsDao;

/**
 * @author CCB
 * date: 12/9/2022 下午12:33
 * Description: 抽象工厂接口
 */
public interface AbstractFctory {
    public NewsDao getInstance();
}
