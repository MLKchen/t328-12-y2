package com.ccb.shili.dao.impl;

import java.lang.reflect.Proxy;

/**
 * @author CCB
 * date: 12/9/2022 下午1:21
 * Description: 描述
 */
public class IntermediaryJdkProxyFactory {
    public static <T> T create(Object target){
        //IntermediaryInvocationHandler的成员变量target存在线程安全问题，
        //故将IntermediaryInvocationHandler作为局部变量使用。
        IntermediaryInvocationHandler handler = new IntermediaryInvocationHandler();
        handler.setTarget(target);
        return (T) Proxy.newProxyInstance(target.getClass().getClassLoader(),target.getClass().getInterfaces(),handler);
    }
}
