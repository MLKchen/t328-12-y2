package com.ccb.shili.service.factory;

import com.ccb.shili.dao.NewsDao;
import com.ccb.shili.dao.impl.NewsDaoMySqlImpl;
import com.ccb.shili.service.factory.inteface.AbstractFctory;

/**
 * @author CCB
 * date: 12/9/2022 下午12:34
 * Description: 生产NewsDaoMySqlImpl实例工厂
 */
public class MySqlDaoFactory implements AbstractFctory {
    @Override
    public NewsDao getInstance() {
        return new NewsDaoMySqlImpl();
    }
}
