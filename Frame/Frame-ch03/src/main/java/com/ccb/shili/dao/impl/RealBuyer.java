package com.ccb.shili.dao.impl;

import com.ccb.shili.dao.Buyer;
import org.apache.log4j.Logger;

/**
 * @author CCB
 * date: 12/9/2022 下午12:44
 * Description: 描述
 */
public class RealBuyer implements Buyer {
    private Logger logger = Logger.getLogger(RealBuyer.class);
    @Override
    public String havealook() {
        logger.debug("实地查看一下");
        return "一些意见";
    }
}
