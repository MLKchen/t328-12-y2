package com.ccb.dao;

import com.ccb.entity.Employee;

/**
 * @author CCB
 * date: 13/9/2022 下午4:02
 * Description: 员工接口
 */
public interface EmployeeDao {
    public Employee getEmployee();
}
