import com.ccb.shili.dao.NewsDao;
import com.ccb.shili.entity.News;
import com.ccb.shili.service.factory.MySqlDaoFactory;
import com.ccb.shili.service.factory.SimpleDaoFactory;
import com.ccb.shili.service.factory.inteface.AbstractFctory;
import com.ccb.shili.service.impl.NewsServiceImpl;
import org.junit.Test;

/**
 * @author CCB
 * date: 11/9/2022 下午6:54
 * Description: 测试-设计模式示例
 */
public class Demo {

    @Test//示例3-test
    public void Demo01(){
//        NewsDao dao = SimpleDaoFactory.getInstance();
//        NewsServiceImpl service = new NewsServiceImpl();
//        service.setDao(dao);
    }

    @Test//示例4-test
    public void Demo02(){
        NewsDao dao = SimpleDaoFactory.getInstance("mysql");
        NewsServiceImpl service = new NewsServiceImpl();
        service.setDao(dao);
        News news = new News();
        dao.save(news);
    }

    @Test
    public void Demo03(){
        AbstractFctory fctory = new MySqlDaoFactory();
        NewsDao dao = fctory.getInstance();
        News news = new News();
        dao.save(news);
    }
}
