import com.ccb.shili.dao.Buyer;
import com.ccb.shili.dao.impl.*;
import org.apache.log4j.Logger;
import org.junit.Test;

/**
 * @author CCB
 * date: 12/9/2022 下午12:52
 * Description: 代理模式---关于购房系统
 */
public class Demo02 {
    private Logger logger = Logger.getLogger(Demo02.class);
    @Test
    public void Demo01(){
        Buyer buyer = new IntermediaryImpl(new RealBuyer());
        String result = buyer.havealook();
        logger.debug(result);
    }

    @Test
    public void Demo02(){
        RealBuyer buyer = new IntermediarySubclass();
        String result = buyer.havealook();
        logger.debug(result);
    }

    @Test
    public void Demo03(){
        Buyer buyer = IntermediaryJdkProxyFactory.create(new RealBuyer());
        String result = buyer.havealook();
        logger.debug(result);
    }

    @Test
    public void Demo04(){
        RealBuyer buyer = IntermediaryCglibProxyFactory.create(RealBuyer.class);
        String result = buyer.havealook();
        logger.debug(result);
    }
}
