package com.ccb.dao.impl;

import com.ccb.dao.StorageRecordDao;
import com.ccb.entity.StorageRecord;
import org.mybatis.spring.SqlSessionTemplate;

/**
 * @author CCB
 * date: 5/10/2022 上午10:07
 * Description: 描述
 */
public class StorageRecordDaoImpl implements StorageRecordDao {

    private SqlSessionTemplate sqlSession;

    @Override
    public int addRecord(StorageRecord storageRecord) {
        return sqlSession.insert("com.ccb.dao.StorageRecordDao.addRecord",storageRecord);
    }

    @Override
    public int delRecord(StorageRecord storageRecord) {
        return sqlSession.delete("com.ccb.dao.StorageRecordDao.delRecord",storageRecord);
    }

    @Override
    public int updateRecord(StorageRecord storageRecord) {
        return sqlSession.update("com.ccb.dao.StorageRecordDao.updateRecord",storageRecord);
    }

    public SqlSessionTemplate getSqlSession() {
        return sqlSession;
    }

    public void setSqlSession(SqlSessionTemplate sqlSession) {
        this.sqlSession = sqlSession;
    }
}
