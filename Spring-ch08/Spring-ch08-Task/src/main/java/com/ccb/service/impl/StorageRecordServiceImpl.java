package com.ccb.service.impl;

import com.ccb.dao.StorageRecordDao;
import com.ccb.entity.StorageRecord;
import com.ccb.service.StorageRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author CCB
 * date: 5/10/2022 上午10:29
 * Description: 描述
 */
@Service("StorageRecordServiceImpl")
public class StorageRecordServiceImpl implements StorageRecordService {

    @Autowired
    private StorageRecordDao storageRecordDao;

    int result = 0;

    @Override
    public int addRecord(StorageRecord storageRecord) {
        try{
            if (storageRecordDao.addRecord(storageRecord)==1){
                result = 1;
            }
        }catch (RuntimeException e){
            e.printStackTrace();
            throw e;
        }
        return result;
    }

    @Override
    public int delRecord(StorageRecord storageRecord) {
        try{
            if (storageRecordDao.delRecord(storageRecord)==1){
                result = 1;
            }
        }catch (RuntimeException e){
            e.printStackTrace();
            throw e;
        }
        return result;
    }

    @Override
    public int updateRecord(StorageRecord storageRecord) {
        try{
            if (storageRecordDao.updateRecord(storageRecord)==1){
                result = 1;
            }
        }catch (RuntimeException e){
            e.printStackTrace();
            throw e;
        }
        return result;
    }

    public StorageRecordDao getStorageRecordDao() {
        return storageRecordDao;
    }

    public void setStorageRecordDao(StorageRecordDao storageRecordDao) {
        this.storageRecordDao = storageRecordDao;
    }
}
