package com.ccb.service;

import com.ccb.entity.StorageRecord;

/**
 * @author CCB
 * date: 5/10/2022 上午10:03
 * Description: 入库记录表的方法接口
 */
public interface StorageRecordService {
    /**
     * 新增入库记录
     * @param storageRecord
     * @return
     */
    int addRecord(StorageRecord storageRecord);

    /**
     * 删除入库记录
     * @param storageRecord
     * @return
     */
    int delRecord(StorageRecord storageRecord);

    /**
     * 修改入库记录
     * @param storageRecord
     * @return
     */
    int updateRecord(StorageRecord storageRecord);
}
