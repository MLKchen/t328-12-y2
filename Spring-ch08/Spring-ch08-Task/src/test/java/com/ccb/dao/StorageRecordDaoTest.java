package com.ccb.dao;

import com.ccb.entity.StorageRecord;
import com.ccb.service.StorageRecordService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.*;

public class StorageRecordDaoTest {

    @Test
    public void addRecord() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        StorageRecordService sysroleService = (StorageRecordService) ctx.getBean("StorageRecordServiceImpl");
        StorageRecord storageRecord = new StorageRecord();
        storageRecord.setSrCode("99");
        sysroleService.addRecord(storageRecord);
    }

    @Test
    public void delRecord() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        StorageRecordService sysroleService = (StorageRecordService) ctx.getBean("StorageRecordService");
        StorageRecord storageRecord = new StorageRecord();
        storageRecord.setId(22);
        sysroleService.delRecord(storageRecord);
    }

    @Test
    public void updateRecord() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        StorageRecordService sysroleService = (StorageRecordService) ctx.getBean("StorageRecordService");
        StorageRecord storageRecord = new StorageRecord();
        storageRecord.setId(21);
        storageRecord.setGoodsName("测试");
        sysroleService.delRecord(storageRecord);
    }
}