package com.ccb.dao.SysUser;

import com.ccb.pojo.Supplier;
import com.ccb.pojo.SysUser;
import com.ccb.pojo.Sysrole;
import com.ccb.service.SupplierService;
import com.ccb.service.SysUSerService;
import com.ccb.service.SysroleService;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class SysUserMapperTest {
    private Logger logger = Logger.getLogger(SysUserMapperTest.class);

    @Before
    public void setUp() throws Exception{}

    @Test
    public void selectSysUserList() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SysUSerService uSerService = (SysUSerService) ctx.getBean("SysUSerServiceImpl");
        List<SysUser> userList = new ArrayList<SysUser>();
        SysUser userCondition = new SysUser();
        userCondition.setRealName("赵");
        userCondition.setRoleId(2);
        userList = uSerService.getList(userCondition);
        for (SysUser userResult : userList){
            logger.info("testGetUserList account:"
            +userResult.getAccount()+"  and realName:"
            +userResult.getRealName()+"  and roleId"
            +userResult.getRoleId()+"  and roleName"
            +userResult.getRoleIdName()+"  and address:"
            +userResult.getAddress());
        }
    }


    @Test
    public void testAddUser() throws ParseException{
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SysUSerService uSerService = (SysUSerService) ctx.getBean("SysUSerServiceImpl");
        SysUser user = new SysUser();
        user.setAccount("Spring");
        user.setRealName("测试");
        user.setPassword("123456");
        user.setSex(1);
        user.setPhone("12345678910");
        boolean result = uSerService.add(user);
        logger.info("testAdd result:"+result);
    }

    @Test
    public void selectSupplierList(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SupplierService supplierService = (SupplierService) ctx.getBean("supplierService");
        Supplier supplier = new Supplier();
        supplier.setSupName("公司");
        List<Supplier> supplierList = supplierService.selectSupplierList(supplier);
        for (Supplier suppliers : supplierList){
            System.out.println("供货商编号:"+suppliers.getSupCode()+"\t供货商名称:"+suppliers.getSupName()+"\t供货商联系人:"+suppliers.getSupPhone());
        }
    }

    @Test
    public void addSupplier(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SupplierService supplierService = (SupplierService) ctx.getBean("supplierService");
        Supplier supplier = new Supplier();
        supplier.setSupCode("BJ_GYS099");
        supplier.setSupName("湖南趣买买食用油有限公司");
        supplier.setSupDesc("测试");
        supplier.setSupPhone("12345678910");
        boolean result = supplierService.addSupplier(supplier);
        logger.info("testAdd result:"+result);
    }

    @Test
    public void removeSupplier(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SupplierService supplierService = (SupplierService) ctx.getBean("supplierService");
        Supplier supplier = new Supplier();
        supplier.setId(19);
        boolean result = supplierService.removeSupplier(supplier);
        logger.info("testAdd result:"+result);
    }

    @Test
    public void selectSysroleList(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SysroleService sysroleService = (SysroleService) ctx.getBean("sysroleService");
        Sysrole sysrole = new Sysrole();
        sysrole.setRoleName("店");
        List<Sysrole> sysroleList = sysroleService.selectSysroleList(sysrole);
        for (Sysrole sysrole1 : sysroleList){
            System.out.println(sysrole1.getRoleName()+"\t"+sysrole1.getCode());
        }
    }

    @Test
    public void addSysrole(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SysroleService sysroleService = (SysroleService) ctx.getBean("sysroleService");
        Sysrole sysrole = new Sysrole();
        sysrole.setCode("ABCDEFG");
        sysrole.setRoleName("测试");
        boolean result = sysroleService.addSysrole(sysrole);
        logger.info("testAdd result:"+result);
    }
}