package com.ccb.pojo;

import java.util.Date;

/**
 * @author CCB
 * date: 24/9/2022 下午4:40
 * Description: 用户
 */
public class SysUser {
    private Integer id;     //ID
    private String account; //用户编码
    private String realName;//用户名称
    private String password;//用户密码
    private Integer sex;    //性别
    private Date birthday;  //出生日期
    private String phone;   //电话
    private String address; //地址
    private Integer roleId; //角色ID
    private Integer createdUserId;  //创建者
    private Date createdTime;       //创建时间
    private Integer updateUserId;   //更新者
    private Date updateTime;        //更新时间
    private Integer age;            //年龄

    private String roleIdName;      //角色名称

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Integer createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Integer getUpdateUserId() {
        return updateUserId;
    }

    public void setUpdateUserId(Integer updateUserId) {
        this.updateUserId = updateUserId;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getRoleIdName() {
        return roleIdName;
    }

    public void setRoleIdName(String roleIdName) {
        this.roleIdName = roleIdName;
    }
}
