package com.ccb.service.impl;

import com.ccb.dao.SysUserMapper;
import com.ccb.dao.SysroleMapper;
import com.ccb.pojo.Sysrole;
import com.ccb.service.SysroleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author CCB
 * date: 27/9/2022 下午5:02
 * Description: 描述
 */
@Service("sysroleService")
public class SysroleServiceImpl implements SysroleService {

    @Autowired
    private SysroleMapper sysroleMapper;

    @Override
    public List<Sysrole> selectSysroleList(Sysrole sysrole) {
        return sysroleMapper.selectSysroleList(sysrole);
    }

    @Override
    public boolean addSysrole(Sysrole sysrole) {
        boolean result = false;
        try {
            if(sysroleMapper.addSysrole(sysrole)==true){
                result = true;
                //测试事务回滚，打开注释
//                throw new RuntimeException();
            }
        }catch (RuntimeException e){
//            e.printStackTrace();
            System.out.println("新增失败！");
            throw e;
        }
        return result;
    }

    public SysroleMapper getSysroleMapper() {
        return sysroleMapper;
    }

    public void setSysroleMapper(SysroleMapper sysroleMapper) {
        this.sysroleMapper = sysroleMapper;
    }
}
