package com.ccb.service.impl;


import com.ccb.dao.SysUserMapper;
import com.ccb.pojo.Supplier;
import com.ccb.pojo.SysUser;

import com.ccb.service.SysUSerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author CCB
 * date: 26/9/2022 上午9:23
 * Description: 用户模块业务层实现
 */
@Scope("prototype")
@Service("SysUSerServiceImpl")
public class SysUSerServiceImpl implements SysUSerService {
    @Autowired
    private SysUserMapper sysUserMapper;

    @Override
    public List<SysUser> getList(SysUser sysUser) {
        try {
            return sysUserMapper.selectSysUserList(sysUser);
        }catch (RuntimeException e){
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public boolean add(SysUser sysUser) {
        boolean result = false;
        try {
            if(sysUserMapper.add(sysUser)==true){
                result = true;
                //测试事务回滚，打开注释
//                throw new RuntimeException();
            }
        }catch (RuntimeException e){
//            e.printStackTrace();
            System.out.println("新增失败！");
            throw e;
        }
        return result;
    }




    public SysUserMapper getSysUserMapper() {
        return sysUserMapper;
    }

    public void setSysUserMapper(SysUserMapper sysUserMapper) {
        this.sysUserMapper = sysUserMapper;
    }
}
