package com.ccb.service.impl;


import com.ccb.dao.SupplierMapper;
import com.ccb.pojo.Supplier;

import com.ccb.service.SupplierService;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author CCB
 * date: 27/9/2022 下午3:27
 * Description: 描述
 */
@Transactional
@Service("supplierService")
public class SupplierServiceImpl implements SupplierService {
    @Autowired
    private SupplierMapper supplierMapper;

    @Override
    public List<Supplier> selectSupplierList(Supplier supplier) {
        return supplierMapper.selectSupplierList(supplier);
    }

    @Override
    public boolean addSupplier(Supplier supplier) {
        boolean result = false;
        try {
            if(supplierMapper.addSupplier(supplier)==true){
                result = true;
                //测试事务回滚，打开注释
//                throw new RuntimeException();
            }
        }catch (RuntimeException e){
//            e.printStackTrace();
            System.out.println("新增失败！");
            throw e;
        }
        return result;
    }

    @Override
    public boolean removeSupplier(Supplier supplier) {
        boolean result = false;
        try {
            if(supplierMapper.removeSupplier(supplier)==true){
                result = true;
                //测试事务回滚，打开注释
//                throw new RuntimeException();
            }
        }catch (RuntimeException e){
//            e.printStackTrace();
            System.out.println("删除失败！");
            throw e;
        }
        return result;
    }

    public SupplierMapper getSupplierMapper() {
        return supplierMapper;
    }

    public void setSupplierMapper(SupplierMapper supplierMapper) {
        this.supplierMapper = supplierMapper;
    }
}
