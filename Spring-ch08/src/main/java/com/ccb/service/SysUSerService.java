package com.ccb.service;

import com.ccb.pojo.Supplier;
import com.ccb.pojo.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CCB
 * date: 26/9/2022 上午9:22
 * Description: 用户模块业务接口
 */
public interface SysUSerService {
    //查询用户
    List<SysUser> getList(SysUser sysUser);

    //保存用户
    boolean add(SysUser sysUser);
}
