package com.ccb.dao;

import com.ccb.pojo.Supplier;

import java.util.List;

/**
 * @author CCB
 * date: 27/9/2022 下午3:27
 * Description: 描述
 */
public interface SupplierMapper {
    //查询全部供货商信息
    List<Supplier> selectSupplierList(Supplier supplier);

    //添加供货商
    boolean addSupplier(Supplier supplier);

    //根据ID删除
    boolean removeSupplier(Supplier supplier);
}
