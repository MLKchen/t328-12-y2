package com.ccb.dao.Impl;


import com.ccb.dao.SupplierMapper;
import com.ccb.pojo.Supplier;
import org.mybatis.spring.support.SqlSessionDaoSupport;

import java.util.List;

/**
 * @author CCB
 * date: 27/9/2022 下午3:27
 * Description: 描述
 */
public class SupplierMapperImpl extends SqlSessionDaoSupport implements SupplierMapper {
    @Override
    public List<Supplier> selectSupplierList(Supplier supplier) {
        return null;
    }

    @Override
    public boolean addSupplier(Supplier supplier) {
        return false;
    }

    @Override
    public boolean removeSupplier(Supplier supplier) {
        return false;
    }
}
