package com.ccb.dao.Impl;


import com.ccb.dao.SysUserMapper;
import com.ccb.pojo.Supplier;
import com.ccb.pojo.SysUser;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author CCB
 * date: 26/9/2022 上午9:01
 * Description: 描述
 */
public class SysUserMapperImpl extends SqlSessionDaoSupport implements SysUserMapper {
    private SqlSessionTemplate sqlSession;

    private SysUserMapper sysUserMapper;

    //查询用户列表
    @Override
    public List<SysUser> selectSysUserList(SysUser sysUser) {
        return getSqlSession().selectList("com.ccb.dao.SysUser.SysUserService.selectSysUserList",sysUser);
//        return null;
    }

    @Override
    public boolean add(SysUser sysUser) {
        return true;
    }

    public SqlSessionTemplate getSqlSession() {
        return sqlSession;
    }

    public void setSqlSession(SqlSessionTemplate sqlSession) {
        this.sqlSession = sqlSession;
    }
}
