package com.ccb.dao;

import com.ccb.pojo.Supplier;
import com.ccb.pojo.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CCB
 * date: 26/9/2022 上午8:28
 * Description: 描述
 */
public interface SysUserMapper {
    //查询用户
    List<SysUser> selectSysUserList(SysUser sysUser);

    //增加用户
    boolean add(SysUser sysUser);


}
