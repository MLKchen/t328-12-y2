package com.ccb.dao;

import com.ccb.pojo.Sysrole;

import java.util.List;

/**
 * @author CCB
 * date: 27/9/2022 下午4:47
 * Description: 描述
 */
public interface SysroleMapper {

    //模糊查询角色表
    List<Sysrole> selectSysroleList(Sysrole sysrole);

    //新增角色表
    boolean addSysrole(Sysrole sysrole);
}
