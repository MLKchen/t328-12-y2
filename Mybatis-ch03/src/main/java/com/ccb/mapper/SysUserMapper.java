package com.ccb.mapper;

import com.ccb.shili.entity.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author CCB
 * date: 29/8/2022 上午9:50
 * Description: 用户操作接口
 */
public interface SysUserMapper {


    public List<SysUser> selectList(@Param("realName") String realName,@Param("roleId") Integer roleId);

    /**
     * 查询对应信息
     * @param realName
     * @param roleId
     * @param account
     * @param createdTime
     */
    public List<SysUser> GetUserListByCoose(@Param("realName") String realName,@Param("roleId") Integer roleId,@Param("account") String account,@Param("createdTime") Date createdTime);

    /**
     * 根据角色ID数组查询用户列表信息------使用数组
     * @param roleIds
     * @return
     */
    public List<SysUser> getUserByRoleIdArray(Integer[] roleIds);

    /**
     * 根据角色ID集合 查询用户列表信息------使用集合
     * @param roleList
     * @return
     */
    public List<SysUser> getUserByRoleIdList(List<Integer> roleList);

    /**
     * 根据角色ID集合 查询用户列表信息------使用map
     * @param roleMap
     * @return
     */
    public List<SysUser> getUserByRoleIdMap(Map<String,Object> roleMap);

    /**
     * 根据id修改用户信息
     * @param sysUser
     */
    public int updateUser(SysUser sysUser);

    public List<SysUser> selectPageList(@Param("realName") String realName,@Param("roleId") Integer roleId,@Param("pageBegin") Integer pageBegin,@Param("pageSize")Integer pageSize);

}
