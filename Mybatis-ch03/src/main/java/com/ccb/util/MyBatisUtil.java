package com.ccb.util;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * @Description TODO
 * @Author hyl
 * @Date 2022/8/25 10:46
 **/
public class MyBatisUtil {
	static SqlSessionFactory factory;

	static {
		//1、配置mybatis文件
		String resource ="mybatis-config.xml";
		InputStream is = null;
		try {
			is = Resources.getResourceAsStream(resource);
			factory = new SqlSessionFactoryBuilder().build(is);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static SqlSession getSqlSession(){
		return factory.openSession(false); //关闭自动提交事务
	}

	public static void closeSqlSession(SqlSession sqlSession){
		if(sqlSession != null) {
			sqlSession.close();
		}
	}

}

