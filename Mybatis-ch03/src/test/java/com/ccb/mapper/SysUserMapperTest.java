package com.ccb.mapper;

import com.ccb.shili.entity.SysUser;
import com.ccb.util.MyBatisUtil;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;
import org.omg.PortableInterceptor.INACTIVE;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.*;

public class SysUserMapperTest extends MyBatisUtil  {

    @Test
    public void selectList() {
        String realName = null;
        Integer roleId = null;
        SqlSession session = null;
        try{
            session = getSqlSession();
            List<SysUser> userlist = session.getMapper(SysUserMapper.class).selectList(realName,roleId);
            for (SysUser sysUser : userlist){
                System.out.println(sysUser.getRealName()+"\n");
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
        //SqlSession sqlSession = getSqlSession();
    }

    @Test
    public void GetUserListByCoose(){
        SqlSession sqlSession = getSqlSession();
        List<SysUser> userList = new ArrayList<>();
        try {
            String realName = "";
            Integer roleId = null;
            String account = "";
            Date createdTime = new SimpleDateFormat("yyyy-MM-dd").parse("2022-11-11");
            userList = sqlSession.getMapper(SysUserMapper.class).GetUserListByCoose(realName,roleId,account,createdTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }finally {
            closeSqlSession(sqlSession);
        }
        System.out.println("查询到的用户数量："+userList.size());
        for (SysUser user : userList){
            System.out.println("查询到的用户信息："+user.getRealName());
        }
    }

    @Test
    public void getUserByRoleIdArray(){
        SqlSession sqlSession  =getSqlSession();
        Integer[] roleId = {1,2};
        List<SysUser> userList = new ArrayList<>();
        try {
            userList = sqlSession.getMapper(SysUserMapper.class).getUserByRoleIdArray(roleId);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            closeSqlSession(sqlSession);
        }
        System.out.println("查询到的用户数量："+userList.size());
        for(SysUser sysUser : userList){
            System.out.println("查询到用户信息："+sysUser.getRealName());
        }
    }


    @Test
    public void getUserByRoleIdList(){
        SqlSession sqlSession  =getSqlSession();
        List<Integer> roleList = new ArrayList<Integer>();
        roleList.add(1);
        roleList.add(2);
        List<SysUser> userList = new ArrayList<>();
        try {
            userList = sqlSession.getMapper(SysUserMapper.class).getUserByRoleIdList(roleList);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            closeSqlSession(sqlSession);
        }
        System.out.println("查询到的用户数量："+userList.size());
        for(SysUser sysUser : userList){
            System.out.println("查询到用户信息："+sysUser.getRealName());
        }
    }

    @Test
    public void getUserByRoleIdMap(){
        SqlSession sqlSession  =getSqlSession();
        List<Integer> roleList = new ArrayList<Integer>();
        roleList.add(1);
        roleList.add(2);
        Map<String,Object> roleMap = new HashMap<>();
        roleMap.put("roleIdList",roleList);

        List<SysUser> userList = new ArrayList<>();
        try {
            userList = sqlSession.getMapper(SysUserMapper.class).getUserByRoleIdMap(roleMap);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            closeSqlSession(sqlSession);
        }
        System.out.println("查询到的用户数量："+userList.size());
        for(SysUser sysUser : userList){
            System.out.println("查询到用户信息："+sysUser.getRealName());
        }
    }

    @Test
    public void updateUser(){
        SqlSession sqlSession = getSqlSession();
        Integer userId = 31;
        int count = 0;
        try {
            SysUser sysUser = new SysUser();
            sysUser.setId(userId);
            sysUser.setRealName("测试用户修改test");
            sysUser.setUpdatedUserId(1);
            sysUser.setUpdatedTime(new Date());
            count = sqlSession.getMapper(SysUserMapper.class).updateUser(sysUser);
            sqlSession.commit();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            closeSqlSession(sqlSession);
        }
        System.out.println("修改id="+userId+"用户修改"+(count==0?"失败":"成功"));
    }

    @Test
    public void selectPageList(){
        SqlSession sqlSession = getSqlSession();
        List<SysUser> userList = new ArrayList<>();

        try {
            String realName = "";
            Integer roleId = 2;
            Integer pageIndex = 2;
            Integer pageSize = 2;
            Integer pageBegin = (pageIndex - 1) * pageSize;//计算查询起始位置
            userList = sqlSession.getMapper(SysUserMapper.class).selectPageList(realName,roleId,pageBegin,pageSize);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            closeSqlSession(sqlSession);
        }
        System.out.println("查询到用户数量："+userList.size());
        for (SysUser sysUser : userList){
            System.out.println("查询到的用户信息："+sysUser.getRealName());
        }
    }
}