import com.spring.HelloSpring;
import com.spring.dao.Printer;
import com.spring.entity.User;
import com.spring.service.UserService;
import com.task.entity.Equip;
import com.task.entity.Player;
import com.task.entity.Task01;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


/**
 * @author CCB
 * date: 20/9/2022 上午10:29
 * Description: 描述
 */
public class Spring {
    @Test
    public void testHelloSpring(){
        //通过applicationContext.xml实例化Spring上下文
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        //通过applicationContext的个Bean()方法，根据id来获取bean的实例
        HelloSpring helloSpring = (HelloSpring)context.getBean("helloSpring");
        //执行print（）方法
        helloSpring.print();
    }

    @Test
    public void printerTest(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Printer printer = (Printer)context.getBean("printer");
        String content = "轻灵..轻灵的鼠标一点神奇的世界出现双手..把双手放在胸前" +
                "美丽的画卷轻灵..轻灵的鼠标一点神奇的世界出现双手..把双手放在胸前" +
                "敲打出美丽的画卷是谁让IT生活多彩绚烂青鸟在我们身边纵然是IT时代风云变幻" +
                "青鸟青鸟与我们相伴飞翔吧青鸟将知识在神州大地撒遍飞翔吧青鸟让教育改变我们的生活--";
        printer.print(content);
    }

    @Test
    public void aopTest(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserService service = (UserService)context.getBean("userService");

        User user = new User();
        user.setUsername("CCB");
        user.setPassword("123456");
        service.saveUser(user);
    }

    @Test
    public void Task01(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Task01 Task01 = (Task01)context.getBean("Task01");
        Task01.print();
    }

    @Test//简答3--（4）
    public void Taskplus01(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Equip Ring = (Equip) context.getBean("Ring");
        System.out.println(Ring.getName()+"[速度增效："+Ring.getSpeedPlus()+"攻击增效："+Ring.getAttackPlus()+"防御增效："+Ring.getDefencePlus()+"]");
    }

    @Test//简答3--（5）
    public void Taskplus02(){
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Player player = (Player) context.getBean("zhangsan");

        Equip equip = new Equip();
        equip.setName("紫色梦幻"+player.getRing().getName());
        equip.setAttackPlus(player.getRing().getAttackPlus() + 6);
        equip.setSpeedPlus(player.getRing().getSpeedPlus());
        equip.setDefencePlus(player.getRing().getDefencePlus() + 6);
        equip.setType(player.getArmet().getType());

        System.out.println(player.getRing().getName());
        player.updateEquip(equip);
    }
}