package com.ccb.entity;

/**
 * @author CCB
 * date: 20/9/2022 上午10:02
 * Description: 描述
 */
public class User {
    private String UserName;
    private String UserNo;

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserNo() {
        return UserNo;
    }

    public void setUserNo(String userNo) {
        UserNo = userNo;
    }
}
