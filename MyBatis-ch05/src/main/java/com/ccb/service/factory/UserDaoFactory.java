package com.ccb.service.factory;

import com.ccb.dao.UserDao;
import com.ccb.dao.impl.UserDaoImpl;

/**
 * @author CCB
 * date: 20/9/2022 上午10:13
 * Description: 用户DA哦工厂，负责用户DAO实例的创建工作
 */
public class UserDaoFactory {
    //负责创建用户DAO实例的方法
    public static UserDao getInstance(){
        UserDao dao = new UserDaoImpl();
        return  dao;
    }
}
