package com.ccb.service;

import com.ccb.entity.User;

/**
 * @author CCB
 * date: 20/9/2022 上午10:01
 * Description: 用户模块数层接口
 */
public interface UserService {
    /**
     * 保存用户信息
     * @param user
     */
    public void sava(User user);
}
