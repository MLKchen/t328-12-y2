package com.spring.dao;

/**
 * @author CCB
 * date: 20/9/2022 下午2:07
 * Description: 打印机程序，面向接口编程不是具体实现类，不是具体实现类
 */
public class Printer {
    private Ink ink = null;
    private Paper paper = null;

    /**
     * 打印的方法
     * @param message 要打印的内容
     */
    public void print(String message){
        //输出颜色标记
        System.out.println("使用"+ink.getColor(255,200,0));
        //逐字输出到纸张
        for (int i=0;i<message.length();++i){
            paper.putInChar(message.charAt(i));
        }
        //将纸张内容输出
        System.out.println(paper.getContext());
    }

    /**
     * 设值注入所需的setter方法
     * @param ink 墨盒
     */
    public void setInk(Ink ink) {
        this.ink = ink;
    }

    /**
     * 设值注入所需的setter方法
     * @param paper 纸张
     */
    public void setPaper(Paper paper) {
        this.paper = paper;
    }
}
