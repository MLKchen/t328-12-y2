package com.spring.dao;

/**
 * @author CCB
 * date: 20/9/2022 下午2:05
 * Description: 墨盒接口
 */
public interface Ink {

    /**
     * 配置颜色的方法
     * @param red
     * @param green
     * @param blue
     * @return
     */
    public String getColor(int red,int green,int blue);
}
