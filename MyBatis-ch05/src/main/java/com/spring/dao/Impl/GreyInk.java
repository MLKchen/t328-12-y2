package com.spring.dao.Impl;

import com.spring.dao.Ink;

import java.awt.*;

/**
 * @author CCB
 * date: 20/9/2022 下午2:33
 * Description: 灰色墨盒
 */
public class GreyInk implements Ink {
    /**
     * 打印灰色文字
     */
    @Override
    public String getColor(int red, int green, int blue) {
        int c = (red+green+blue)/3;
        Color color = new Color(c);
        return "#"+Integer.toHexString(color.getRGB()).substring(2);
    }
}
