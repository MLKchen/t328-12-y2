package com.spring.dao.Impl;

import com.spring.dao.Paper;

/**
 * @author CCB
 * date: 20/9/2022 下午2:16
 * Description: 描述
 */
public class TextPaper implements Paper {
    private int charPerLine = 16;   //每行字符树
    private int linePerpage = 5;    //每页行数
    private String content = "";    //纸张中的内容
    private int posX = 0;   //当前横向位置，从0到charPerLine-1
    private int posY = 0;   //当前行数，从0到LinePerPage-1
    private int posP = 1;   //当前页数

    @Override
    public String getContext() {
        String ret = this.content;
        //补齐本页空行，并显示页码
        if(!(posX == 0 && posY == 0)){
            int count = linePerpage - posY;
            for (int i = 0;i<count;++i){
                ret += "==第"+posP+"页 ==";
            }
        }
        return ret;
    }

    //使用set方法注入每行字符数
    public void setCharPerLine(int charPerLine) {
        this.charPerLine = charPerLine;
    }

    //使用set方法注入每页行数
    public void setLinePerpage(int linePerpage) {
        this.linePerpage = linePerpage;
    }

    @Override
    public void putInChar(char c) {
        content += c;
        ++posX;
        //是否换行
        if(posX == charPerLine){
            content += Paper.newline;
            posX = 0;
            ++posY;
        }
        //判断是否翻页
        if(posY == linePerpage){
            content += "== 第"+posP+"页 ==";
            content += Paper.newline + Paper.newline;
            posY = 0;
            ++posP;
        }
    }
}
