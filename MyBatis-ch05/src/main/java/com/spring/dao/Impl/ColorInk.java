package com.spring.dao.Impl;

import com.spring.dao.Ink;

import java.awt.*;

/**
 * @author CCB
 * date: 20/9/2022 下午2:30
 * Description: 彩色墨盒
 */
public class ColorInk implements Ink {
    @Override
    public String getColor(int red, int green, int blue) {
        Color color = new Color(red,green,blue);
        return "#"+Integer.toHexString(color.getRGB()).substring(2);
    }
}
