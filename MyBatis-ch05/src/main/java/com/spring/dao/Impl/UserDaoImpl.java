package com.spring.dao.Impl;

import com.spring.dao.UserDao;
import com.spring.entity.User;
import org.apache.log4j.Logger;

/**
 * @author CCB
 * date: 20/9/2022 下午3:13
 * Description: 描述
 */
public class UserDaoImpl implements UserDao {

    private Logger logger = Logger.getLogger(UserDaoImpl.class);
    @Override
    public void saveUser(User user) {
        System.out.println("数据保存至数据库");
    }
}
