package com.spring.dao;

/**
 * @author CCB
 * date: 20/9/2022 下午2:03
 * Description: 纸张接口
 */
public interface Paper {
    public static final String newline = "\r\n";

    /**
     * 输出一个字符到纸张
     * @param c
     */
    public void putInChar(char c);

    /**
     * 获取纸张上的内容
     * @return
     */
    public String getContext();
}
