package com.spring.dao;

import com.spring.entity.User;

/**
 * @author CCB
 * date: 20/9/2022 下午3:12
 * Description: 用户接口
 */
public interface UserDao {
    public void saveUser(User user);
}
