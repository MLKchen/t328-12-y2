package com.spring.service;

import com.spring.entity.User;

/**
 * @author CCB
 * date: 20/9/2022 下午3:09
 * Description: 用户服务层
 */
public interface UserService {
    /**
     * 保存用户信息
     * @param user
     */
    public void saveUser(User user);
}
