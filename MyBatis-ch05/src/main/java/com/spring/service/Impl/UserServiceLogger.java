package com.spring.service.Impl;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;

import java.util.Arrays;

/**
 * @author CCB
 * date: 20/9/2022 下午3:16
 * Description: 提取公共日志代码
 */
public class UserServiceLogger {
    private static final Logger log = Logger.getLogger(UserServiceLogger.class);

    /**
     * 前置代码
     * @param jp
     */
    public void before(JoinPoint jp){
        log.info("调用"+jp.getTarget()+"的"+jp.getSignature().getName()+"方法.方法入参："+ Arrays.toString(jp.getArgs()));
    }

    public void afterReturning(JoinPoint jp,Object result){
        log.info("调用"+jp.getTarget()+"的"+jp.getSignature().getName()+"方法.方法返回值："+ result);
    }
}
