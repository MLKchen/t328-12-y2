package com.spring.service.Impl;

import com.spring.entity.User;
import com.spring.service.UserService;
import com.spring.dao.UserDao;

/**
 * @author CCB
 * date: 20/9/2022 下午3:11
 * Description: 用户业务类
 */
public class UserServiceImpl implements UserService {

    private UserDao userDao;

    /**
     * 保存用户
     * @param user
     */
    @Override
    public void saveUser(User user) {
        //调用用户DAO的方法保存用户信息
        userDao.saveUser(user);
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
}
