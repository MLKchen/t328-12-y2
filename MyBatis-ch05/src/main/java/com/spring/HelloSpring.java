package com.spring;

/**
 * @author CCB
 * date: 20/9/2022 上午10:34
 * Description: 描述
 */
public class HelloSpring {
    private String hello = null;

    public void print(){
        System.out.println("Spring say:,"+this.getHello()+"!");
    }

    public String getHello(){
        return hello;
    }

    public void setHello(String hello) {
        this.hello = hello;
    }
}
