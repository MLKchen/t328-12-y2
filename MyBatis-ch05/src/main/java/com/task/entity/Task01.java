package com.task.entity;

/**
 * @author CCB
 * date: 22/9/2022 上午11:46
 * Description: 描述
 */
public class Task01 {
    private String lufe;
    private String kongming;

    public void print(){
        System.out.println("路飞:"+lufe+"!");
        System.out.println("诸葛亮:"+this.kongming+"!");
    }

    public String getLufe() {
        return lufe;
    }

    public void setLufe(String lufe) {
        this.lufe = lufe;
    }

    public String getKongming() {
        return kongming;
    }

    public void setKongming(String kongming) {
        this.kongming = kongming;
    }
}
