package com.ccb.dao;

import com.ccb.pojo.SysUser;
import com.ccb.service.SysUserService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class SysUserDaoTest {

    @org.junit.Test
    public void selectSysUserList() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SysUserService uSerService = (SysUserService) ctx.getBean("SysUserService");
        List<SysUser> userList = new ArrayList<SysUser>();
        userList = uSerService.selectSysUserList("a");
        SysUser sysUser = new SysUser();
        System.out.println(userList.size());
    }

    @org.junit.Test
    public void add() {
    }

    @org.junit.Test
    public void del() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SysUserService uSerService = (SysUserService) ctx.getBean("SysUserService");
        System.out.println(uSerService.del(50));
    }

    @org.junit.Test
    public void modify() {
    }
}