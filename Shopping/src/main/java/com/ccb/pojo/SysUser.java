package com.ccb.pojo;

/**
 * @author CCB
 * date: 9/10/2022 下午3:34
 * Description: 描述
 */
public class SysUser {
    private Integer id;	                //主键ID
    private String account;				//账号
    private String password;			//密码
    private Integer sex;				//性别
    private String phone;				//手机号码
    private String address;			    //用户地址

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
