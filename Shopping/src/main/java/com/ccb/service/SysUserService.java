package com.ccb.service;

import com.ccb.pojo.SysUser;

import java.util.List;

/**
 * @author CCB
 * date: 9/10/2022 下午3:36
 * Description: 描述
 */
public interface SysUserService {
    //查询用户
    List<SysUser> selectSysUserList(String account);

    //增加用户
    boolean add(SysUser sysUser);

    //删除
    boolean del(Integer id);

    //修改用户
    boolean modify(SysUser sysUser);
}
