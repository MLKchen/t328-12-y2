package com.ccb.service;

import com.ccb.pojo.SysUser;
import org.apache.ibatis.annotations.Param;

/**
 * @author CCB
 * date: 11/10/2022 下午3:51
 * Description: 描述
 */
public interface LoginService {
    /**
     * 获取登录用户信息
     * @param account
     * @param password
     * @return
     */
    SysUser selectSysUser(@Param("account") String account, @Param("password") String password);
}
