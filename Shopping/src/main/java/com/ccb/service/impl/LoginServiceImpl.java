package com.ccb.service.impl;

import com.ccb.dao.Login;
import com.ccb.pojo.SysUser;
import com.ccb.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author CCB
 * date: 11/10/2022 下午3:52
 * Description: 描述
 */
@Service("LoginService")
public class LoginServiceImpl implements LoginService {

    @Autowired
    private Login login;

    @Override
    public SysUser selectSysUser(String account, String password) {
        return login.selectSysUser(account,password);
    }
}
