package com.ccb.service.impl;

import com.ccb.dao.SysUserDao;
import com.ccb.pojo.SysUser;
import com.ccb.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ccb.dao.SysUserDao;

import java.util.List;

/**
 * @author CCB
 * date: 9/10/2022 下午3:52
 * Description: 描述
 */
@Service("SysUserService")
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private SysUserDao SysUserDao;

    @Override
    public List<SysUser> selectSysUserList(String account) {
        return SysUserDao.selectSysUserList(account);
    }

    @Override
    public boolean add(SysUser sysUser) {
        boolean result = false;
        try {
            if(SysUserDao.add(sysUser)==true){
                result = true;
                //测试事务回滚，打开注释
//                throw new RuntimeException();
            }
        }catch (RuntimeException e){
//            e.printStackTrace();
            System.out.println("新增失败！");
            throw e;
        }
        return result;
    }

    @Override
    public boolean del(Integer id) {
        boolean result = false;
        try {
            if(SysUserDao.del(id)==true){
                result = true;
                //测试事务回滚，打开注释
//                throw new RuntimeException();
            }
        }catch (RuntimeException e){
//            e.printStackTrace();
            System.out.println("删除失败！");
            throw e;
        }
        return result;
    }

    @Override
    public boolean modify(SysUser sysUser) {
        boolean result = false;
        try {
            if(SysUserDao.modify(sysUser)==true){
                result = true;
                //测试事务回滚，打开注释
//                throw new RuntimeException();
            }
        }catch (RuntimeException e){
//            e.printStackTrace();
            System.out.println("修改失败！");
            throw e;
        }
        return result;
    }
}
