package com.ccb.controller;

import com.ccb.pojo.SysUser;
import com.ccb.service.LoginService;
import com.ccb.service.SysUserService;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

/**
 * @author CCB
 * date: 11/10/2022 下午3:58
 * Description: 登录控制类
 */
@Controller
@RequestMapping("")
public class LoginController {
    Logger logger = Logger.getLogger(SysUserController.class);
    ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

    @RequestMapping("/login")
    public String login(Map<String,Object> map,String account,String password){
        logger.debug("111111111");
        LoginService loginService = (LoginService) ctx.getBean("LoginService");
        SysUser mysysUser = loginService.selectSysUser(account,password);
        if (mysysUser!=null){
            map.put("mysysUser",mysysUser);
            return "index";
        }else {
            return "redirect:index";
        }
    }
}
