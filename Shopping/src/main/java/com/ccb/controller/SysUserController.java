package com.ccb.controller;

import com.ccb.pojo.SysUser;
import com.ccb.service.SysUserService;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author CCB
 * date: 9/10/2022 下午4:08
 * Description: 用户模块控制类
 */
@Controller
@RequestMapping("")
public class SysUserController {
    Logger logger = Logger.getLogger(SysUserController.class);
    ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

    /**
     * 条获取全部信息全部
     * @param map
     * @param account
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/getAll")
    public String getAll(Map<String,Object> map, String account) throws Exception{
        SysUserService uSerService = (SysUserService) ctx.getBean("SysUserService");
        List<SysUser> userList = new ArrayList<SysUser>();
        SysUser sysUsers = new SysUser();
        userList = uSerService.selectSysUserList(account);
        ModelAndView modelAndView = new ModelAndView();
        map.put("UserList",userList);
        map.put("account",account);
        return "index";
    }

    /**
    * @author CCB
    * date: 2022-10-11 下午2:41
    * Description: 条件查询
    */
    @GetMapping(value = "/getAll")
    public String getAll(Map<String,Object> map) throws Exception{
        SysUserService uSerService = (SysUserService) ctx.getBean("SysUserService");
        List<SysUser> userList = new ArrayList<SysUser>();
        userList = uSerService.selectSysUserList("");
        ModelAndView modelAndView = new ModelAndView();
        map.put("UserList",userList);
        return "index";
    }

    /**
    * @author CCB
    * date: 2022-10-11 下午2:41
    * Description: 删除对应信息
    */
    @RequestMapping(value = "/del")
    public String delUser(Integer id, HttpServletResponse response){
        SysUserService uSerService = (SysUserService) ctx.getBean("SysUserService");
        boolean result = uSerService.del(id);
        String end = "0";
        if(result==true){
            end = "1";
        }
        //设置响应格式
        response.setCharacterEncoding("utf-8");
        try {
            response.getWriter().print(end);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
