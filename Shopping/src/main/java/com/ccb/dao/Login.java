package com.ccb.dao;

import com.ccb.pojo.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CCB
 * date: 11/10/2022 下午3:49
 * Description: 描述
 */
public interface Login {
    /**
     * 获取登录用户信息
     * @param account
     * @param password
     * @return
     */
    SysUser selectSysUser(@Param("account") String account,@Param("password") String password);
}
