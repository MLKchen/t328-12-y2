<%--
  Created by IntelliJ IDEA.
  User: MLK
  Date: 9/10/2022
  Time: 下午4:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<div style="width: 600px;margin: auto">
    <form method="post" action="${pageContext.request.contextPath}/getAll">
        <input name="account" type="text" value="${account}" placeholder="请在此输入查询的昵称">
        <input type="submit">
    </form>
    <table border="1" style="margin: auto">
        <tr>
            <th>ID</th>
            <th>昵称</th>
            <th>地址</th>
            <th>电话</th>
            <th>操作</th>
        </tr>
        <c:forEach items="${UserList}" var="userList">
            <tr>
                <td>${userList.id}</td>
                <td>${userList.account}</td>
                <td>${userList.address}</td>
                <td>${userList.phone}</td>
                <td><a href="javascript:void (0)"><span onclick="del(this,${userList.id})">删除</span></a></td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script type="text/javascript">
    function del(obj,id) {
        alert(11)
        $.ajax({
            url:"${pageContext.request.contextPath}/del",
            data:"id="+id,
            success:function (data) {
                if (data!="0"){
                    alert("删除成功！");
                    $(obj).parents("tr").remove();
                }else {
                    alert("删除失败！")
                }
            },
            error:function () {
                alert("ajax请求失败！")
            }
        })
    }
</script>
</html>
