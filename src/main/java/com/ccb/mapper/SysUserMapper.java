package com.ccb.mapper;

import com.ccb.shili.entity.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author CCB
 * date: 2022/8/24 17:58
 * Description: 用户表接口
 */
public interface SysUserMapper {
    /**
     * 统计用户数量的方法
     * @return
     */
    public int count();

    /**
     * 查询用户列表
     */
    public List<SysUser> getUserList();

    /**
     * 根据用户的真实姓名模糊查询匹配用户，参数realName为需要传递给SQL语句的查询条件
     * @param realName
     * @return
     */
    public List<SysUser> getUserByRealName(String realName);

    /**
     * 查询用户列表
     * @param sysUser
     * @return
     * 使用实体类 SysUser 传参
     */
    public List<SysUser> getUserListByPojo(SysUser sysUser);

    /**
     * 查询用户列表
     * @param userMap
     * @return
     * 使用 Map集合 注释传参
     */
    public List<SysUser> getUserListByMap(Map<String,Object> userMap);

    /**
     *
     * @param realName
     * @param roleId
     * @return
     * 使用 @Param 注释 传参
     */
    public List<SysUser> getUSerListByParms(@Param("realName") String realName,@Param("roleId") int roleId);

    /**
     * 查询用户列表，包括角色名称
     * @param sysUser
     * @return
     */
    public List<SysUser> getUserListWithRoleName(SysUser sysUser);

    /**
     * 根据角色ID查询用户，包含系统角色实例
     * @param roleId
     * @return
     */
    public List<SysUser> getUserListByRoleId(@Param("roleId") Integer roleId);

    /**
     * 根据用户id查询用户及相关地址
     * @param userId
     * @return
     */
    public List<SysUser> getUserAddressesByUserId(@Param("userId") Integer userId);

    /**
     * 添加用户
     * @param user
     * @return int 影响的行数
     */
    public int add(SysUser user);

    /**
     * 修改用户
     * @param sysUser
     * @return int 影响行数
     */
    public int modify(SysUser sysUser);

    /**
     * 修改个人密码
     * @param id
     * @param pwd
     * @return
     */
    public int updatePwd(@Param("id") Integer id,@Param("password") String pwd);

    /**
     * 删除用户
     * @param id
     * @return
     */
    public int deleteUserById(@Param("id") Integer id);
}
