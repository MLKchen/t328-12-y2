package com.ccb.mapper;

import com.ccb.shili.entity.SysSupplier;

import java.util.List;

/**
 * @author CCB
 * date: 25/8/2022 下午3:20
 * Description: 关于商家信息/商家表接口
 */
public interface SysSupplierMapper {

    /**
     * 获取全部商家信息
     * @return 返回全部商家信息
     */
    public List<SysSupplier> getAllSupplier(SysSupplier sysSupplier);

    /**
     * 增加供货商
     * @return
     */
    public int addSupplier(SysSupplier sysSupplier);

    /**
     * 修改对应供货商
     * @param sysSupplier
     * @return
     */
    public int updateSupplier(SysSupplier sysSupplier);

    public int deleteSupplier(SysSupplier supplier);

    /**
     * 根据供货商名称（模糊查询）供货商编码(模糊查询) 联系人（模糊查询） 创建时间 查询记录
     * @param sysSupplier
     * @return
     */
    public List<SysSupplier> findSupplierBySupName(SysSupplier sysSupplier);

    /**
     * 更新供货商的数据
     * @param sysSupplier
     */
    public void modifySupplier(SysSupplier sysSupplier);

    /**
     * 更新供货商的数据---使用trim
     * @param sysSupplier
     */
    public void modifySuppliertrim(SysSupplier sysSupplier);
}
