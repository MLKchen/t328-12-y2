package com.ccb.mapper;

import com.ccb.shili.entity.StorageRecord;
import com.ccb.shili.entity.SysSupplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @author CCB
 * date: 25/8/2022 下午4:14
 * Description: 入库表接口
 */
public interface StorageRecordMapper {


    public List<StorageRecord> getAllrecord(@Param("pageBegin") Integer pageBegin,@Param("pageSize") Integer pageSize);

    /**ch02上机1-2
     * 根据商品名称（模糊查询），供货商ID，支付状态查询入库记录
     * @return
     */
    public SysSupplier findStorageRecord(SysSupplier supplier);

    /**
     * 通过供货商，支付状态，商品名称（模糊查询）查询入库记录---ch03上机1
     * @param storageRecord
     * @return
     */
    public List<StorageRecord> findRecordById(StorageRecord storageRecord);

    /**
     * 根据供货商ID查询供货商关联的入库数据---array
     * @param supplierId
     * @return
     */
    public List<StorageRecord> findRecordByIDarray(Integer[] supplierId);

    /**
     * 根据供货商ID查询供货商关联的入库数据---list
     * @param supplierIdlist
     * @return
     */
    public List<StorageRecord> findRecordByIDlist(List<Integer> supplierIdlist);


    public List<StorageRecord> findRecordBysrCodemap(Map<String,Object> supplier);
}
