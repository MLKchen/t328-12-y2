package com.ccb.mapper;

import com.ccb.shili.entity.SysRole;
import com.ccb.shili.entity.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CCB
 * date: 29/8/2022 上午8:22
 * Description: 角色操作接口
 */
public interface SysRoleMapper {

    /**
     * 增加角色
     * @param sysRole
     */
    public void addRole(SysRole sysRole);

    /**
     * 根据角色id修改调用的角色信息
     * @param sysRole
     */
    public void updateRole(SysRole sysRole);

    /**
     * 通过角色ID查询用户信息
     * @param roleId
     * @return
     */
    public List<SysUser> findUserByRoleIDList(@Param("roleId") Integer roleId);

    /**
     * 根据角色ID删除对应角色信息
     * @param id
     */
    public void deleteRole(@Param("id") Integer id);

    /**
     * 根据角色ID删除对应用户信息
     * @param id
     */
    public void deleteUser(@Param("id") Integer id);

    /**
     * 根据角色名称模糊查询角色信息列表
     * @return
     */
    public List<SysRole> findRoleByroleNameList(SysRole sysRole);
}
