package com.ccb.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author CCB
 * date: 25/8/2022 下午3:55
 * Description: 日期格式工具类
 *              1.格式化日期为yyyy-MM-dd
 *              2.获取键如日期为星期几
 *              3.获取两个时间相差多少天
 *              4.距离多少天 多少小时 多少分 多少秒
 */
public class DateUtil {
        /**
         * 日期格式，年月日，用横杠分开，例如：2006-12-25，2008-08-08
         */
        public static final String DATE_FORMAT_YYYY_MM_DD = "yyyy-MM-dd";

        /**
         * 日期格式，年月日时分秒，年月日用横杠分开，时分秒用冒号分开
         * 例如：2005-05-10 23：20：00，2008-08-08 20:08:08
         */
        public static final String DATE_TIME_FORMAT_YYYY_MM_DD_HH_MI_SS = "yyyy-MM-dd HH:mm:ss";

        /* ************工具方法***************   */

        /**
         * 格式化Date时间
         * @param time Date类型时间
         * @return 格式化后的字符串 YYYY_MM_DD
         */
        public String parseDateToStr(Date time){
            DateFormat dateFormat=new SimpleDateFormat(DATE_FORMAT_YYYY_MM_DD);
            return dateFormat.format(time);
        }

        /**
         * 获取某个日期为星期几
         * @param date
         * @return String "星期*"
         */
        public String getDateOfWeek(Date date) {
            String[] weekDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
            if (w < 0)
                w = 0;
            return weekDays[w];
        }

        /**
         * 两个时间之间相差距离多少天
         * @param date1 时间参数 1：
         * @param date2 时间参数 2：
         * @return 相差天数
         */
        public Long getdateDifferDay(String date1, String date2) throws Exception{
            DateFormat df = new SimpleDateFormat(DATE_TIME_FORMAT_YYYY_MM_DD_HH_MI_SS);
            Date one;
            Date two;
            long days=0;
            try {
                one = df.parse(date1);
                two = df.parse(date2);
                long time1 = one.getTime();
                long time2 = two.getTime();
                long diff ;
                if(time1<time2) {
                    diff = time2 - time1;
                } else {
                    diff = time1 - time2;
                }
                days = diff / (1000 * 60 * 60 * 24);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return days;
        }
}
