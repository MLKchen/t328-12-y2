package com.ccb.mapper;

import com.ccb.shili.entity.StorageRecord;
import com.ccb.shili.entity.SysSupplier;
import com.ccb.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.*;

public class StorageRecordMapperTest extends MyBatisUtil {

    @Test
    public void getAllrecord() {
        SqlSession session = getSqlSession();
        Integer pageSize = 5;//显示页数
        Integer PageIndex = 3;//当前页数
        Integer pageBegin = (PageIndex - 1) * pageSize;//计算查询的起始位置
        List<StorageRecord> list = session.getMapper(StorageRecordMapper.class).getAllrecord(pageBegin,pageSize);
        for (StorageRecord storageRecord : list){
            System.out.println(storageRecord.getId()+"\t"+storageRecord.getGoodsName());
        }
        closeSqlSession(session);
    }

    @Test//ch02上机1-2
    public void findStorageRecord(){
        SysSupplier supplier = new SysSupplier();
        supplier.setId(2);
        SqlSession sqlSession = getSqlSession();
        SysSupplier sysSupplier =  sqlSession.getMapper(StorageRecordMapper.class).findStorageRecord(supplier);
        System.out.println("供货商家==============\n供货ID:"+sysSupplier.getId()
                        +"\t供货商编号:"+sysSupplier.getSupCode()
                        +"\t供货商名称:"+sysSupplier.getSupName()
                        +"\t联系人:"+sysSupplier.getSupContact()
                        +"\t联系电话:"+sysSupplier.getSupPhone()
                        +"\n入库信息================"
        );
       for (StorageRecord storageRecord1 : sysSupplier.getStorageRecordList()){
            System.out.println("入库编码："+storageRecord1.getSrCode()
                    +"\t商品名称："+storageRecord1.getGoodsName()
                    +"\t商品总金额："+storageRecord1.getTotalAmount()
                    +"\t支付状态："+storageRecord1.getPayStatus()
            );
        }
    }

    @Test
    public void findRecordById(){
        StorageRecord storageRecord = new StorageRecord();
        storageRecord.setSupplierId(2);
        storageRecord.setGoodsName("乳");
        storageRecord.setPayStatus(2);
        SqlSession sqlSession = getSqlSession();
        List<StorageRecord> list = sqlSession.getMapper(StorageRecordMapper.class).findRecordById(storageRecord);
        for (StorageRecord storageRecord1 : list){
            System.out.println(storageRecord1.getGoodsName());
        }
        closeSqlSession(sqlSession);
    }

    @Test
    public void  findRecordByIDarray(){
        SqlSession sqlSession = getSqlSession();
        List<StorageRecord> list = new ArrayList<>();
        Integer[] supplierId = {1,2};

        list = sqlSession.getMapper(StorageRecordMapper.class).findRecordByIDarray(supplierId);
        for (StorageRecord storageRecord : list){
            System.out.println(storageRecord.getGoodsName());
        }
    }

    @Test
    public void findRecordByIDlist(){
        SqlSession sqlSession = getSqlSession();
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);

        List<StorageRecord> StorageRecordlist = sqlSession.getMapper(StorageRecordMapper.class).findRecordByIDlist(list);
        for (StorageRecord storageRecord : StorageRecordlist){
            System.out.println(storageRecord.getGoodsName());
        }
    }

    @Test
    public void findRecordBysrCodemap(){
        SqlSession sqlSession = getSqlSession();
        List<Integer> listsupplierId = new ArrayList<>();//供货商id集合
        listsupplierId.add(1);
        listsupplierId.add(2);
        String srCode = "sr_2019_0";//模糊查询
        Map<String,Object> map = new HashMap<>();
        map.put("listsupplierId",listsupplierId);
        map.put("srCode",srCode);
        SqlSession sqlSession1 = getSqlSession();
        List<StorageRecord> list = sqlSession1.getMapper(StorageRecordMapper.class).findRecordBysrCodemap(map);
        for (StorageRecord storageRecord : list){
            System.out.println(storageRecord.getGoodsName());
        }
    }
}