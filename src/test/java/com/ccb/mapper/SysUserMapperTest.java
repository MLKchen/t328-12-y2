package com.ccb.mapper;

import com.ccb.shili.entity.Address;
import com.ccb.shili.entity.SysUser;
import com.ccb.util.DateUtil;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SysUserMapperTest {
    SqlSession session = null;

    @Before //之前
    public void before(){
        //1、配置mybatis文件
        String resource ="mybatis-config.xml";
        InputStream is = null;
        try {
            //2、引用
            is = Resources.getResourceAsStream(resource);
            //3、创建SqlSessionFactory
            SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
            //4、打开SqlSession
            session = factory.openSession();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @After //之后
    public void after(){
        //7、关闭
        session.close();
    }

    @Test
    public void count() {
        int count = session.getMapper(SysUserMapper.class).count();
        System.out.println(count);
    }

    @Test
    public void getUserList() {
        List<SysUser> userList = session.getMapper(SysUserMapper.class).getUserList();
        for (SysUser user : userList){
            System.out.println(
                    user.getRealName()
            );
        }
    }

    @Test
    public void getUserByRealName() {
        List<SysUser> userList = session.getMapper(SysUserMapper.class).getUserByRealName("李");
        for (SysUser user : userList){
            System.out.println(
                    user.getRealName()
            );
        }
    }

    @Test
    public void getUserListByPojo(){
        SysUser sysUser = new SysUser();
        sysUser.setRealName("李");
        sysUser.setRoleId(3);
        List<SysUser> userList = session.getMapper(SysUserMapper.class).getUserListByPojo(sysUser);
        System.out.println(userList.size());

    }

    @Test
    public void getUserListByMap(){
        Map<String,Object> userMap = new HashMap<>();
        userMap.put("rName","赵");
        userMap.put("rId",2);
        List<SysUser> userList = session.getMapper(SysUserMapper.class).getUserListByMap(userMap);
        System.out.println(userList.size());
    }

    @Test
    public void getUSerListByParms(){
        List<SysUser> userList = session.getMapper(SysUserMapper.class).getUSerListByParms("赵",2);
        System.out.println(userList.size());
    }

    @Test
    public void getUserListWithRoleName(){
        SysUser sysUser = new SysUser();
        sysUser.setRoleId(3);
        sysUser.setRealName("李");
        List<SysUser> userList = session.getMapper(SysUserMapper.class).getUserListWithRoleName(sysUser);
        for (SysUser User : userList){
            System.out.println(User.getRealName());
            System.out.println(User.getUserRoleName());
        }
    }

    @Test
    public void getUserListByRoleId(){
        List<SysUser> userList = session.getMapper(SysUserMapper.class).getUserListByRoleId(2);
        System.out.println(userList.size());
        for (SysUser user : userList){
            System.out.println(
                    "testGetUSerList userId"+user.getId()+
                    "and realName:" + user.getRealName()+
                    "and roleId:" + user.getSysRole().getId()+
                    "and code:" + user.getSysRole().getCode()+
                    "and roleName:"+ user.getSysRole().getRoleName()
            );
        }
    }

    @Test
    public void getUserAddressesByUserId(){
        List<SysUser> userList = session.getMapper(SysUserMapper.class).getUserAddressesByUserId(1);
        for (SysUser sysUser : userList){
            System.out.println("userList(include:addresslist) =====> account:"+
                    sysUser.getAccount()+",realName: "+sysUser.getRealName());
            for (Address address : sysUser.getAddressesList()){
                System.out.println(
                        "address----> id: "+address.getId()+
                        ", contact: "+address.getContact()+
                        ",addressDesc :"+address.getAddressDesc()+
                        ",tel: "+address.getTel()+
                        ",postCode: "+address.getPostCode()
                );
            }
        }
    }
    DateUtil dateUtil = new DateUtil();
    @Test
    public void add(){
        System.out.println("testAdd !===============");
        int count = 0;
        try {
            SysUser user = new SysUser();
            user.setAccount("test001");
            user.setRealName("测试用户001");
            user.setPassword("1234567");
            Date birthday = new SimpleDateFormat("yyyy-MM-dd").parse("2003-10-22");
            user.setBirthday(birthday);
            user.setAddress("测试地址abc");
            user.setSex(1);
            user.setPhone("13411110000");
            user.setRoleId(1);
            user.setCreatedUserId(1);
            user.setCreatedTime(new Date());
            count = session.getMapper(SysUserMapper.class).add(user);
            //模拟发生异常
            //int i = 2/0;
            session.commit();
        }catch (Exception e){
            e.printStackTrace();
            session.rollback();
            count = 0;
        }finally {
            System.out.println("testAdd count: " + count);
        }
    }

    @Test
    public void modify(){
        System.out.println("testmodify !===============");
        int count = 0;
        try {
            SysUser user = new SysUser();
            user.setAccount("test001");
            user.setRealName("测试用户9999");
            user.setPassword("1234567");
            Date birthday = new SimpleDateFormat("yyyy-MM-dd").parse("2003-10-22");
            user.setBirthday(birthday);
            user.setAddress("测试地址abc");
            user.setSex(1);
            user.setPhone("13411110000");
            user.setRoleId(1);
            user.setUpdatedUserId(1);
            user.setUpdatedTime(new Date());
            user.setId(29);
            count = session.getMapper(SysUserMapper.class).modify(user);
            session.commit();
        } catch (Exception e) {
            e.printStackTrace();
            session.rollback();
            count = 0;
        } finally {
            System.out.println("testmodify count: " + count);
        }
    }

    @Test
    public void updatePwd(){
        System.out.println("testupdatePwd !===============");
        String pwd = "666666";
        Integer id = 29;
        int count = 0;
        try{
            count = session.getMapper(SysUserMapper.class).updatePwd(id,pwd);
            session.commit();
        }catch (Exception e){
            e.printStackTrace();
            session.rollback();
            count = 0;
        }finally {
            System.out.println("testupdatePwd count: " + count);
        }
    }

    @Test
    public void deleteUserById(){
        int count = 0;
        try{
            count = session.getMapper(SysUserMapper.class).deleteUserById(16);
            session.commit();
        }catch (Exception e){
            e.printStackTrace();
            session.rollback();
            count = 0;
        }finally {
            System.out.println("testdeleteUserById count: " + count);
        }
    }

}