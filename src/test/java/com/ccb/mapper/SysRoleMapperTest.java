package com.ccb.mapper;

import com.ccb.shili.entity.SysRole;
import com.ccb.shili.entity.SysUser;
import com.ccb.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SysRoleMapperTest extends MyBatisUtil {

    @Test
    public void addRole() {
        SqlSession sqlSession = getSqlSession();
        int result = 0;
        try {
            SysRole sysRole = new SysRole();
            sysRole.setCode("SMBMS_PROMOTERS");
            sysRole.setRoleName("促销员");
            sysRole.setCreatedUserId(1);
            Date CreatedTime = new SimpleDateFormat("yyyy-MM-dd").parse("2022-08-29");
            sysRole.setCreatedTime(CreatedTime);
            sqlSession.getMapper(SysRoleMapper.class).addRole(sysRole);
            sqlSession.commit();
            ++result;
        }catch (Exception e){
            e.printStackTrace();
            sqlSession.rollback();
            result = 0;
        }finally {
            System.out.println("角色增加结果："+result);
        }
    }

    @Test
    public void updateRole(){
        SqlSession sqlSession = getSqlSession();
        int result = 0;
        try {
            SysRole sysRole = new SysRole();
            sysRole.setRoleName("促销员01");
            sysRole.setId(6);
            sqlSession.getMapper(SysRoleMapper.class).updateRole(sysRole);
            sqlSession.commit();
            result=0;
        }catch (Exception e){
            e.printStackTrace();
            result = 0;
            sqlSession.rollback();
        }finally {
            System.out.println("角色修改结果："+result);
        }
    }

    @Test
    public void deleteRole(){
        int roleId = 5;//定义删除的角色ID
        int result = 0;
        SqlSession sqlSession1 = getSqlSession();
        SqlSession sqlSession2 = getSqlSession();
        SqlSession sqlSession3 = getSqlSession();
        try {
            List<SysUser> sysUserList = sqlSession1.getMapper(SysRoleMapper.class).findUserByRoleIDList(roleId);
            if (sysUserList.size()==0){
               sqlSession2.getMapper(SysRoleMapper.class).deleteRole(roleId);
               sqlSession2.commit();
            }else {
               sqlSession3.getMapper(SysRoleMapper.class).deleteUser(roleId);
               sqlSession3.commit();
               sqlSession2.getMapper(SysRoleMapper.class).deleteRole(roleId);
               sqlSession2.commit();
            }
            result = 1;
        }catch (Exception e){
            e.printStackTrace();
            sqlSession2.rollback();
            sqlSession3.rollback();
            result = 0;
        }finally {
            System.out.println("角色删除结果："+result);
        }
    }

    @Test
    public void findRoleByroleNameList(){
        SqlSession sqlSession = getSqlSession();
        Integer pageSize = 5;//显示页数
        Integer PageIndex = 1;//当前页数
        Integer pageBegin = (PageIndex - 1) * pageSize;//计算查询的起始位置
        SysRole sysRole = new SysRole();
        sysRole.setRoleName("");
        sysRole.setPageBegin(pageBegin);
        sysRole.setPageSize(pageSize);
        List<SysRole> sysRoleList = sqlSession.getMapper(SysRoleMapper.class).findRoleByroleNameList(sysRole);
        for(SysRole sysRoles : sysRoleList){
            System.out.println("Code:"+sysRoles.getCode()+
                    "\tRoleName:"+sysRoles.getRoleName());
        }
    }
}