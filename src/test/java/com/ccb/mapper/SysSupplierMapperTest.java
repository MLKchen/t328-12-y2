package com.ccb.mapper;

import com.ccb.shili.entity.SysSupplier;
import com.ccb.util.MyBatisUtil;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

public class SysSupplierMapperTest extends MyBatisUtil {

    @Test
    public void getAllSupplier() {
        SqlSession session = getSqlSession();
        SysSupplier supplier = new SysSupplier();
        Integer pageSize = 5;//显示页数
        Integer PageIndex = 2;//当前页数
        Integer pageBegin = (PageIndex - 1) * pageSize;//计算查询的起始位置
        supplier.setPageBegin(pageBegin);
        supplier.setPageSize(pageSize);
        List<SysSupplier> list = session.getMapper(SysSupplierMapper.class).getAllSupplier(supplier);
        for (SysSupplier suppliers : list){
            System.out.println(suppliers.getSupName());
        }
        System.out.println(list.size());
        closeSqlSession(session);
    }

    @Test//ch02上机5-1
    public void addSupplier(){
        SysSupplier supplier = new SysSupplier();
        supplier.setSupCode("测试");
        supplier.setSupName("测试");
        supplier.setSupDesc("测试");
        supplier.setSupContact("测试");
        supplier.setSupPhone("测试");
        supplier.setSupAddress("测试");
        supplier.setSupFax("测试");
        supplier.setCreatedUserId(111);
        supplier.setCreatedTime("2022-08-27");
        SqlSession sqlSession = getSqlSession();
        int num = 0;
        try {
            num = sqlSession.getMapper(SysSupplierMapper.class).addSupplier(supplier);
            sqlSession.commit();
        }catch (Exception e){
            e.printStackTrace();
            sqlSession.rollback();
            num = 0;
        }finally {
            System.out.println("商家增加执行结果:num="+num );
        }
    }

    @Test//ch02上机5-2
    public void updateSupplier(){
        SysSupplier supplier = new SysSupplier();
        supplier.setSupCode("测试111");
        supplier.setSupName("测试222");
        supplier.setSupDesc("测试333");
        supplier.setSupContact("测试444");
        supplier.setSupPhone("测试");
        supplier.setSupAddress("测试");
        supplier.setSupFax("测试");
        supplier.setCreatedUserId(111);
        supplier.setCreatedTime("2022-08-27");
        supplier.setId(18);
        SqlSession sqlSession = getSqlSession();
        int num = 0;
        try {
            num = sqlSession.getMapper(SysSupplierMapper.class).updateSupplier(supplier);
            sqlSession.commit();
        }catch (Exception e){
            e.printStackTrace();
            sqlSession.rollback();
            num = 0;
        }finally {
            System.out.println("商家修改执行结果:num="+num );
        }
    }

    @Test
    public void deleteSupplier(){
        SysSupplier supplier = new SysSupplier();
        supplier.setId(18);
        SqlSession sqlSession = getSqlSession();
        int num = 0;
        try {
            num = sqlSession.getMapper(SysSupplierMapper.class).deleteSupplier(supplier);
            sqlSession.commit();
        }catch (Exception e){
            e.printStackTrace();
            sqlSession.rollback();
            num = 0;
        }finally {
            System.out.println("商家删除执行结果:num="+num );
        }
    }

    @Test
    public void findSupplierBySupName(){
        SysSupplier supplier = new SysSupplier();
        //supplier.setSupName("北京");
        //supplier.setSupCode("BJ");
        //supplier.setSupContact("明");
        supplier.setCreatedTime("2014-11-21 12:51:11");
        SqlSession sqlSession = getSqlSession();
        List<SysSupplier> list = sqlSession.getMapper(SysSupplierMapper.class).findSupplierBySupName(supplier);
        for (SysSupplier supplier1 : list){
            System.out.println(supplier1.getSupName());
        }
    }

    @Test
    public void modifySupplier(){
        int result = 0;
        SqlSession sqlSession = getSqlSession();
        try {
            SysSupplier supplier = new SysSupplier();
            supplier.setSupCode("4444");
            supplier.setId(19);
            sqlSession.getMapper(SysSupplierMapper.class).modifySupplier(supplier);
            sqlSession.commit();
            result=1;
        }catch (Exception e){
            e.printStackTrace();
            sqlSession.rollback();
            result=0;
        }finally {
            System.out.println("修改结果："+result);
            closeSqlSession(sqlSession);
        }
    }

    @Test
    public void modifySuppliertrim(){
        int result = 0;
        SqlSession sqlSession = getSqlSession();
        try {
            SysSupplier supplier = new SysSupplier();
            supplier.setSupCode("dddd");
            supplier.setId(19);
            sqlSession.getMapper(SysSupplierMapper.class).modifySuppliertrim(supplier);
            sqlSession.commit();
            result=1;
        }catch (Exception e){
            e.printStackTrace();
            sqlSession.rollback();
            result=0;
        }finally {
            System.out.println("修改结果："+result);
            closeSqlSession(sqlSession);
        }
    }
}