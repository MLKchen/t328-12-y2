package com.ccb.dao;

import com.ccb.controller.SysUSerController;
import com.ccb.pojo.SysUser;
import com.ccb.service.SysUserService;
import org.apache.log4j.Logger;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.*;

public class SysUserMapperTest {
    private Logger logger = Logger.getLogger(SysUSerController.class);
    @Test
    public void login() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SysUserService sysUserService = (SysUserService) ctx.getBean("sysUserService");
        SysUser sysUser = sysUserService.login("admin","1111111");
        System.out.println(sysUser.getPhone());
    }

    @Test
    public void getUserList(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SysUserService sysUserService = (SysUserService) ctx.getBean("sysUserService");
//        List<SysUser> sysUser = sysUserService.getUserList("李",null,1,5);
//        for (SysUser sysUser1 : sysUser){
//            System.out.println(sysUser1.getRealName());
//        }
        List<SysUser> sysUsersList = sysUserService.getUserList("李",null,0,1,null);
        SysUser sysUser = new SysUser();
        for (SysUser sysUsers : sysUsersList){
            sysUser.setId(sysUsers.getId());
            sysUser.setAccount(sysUsers.getAccount());
            sysUser.setRealName(sysUsers.getRealName());
            sysUser.setSex(sysUsers.getSex());
            sysUser.setRoleId(sysUsers.getRoleId());
            sysUser.setRoleName(sysUsers.getRoleName());
            System.out.println(sysUsers.getRealName());
        }
        System.out.println(sysUsersList.size());
    }

    @Test
    public void Tt(){
        SysUser sysUser = new SysUser();
        sysUser.setBirthday(new Date());
        System.out.println(sysUser.getAge());
    }

    @Test
    public void update(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SysUserService sysUserService = (SysUserService) ctx.getBean("sysUserService");
        SysUser sysUser = new SysUser();
        sysUser.setAddress("9999");
        sysUser.setId(60);
        sysUserService.modifyUser(sysUser);
    }

    @Test
    public void add(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SysUserService sysUserService = (SysUserService) ctx.getBean("sysUserService");
        SysUser sysUser = new SysUser();
        sysUser.setAccount("1111");
        sysUser.setPassword("1111");
        sysUser.setSex(1);
        sysUser.setBirthday(new Date());
        sysUser.setPhone("13762543844");
        sysUser.setAddress("1111");
        sysUser.setRoleId(1);
        sysUser.setIdPicPath("111");
        sysUser.setWorkPicPath("9999");
        sysUserService.addUser(sysUser);
    }

    @Test
    public void del(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SysUserService sysUserService = (SysUserService) ctx.getBean("sysUserService");
//        HashMap<String,Object> resultMap = new HashMap<String, Object>();
//        int delResult = sysUserService.delUser(72);
//        System.out.println(delResult);
        System.out.println(sysUserService.getUserCount(null,2));
    }

    @Test
    public void upwd(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SysUserService sysUserService = (SysUserService) ctx.getBean("sysUserService");
        sysUserService.modipwd(1,"2222222");
    }

    @Test
    public void DateTest(){
        Long timeStamp = System.currentTimeMillis();//获取当前时间戳

        SimpleDateFormat dateFormat = new SimpleDateFormat("现在是 yyyy 年 MM 月 dd 日");

        String nowtime = dateFormat.format(new Date(timeStamp));

        System.out.println(nowtime);
    }
}