package com.ccb.dao;

import com.ccb.pojo.SysRole;
import com.ccb.service.SysRoleService;
import com.ccb.service.SysUserService;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

import static org.junit.Assert.*;

public class SysRoleMapperTest {

    ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
    SysUserService sysUserService = (SysUserService) ctx.getBean("sysUserService");
    SysRoleService sysRoleService = (SysRoleService) ctx.getBean("SysRoleService");

    @Test
    public void getRoleList() {
    }

    @Test
    public void selectSysRoleList() {
        List<SysRole> roleList = sysRoleService.getList();
        System.out.println(roleList.size());
    }
}