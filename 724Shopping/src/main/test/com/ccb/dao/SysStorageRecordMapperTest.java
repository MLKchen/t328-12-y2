package com.ccb.dao;

import com.ccb.pojo.StorageRecord;
import com.ccb.pojo.SysSupplier;
import com.ccb.service.SysStorageRecordService;
import com.ccb.service.SysSupplierService;
import com.ccb.utils.Constants;
import com.ccb.utils.PageUtil;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

import static org.junit.Assert.*;

public class SysStorageRecordMapperTest {

    @Test
    public void getRecordList() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SysStorageRecordService sysStorageRecordService = (SysStorageRecordService) ctx.getBean("SysStorageRecordService");
        int count = sysStorageRecordService.getRecordCount("国",null,null);
        System.out.println("count"+count);
        //设置页面容量
        int pageSizes = Constants.pageSize;
        PageUtil page = new PageUtil();
        page.setNowPage(1);
        page.setShowSize(pageSizes);
        page.setCountPage(count);
        List<StorageRecord> storageRecordList = sysStorageRecordService.getRecordList("国",null,null,page.getOffset(),page.getShowSize());;
        System.out.println(storageRecordList.size());
        for (StorageRecord storageRecord : storageRecordList){
            System.out.println(storageRecord.getGoodsCount());
        }
    }

    @Test
    public void sup(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SysSupplierService supplierService = (SysSupplierService) ctx.getBean("SysSupplierService");
        List<SysSupplier> sysSupplierList = supplierService.getSupplierList("5");
        System.out.println(sysSupplierList.size());
    }

    @Test
    public void sto(){
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        SysStorageRecordService sysStorageRecordService = (SysStorageRecordService) ctx.getBean("SysStorageRecordService");
        StorageRecord storageRecord = sysStorageRecordService.getRecord(1);
        System.out.println(storageRecord.getGoodsName());
    }
}