package com.ccb.service;

import com.ccb.pojo.SysSupplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CCB
 * date: 14/10/2022 下午5:27
 * Description: 描述
 */
public interface SysSupplierService {

    /**
     * 获取商家集合
     * @return
     */
    List<SysSupplier> getSupplierList(@Param("id") String id);

    /**
     * 查询供应商数量
     *
     * @param supCode
     * @param supName
     * @return
     */
    int getSupplierCount(@Param("supCode") String supCode,
                         @Param("supName") String supName);

    /**
     * 查询供应商
     *
     * @return
     */
    List<SysSupplier> getList(@Param("supCode") String supCode,
                           @Param("supName") String supName,
                           @Param("pageIndex") Integer pageIndex,
                           @Param("pageSize") Integer pageSize);


    /**
     * 新增供应商
     *
     * @param supplier
     * @return
     */
    boolean add(SysSupplier supplier);

    /**
     * 根据编号查询供应商
     * @param id
     * @return
     */
    SysSupplier getSupplierById(@Param("id")Integer id);

    /**
     * 修改供应商信息
     *
     * @param supplier
     * @return
     */
    boolean updateSupplier(SysSupplier supplier);

    /**
     * 删除供应商
     *
     * @param id
     * @return
     */
    boolean delSupplier(@Param("id") Integer id);
}
