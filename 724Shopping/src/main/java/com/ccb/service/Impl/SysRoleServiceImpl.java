package com.ccb.service.Impl;

import com.ccb.dao.SysRoleMapper;
import com.ccb.pojo.SysRole;
import com.ccb.service.SysRoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author CCB
 * date: 12/10/2022 上午10:24
 * Description: 描述
 */
@Service("SysRoleService")
public class SysRoleServiceImpl implements SysRoleService {

    @Resource
    private SysRoleMapper sysRoleMapper;

    @Override
    public List<SysRole> getRoleList(Integer roleId) {
        return sysRoleMapper.getRoleList(roleId);
    }

    @Override
    public List<SysRole> getList() {
        try {
            return sysRoleMapper.selectSysRoleList();
        }catch (RuntimeException e){
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public SysRole getSysRoleCode(String code) {
        try {
            return sysRoleMapper.getSysRoleCode(code);
        }catch (RuntimeException e){
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public boolean add(SysRole sysRole) {
        boolean result = false;
        try{
            if (sysRoleMapper.add(sysRole)==1){
                result = true;
            }
        }catch (RuntimeException e){
            e.printStackTrace();
            throw e;
        }
        return result;
    }

    @Override
    public SysRole getSysRoleId(Integer id) {
        try {
            return sysRoleMapper.getSysRoleId(id);
        }catch (RuntimeException e){
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public boolean update(SysRole sysRole) {
        boolean result = false;
        try{
            if (sysRoleMapper.update(sysRole)==1){
                result = true;
            }
        }catch (RuntimeException e){
            e.printStackTrace();
            throw e;
        }
        return result;
    }

    @Override
    public boolean delete(Integer id) {
        boolean result = false;
        try{
            if (sysRoleMapper.delete(id)==1){
                result = true;
            }
        }catch (RuntimeException e){
            e.printStackTrace();
            throw e;
        }
        return result;
    }
}
