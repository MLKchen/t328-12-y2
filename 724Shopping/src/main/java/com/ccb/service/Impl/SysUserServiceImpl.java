package com.ccb.service.Impl;

import com.ccb.dao.SysUserMapper;
import com.ccb.pojo.SysUser;
import com.ccb.service.SysUserService;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author CCB
 * date: 11/10/2022 下午7:08
 * Description: 描述
 */
@Service("sysUserService")
public class SysUserServiceImpl implements SysUserService {

    @Resource
    private SysUserMapper sysUserMapper;

    @Override
    public SysUser login(String account, String password) {
        return sysUserMapper.login(account,password);
    }

    @Override
    public Integer getUserCount(String realName, Integer roleId) {
        return sysUserMapper.getUserCount(realName,roleId);
    }

    @Override
    public List<SysUser> getUserList(String queryRealName, Integer queryRoleId, Integer pageIndex, Integer pageSize,Integer userid) {
        return sysUserMapper.getUserList(queryRealName,queryRoleId,pageIndex,pageSize,userid);
    }

    @Override
    public int addUser(SysUser sysUser) {
        return sysUserMapper.addUser(sysUser);
    }

    @Override
    public int modifyUser(SysUser sysUser) {
        return sysUserMapper.modifyUser(sysUser);
    }

    @Override
    public SysUser getUser(String account) {
        return sysUserMapper.getUser(account);
    }

    @Override
    public int delUser(Integer id) {
        return sysUserMapper.delUser(id);
    }

    @Override
    public int modipwd(Integer id, String password) {
        return sysUserMapper.modipwd(id,password);
    }

    @Override
    public List<SysUser> getUserByRoleId(Integer id) {
        return sysUserMapper.getUserByRoleId(id);
    }
}
