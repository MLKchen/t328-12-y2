package com.ccb.service.Impl;

import com.ccb.dao.SysSupplierMapper;
import com.ccb.pojo.SysSupplier;
import com.ccb.service.SysSupplierService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author CCB
 * date: 14/10/2022 下午5:27
 * Description: 描述
 */
@Service("SysSupplierService")
public class SysSupplierServiceImpl implements SysSupplierService {

    @Resource
    private SysSupplierMapper SysSupplierMapper;

    @Override
    public List<SysSupplier> getSupplierList(String id) {
        return SysSupplierMapper.getSupplierList(id);
    }

    @Override
    public int getSupplierCount(String supCode, String supName) {
        try {
            return SysSupplierMapper.getSupplierCount(supCode, supName);
        } catch (RuntimeException e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public List<SysSupplier> getList(String supCode, String supName, Integer pageIndex, Integer pageSize) {
        try {
            return SysSupplierMapper.selectSupplierList(supCode, supName, pageIndex, pageSize);
        } catch (RuntimeException e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public boolean add(SysSupplier supplier) {
        boolean result = false;
        try {
            if (SysSupplierMapper.add(supplier) == 1) {
                result = true;
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
            throw e;
        }
        return result;
    }

    @Override
    public SysSupplier getSupplierById(Integer id) {
        try {
            return SysSupplierMapper.getSupplierById(id);
        } catch (RuntimeException e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    public boolean updateSupplier(SysSupplier supplier) {
        boolean result = false;
        try {
            if (SysSupplierMapper.updateSupplier(supplier) == 1) {
                result = true;
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
            throw e;
        }
        return result;
    }

    @Override
    public boolean delSupplier(Integer id) {
        boolean result = false;
        try {
            if (SysSupplierMapper.delSupplier(id) == 1) {
                result = true;
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
            throw e;
        }
        return result;
    }

}
