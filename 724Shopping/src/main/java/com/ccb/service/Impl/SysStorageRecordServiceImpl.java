package com.ccb.service.Impl;

import com.ccb.dao.SysStorageRecordMapper;
import com.ccb.pojo.StorageRecord;
import com.ccb.service.SysStorageRecordService;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author CCB
 * date: 14/10/2022 下午3:13
 * Description: 描述
 */
@Service("SysStorageRecordService")
public class SysStorageRecordServiceImpl implements SysStorageRecordService {

    @Resource
    private SysStorageRecordMapper SysStorageRecordMapper;

    @Override
    public List<StorageRecord> getRecordList(String goodsName, String supplierId,
                                             String payStatus,Integer pageIndex,
                                             @Param("pageSize") Integer pageSize) {
        return SysStorageRecordMapper.getRecordList(goodsName,supplierId,payStatus,pageIndex,pageSize);
    }

    @Override
    public Integer getRecordCount(String goodsName, String supplierId, String payStatus) {
        return SysStorageRecordMapper.getRecordCount(goodsName,supplierId,payStatus);
    }

    @Override
    public int addRecordCount(StorageRecord storageRecord) {
        return SysStorageRecordMapper.addRecordCount(storageRecord);
    }

    @Override
    public StorageRecord getRecord(Integer id) {
        return SysStorageRecordMapper.getRecord(id);
    }

    @Override
    public int modifyRecord(StorageRecord storageRecord) {
        return SysStorageRecordMapper.modifyRecord(storageRecord);
    }

    @Override
    public int del(Integer id) {
        return SysStorageRecordMapper.del(id);
    }
}
