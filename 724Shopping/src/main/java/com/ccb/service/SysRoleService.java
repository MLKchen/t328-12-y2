package com.ccb.service;

import com.ccb.pojo.SysRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CCB
 * date: 12/10/2022 上午10:24
 * Description: 描述
 */
public interface SysRoleService {
    /**
     * 获取角色ID
     * @param roleId
     * @return
     */
    List<SysRole> getRoleList(@Param("roleId")Integer roleId);

    /**
     * 查询角色表信息
     * @return
     */
    List<SysRole> getList();

    /**
     * 根据角色编码查询角色
     * @param code
     * @return
     */
    SysRole getSysRoleCode(@Param("code")String code);

    /**
     * 新增角色表信息
     * @param sysRole
     * @return
     */
    boolean add(SysRole sysRole);

    /**
     * 根据角色编号查询角色
     * @param id
     * @return
     */
    SysRole getSysRoleId(@Param("id")Integer id);

    /**
     * 修改角色信息
     * @param sysRole
     * @return
     */
    boolean update(SysRole sysRole);

    /**
     * 删除角色信息
     * @param id
     * @return
     */
    boolean delete(@Param("id")Integer id);
}
