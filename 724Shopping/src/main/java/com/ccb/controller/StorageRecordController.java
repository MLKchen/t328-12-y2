package com.ccb.controller;

import com.alibaba.fastjson.JSON;
import com.ccb.pojo.StorageRecord;

import com.ccb.pojo.SysSupplier;
import com.ccb.pojo.SysUser;
import com.ccb.service.SysStorageRecordService;
import com.ccb.service.SysSupplierService;

import com.ccb.utils.Constants;
import com.ccb.utils.PageUtil;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;

/**
 * @author CCB
 * date: 14/10/2022 下午2:42
 * Description: 描述
 */
@Controller
@RequestMapping("/storageRecord")
public class StorageRecordController {
    private Logger logger = Logger.getLogger(StorageRecordController.class);
    ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
    SysStorageRecordService sysStorageRecordService = (SysStorageRecordService) ctx.getBean("SysStorageRecordService");

    /**
     * 获取入库信息的集合
     * @param session
     * @param model
     * @param queryGoodsName
     * @param querySupplierId
     * @param queryPayStatus
     * @param pageIndex
     * @return
     */
    @GetMapping("/list")
    public String torecord(HttpSession session,Model model, String queryGoodsName,
                           String querySupplierId, String queryPayStatus,
                           @RequestParam(defaultValue = "1") Integer pageIndex) {
        //用于异常空指针
        if (queryGoodsName == "") {
            queryGoodsName = null;
        }
        if (queryPayStatus == "") {
            queryPayStatus = null;
        }
        if (querySupplierId == "") {
            querySupplierId = null;
        }
        SysSupplierService supplierService = (SysSupplierService) ctx.getBean("SysSupplierService");

        //获取总数量/根据条件获取
        int count = sysStorageRecordService.getRecordCount(queryGoodsName, querySupplierId, queryPayStatus);

        //设置页面容量
        int pageSizes = Constants.pageSize;
        PageUtil page = new PageUtil();
        page.setNowPage(pageIndex);
        page.setShowSize(pageSizes);
        page.setCountPage(count);

        //获取记录集合
        List<StorageRecord> storageRecordList = sysStorageRecordService.getRecordList(queryGoodsName, querySupplierId, queryPayStatus, page.getOffset(), page.getShowSize());

        //获取对应条件的记录数
        int totalPageCount = page.getTotalPageCount();

        if (pageIndex > totalPageCount  && totalPageCount!=0) {
//            storageRecordList = null;//处理因没有数据满足提交出现空集合而导致页面报错的代码
            pageIndex = totalPageCount;
            throw new RuntimeException("页码不正确");
        }

        //获取全部供货商集合
        List<SysSupplier> sysSupplierList = supplierService.getSupplierList(null);

        //将页面展示所需数据存放于model中
        model.addAttribute("queryGoodsName", queryGoodsName);
        model.addAttribute("querypayStatus", queryPayStatus);
        model.addAttribute("querySupplierId", querySupplierId);
        session.setAttribute("querySupplierId",querySupplierId);
        model.addAttribute("storageRecordList", storageRecordList);
        model.addAttribute("totalPageCount", totalPageCount);
        model.addAttribute("totalCount", count);
        model.addAttribute("currentPageNo", pageIndex);
        model.addAttribute("supplierList", sysSupplierList);

        //页面跳转
        return "storageRecord/list";
    }

    /**
     * 跳转到添加页面
     * @return
     */
    @RequestMapping("/toAdd")
    public String toAdd() {
        return "storageRecord/add";
    }

    /**
     * 添加入库信息
     * @param session
     * @param storageRecord
     * @return
     */
    @PostMapping("/add")
    public String add(HttpSession session,StorageRecord storageRecord){
        //获取登录用户的信息
        SysUser sysUser = (SysUser) session.getAttribute(Constants.USER_SESSION);
        storageRecord.setCreatedUserId(sysUser.getId());
        //添加入库信息
        sysStorageRecordService.addRecordCount(storageRecord);
        return "redirect:/storageRecord/list";
    }

    /**
     * 入库信息展示
     * @param model
     * @param storageRecordId
     * @return
     */
    @RequestMapping("/view/{storageRecordId}")
    public String view(Model model, @PathVariable Integer storageRecordId){
        StorageRecord storageRecord = sysStorageRecordService.getRecord(storageRecordId);
        model.addAttribute("storageRecord",storageRecord);
        return "/storageRecord/view";
    }

    /**
     * 跳转至修改页面/获取相关数据
     * @param model
     * @param storageRecordId
     * @return
     */
    @RequestMapping("/toUpdate/{storageRecordId}")
    public String toUpdate(Model model, @PathVariable Integer storageRecordId){
        StorageRecord storageRecord = sysStorageRecordService.getRecord(storageRecordId);
        model.addAttribute("storageRecord",storageRecord);
        return "/storageRecord/update";
    }

    /**
     * 修改入库信息
     * @param session
     * @param storageRecord
     * @return
     */
    @RequestMapping("/update")
    public String update(HttpSession session,StorageRecord storageRecord){
        sysStorageRecordService.modifyRecord(storageRecord);
        return "redirect:/storageRecord/list";
    }

    /**
     * Ajax删除入库信息-根据id删除
     * @param storageRecordId
     * @return
     */
    @ResponseBody
    @DeleteMapping("/del/{storageRecordId}")
    public String del(@PathVariable Integer storageRecordId){
        HashMap<String,Object> resultMap = new HashMap<String, Object>();
        int delResult = sysStorageRecordService.del(storageRecordId);
        if(delResult==1){
            resultMap.put("delResult","true");
        }else if(delResult==0){
            resultMap.put("delResult","notexist");
        }else {
            resultMap.put("delResult","false");
        }
        return JSON.toJSONString(resultMap);
    }

}
