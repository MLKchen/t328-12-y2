package com.ccb.controller;

import com.alibaba.fastjson.JSON;
import com.ccb.pojo.SysRole;
import com.ccb.pojo.SysUser;
import com.ccb.service.SysRoleService;
import com.ccb.service.SysUserService;
import com.ccb.utils.Constants;
import com.mysql.cj.util.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;

/**
 * @author CCB
 * date: 14/10/2022 下午10:27
 * Description: 描述
 */
@Controller
@RequestMapping("/role")
public class SysRoleController {
    private Logger logger = Logger.getLogger(SysRoleController.class);
    ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
    SysUserService sysUserService = (SysUserService) ctx.getBean("sysUserService");
    SysRoleService sysRoleService = (SysRoleService) ctx.getBean("SysRoleService");

    /**
     * 进入角色信息列表
     *
     * @return
     */
    @RequestMapping("/list")
    public String list(Model model) {
        List<SysRole> roleList = sysRoleService.getList();
        logger.info("进入角色列表页面，sysRoles参数：" + roleList);
        if (roleList != null) {
            model.addAttribute("roleList", roleList);
            return "sysRole/list";
        }
        return "frame";
    }

    /**
     * 进入新增页面
     *
     * @return
     */
    @RequestMapping("/toAdd")
    public String toAdd() {
        logger.info("进入新增角色信息页面");
        return "sysRole/add";
    }

    /**
     * Ajax验证角色编码
     *
     * @param code
     * @return
     */
    @ResponseBody
    @GetMapping("/codeExist")
    public Object codeExist(@RequestParam String code) {
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        logger.info("进入Ajax验证角色编码，code参数：" + code);
        if (StringUtils.isNullOrEmpty(code)) {
            resultMap.put("exist", -1);
        } else {
            SysRole sysRole = sysRoleService.getSysRoleCode(code);
            if (sysRole != null) {
                resultMap.put("exist", 1);//账号已存在
            } else {
                resultMap.put("exist", 0);//账号可用
            }
        }
        return JSON.toJSONString(resultMap);
    }

    /**
     * 添加角色
     *
     * @param sysRole
     * @param session
     * @return
     */
    @PostMapping("/add")
    public String add(SysRole sysRole, HttpSession session) {
        logger.info("进入添加角色方法，sysRole参数：" + sysRole);
        sysRole.setCreatedUserId(((SysUser) session.getAttribute(Constants.USER_SESSION)).getId());
        if (sysRoleService.add(sysRole)) {
            return "redirect:/role/list";
        }
        return "sysRole/add";
    }

    /**
     * 进入修改页面
     *
     * @return
     */
    @RequestMapping("/toUpdate/{roleid}")
    public String toUpdate(@PathVariable Integer roleid, Model model) {
        logger.info("进入修改页面，roleid参数：" + roleid);
        SysRole sysRole = sysRoleService.getSysRoleId(roleid);
        model.addAttribute("sysRole", sysRole);
        return "sysRole/update";
    }

    /**
     * 修改角色信息
     *
     * @return
     */
    @PostMapping("/update")
    public String update(SysRole sysRole, HttpSession session) {
        logger.info("进入修改角色信息方法，sysRole参数：" + sysRole);
        sysRole.setCreatedUserId(((SysUser) session.getAttribute(Constants.USER_SESSION)).getId());
        if (sysRoleService.update(sysRole)) {
            return "redirect:/role/list";
        }
        return "sysRole/list";
    }

    /**
     * Ajax删除角色信息
     *
     * @param roleid
     */
    @ResponseBody
    @DeleteMapping("/del/{roleid}")
    public Object del(@PathVariable int roleid) {
        logger.info("进入删除角色信息方法，roleid参数" + roleid);
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
//        List<SysUser> userList = sysUserService.getUserList(null,null,null,null,roleid);
        List<SysUser> userList = sysUserService.getUserByRoleId(roleid);
        if (userList.size() > 0) {//判断该角色对应的用户是否存在
            resultMap.put("delResult", "false");
        } else {
            if (sysRoleService.delete(roleid)) {//判断是否删除成功
                resultMap.put("delResult", "true");
            } else {
                resultMap.put("delResult", "notexist");
            }
        }
        return JSON.toJSONString(resultMap);
    }
}
