package com.ccb.controller;


import com.alibaba.fastjson.JSON;
import com.ccb.pojo.SysRole;
import com.ccb.pojo.SysUser;
import com.ccb.service.SysRoleService;
import com.ccb.service.SysUserService;
import com.ccb.utils.Constants;
import com.ccb.utils.PageUtil;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.File;
import java.util.HashMap;
import java.util.List;

/**
 * @author CCB
 * date: 11/10/2022 下午5:27
 * Description: 描述
 */
@Controller
@RequestMapping("/user")
public class SysUSerController {
    private Logger logger = Logger.getLogger(SysUSerController.class);
    ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

    //跳转到登录页面
    public String login(){
        logger.debug("跳转到登录页面");
        return "login";
    }

    //登录
    @PostMapping("/login")
    public String doLogin(HttpServletRequest request, HttpSession session,@RequestParam String account, @RequestParam String password){
        logger.debug("登录方法");
        //调用service方法
        SysUserService sysUserService = (SysUserService) ctx.getBean("sysUserService");
        SysUser sysUser = sysUserService.login(account,password);
        if(sysUser != null){
            //登录成功,重定向到/user/toMain
            //将用户放入session
            session.setAttribute(Constants.USER_SESSION,sysUser);
            return "redirect:/user/toMain";
        }else {
            request.setAttribute("error","用户名或密码不正确");
            return "login";
        }
    }

    //跳转到首页
    @RequestMapping("/toMain")
    public String mian(HttpSession session){
        if(session.getAttribute(Constants.USER_SESSION)==null){
            //重定向到页面接口
            return "redirect:/user/toLogin";
        }
        return "frame";
    }

    //获取用户集合/条件查询
    @GetMapping("/list")
    public String getUserList(HttpSession session,Model model,String queryRealName,Integer queryRealId,@RequestParam(defaultValue = "1")Integer pageIndex){
        List<SysUser> userList = null;
        SysUserService sysUserService = (SysUserService) ctx.getBean("sysUserService");
        SysRoleService sysRoleService = (SysRoleService) ctx.getBean("SysRoleService");
        logger.debug(1);
        try{
            logger.debug(2);
            if(queryRealName==""){
                queryRealName = null;
            }
            logger.debug(3);
            //设置页面容量
            int pageSize = Constants.pageSize;
            //总数量(表)
            int totalCount = sysUserService.getUserCount(queryRealName,queryRealId);
            System.out.println("queryRealId："+queryRealId);
            System.out.println("totalCount："+totalCount);
            PageUtil page = new PageUtil();
            page.setNowPage(pageIndex);
            page.setShowSize(pageSize);
            page.setCountPage(totalCount);
            int totalPageCount = page.getTotalPageCount();
            //控制首页和尾页
            logger.debug(4);
            if(pageIndex > totalPageCount  && totalPageCount!=0){
                pageIndex = totalPageCount;
                throw new RuntimeException("页码不正确");
            }
            userList = sysUserService.getUserList(queryRealName,queryRealId,page.getOffset(),page.getShowSize(),null);
            System.out.println("userList"+userList.size());
            model.addAttribute("userList",userList);
            List<SysRole> roleList = null;
            //这里是角色表信息没有写
            roleList = sysRoleService.getRoleList(null);
            model.addAttribute("roleList",roleList);
            session.setAttribute("roleList",roleList);
            model.addAttribute("queryRealName",queryRealName);
            model.addAttribute("queryRealId",queryRealId);
            model.addAttribute("totalPageCount",totalPageCount);
            model.addAttribute("totalCount",totalCount);
            model.addAttribute("currentPageNo",pageIndex);
        }catch (Exception e){
            e.printStackTrace();
            logger.error("用户列表接口访问失败");
            return "redirect:/user/toSysError";
        }
        return "sysUser/list";
    }

    //跳转到添加用户页面
    @GetMapping("/toAdd")
    public String toAdd(){
        return "sysUser/add";
    }

    //新增用户
    @PostMapping("/add")
    public String add(@Valid SysUser sysUser, HttpSession session,HttpServletRequest request, BindingResult bindingResult, MultipartFile[] attachs){
        SysUserService sysUserService = (SysUserService) ctx.getBean("sysUserService");
        if(bindingResult.hasErrors()){
            logger.debug("添加用户验证失败");
            return "sysUser/add";
        }
        String idPicPath = null;
        String workPicPath = null;
        String errorInfo = null;
        boolean flag = true;
        String path = request.getSession().getServletContext().getRealPath("statics"+ File.separator+"uploadfiles");
        logger.info("文件存储路径："+path);

        for (int i=0;i<attachs.length;i++){
            MultipartFile attach = attachs[i];
            //判断文件是否为空
            if(!attach.isEmpty()){
                if(i == 0){
                    errorInfo = "uploadFileError";
                }else if(i == 1){
                    errorInfo = "uploadWpError";
                }
                String originalFile = attach.getOriginalFilename();//原文件名
                logger.info("原文件名名："+originalFile);
                String prefix = FilenameUtils.getExtension(originalFile);//原文件后缀
                logger.debug("原文件后缀为："+prefix);
                int filesize = 15000000;
                logger.debug("文件大小："+attach.getSize());


                if(attach.getSize()>filesize){//上传大小不得超过500kb
                    request.setAttribute("uploadFileError","*上传大小不得超过500kb");
                    flag = false;
                }else if(prefix.equalsIgnoreCase("jpg")
                        || prefix.equalsIgnoreCase("png")
                        || prefix.equalsIgnoreCase("jpeg")
                        || prefix.equalsIgnoreCase("pneg")){//上传图片不正确
                    String fileName = System.currentTimeMillis()+ RandomUtils.nextInt(1000000)+"_Personal."+prefix;
                    logger.debug("新文件名称："+fileName);
                    File targetFIle = new File(path);
                    if(!targetFIle.exists()){
                        targetFIle.mkdirs();
                    }
                    //保存
                    try {
                        attach.transferTo(new File(targetFIle,fileName));
                    }catch (Exception e){
                        e.printStackTrace();
                        request.setAttribute(errorInfo,"* 上传失败！");
                        flag = false;
                    }
                    if(i == 0){
                        idPicPath = File.separator+"statics"+File.separator+"uploadfiles"+File.separator+fileName;
                    }else if(i == 1){
                        workPicPath = File.separator+"statics"+File.separator+"uploadfiles"+File.separator+fileName;
                    }
                    logger.debug("idPicPath:"+idPicPath);
                    logger.debug("workPicPath:"+workPicPath);
                }else {
                    request.setAttribute(errorInfo,"* 上传图片格式不正确");
                }
            }
        }
        if(flag){
            sysUser.setCreatedUserId(((SysUser)session.getAttribute(Constants.USER_SESSION)).getId());
            System.out.println(sysUser.getRealName());
            System.out.println(sysUser.getBirthday());
            System.out.println(idPicPath);
            System.out.println(workPicPath);
            sysUser.setIdPicPath(idPicPath);
            sysUser.setWorkPicPath(workPicPath);
            if (sysUserService.addUser(sysUser)==1){
                return "redirect:/user/list";
            }
        }
        return "sysUser/add";
    }

    //用户详情
    @RequestMapping("/view/{userid}")
    public String view(Model model, @PathVariable Integer userid){
        SysUserService sysUserService = (SysUserService) ctx.getBean("sysUserService");
        List<SysUser> sysUsersList = sysUserService.getUserList(null,null,0,1,userid);
        SysUser sysUser = new SysUser();
        for (SysUser sysUsers : sysUsersList){
            sysUser.setId(sysUsers.getId());
            sysUser.setAccount(sysUsers.getAccount());
            sysUser.setSex(sysUsers.getSex());
            sysUser.setBirthday(sysUsers.getBirthday());
            sysUser.setPhone(sysUsers.getPhone());
            sysUser.setAddress(sysUsers.getAddress());
            sysUser.setRealName(sysUsers.getRealName());
            sysUser.setRoleName(sysUsers.getRoleName());
            sysUser.setIdPicPath(sysUsers.getIdPicPath());
            sysUser.setWorkPicPath(sysUsers.getWorkPicPath());
        }
        model.addAttribute("sysUser",sysUser);
        return "sysUser/view";
    }

    //跳转到异常处理页面
    @RequestMapping(value = "/toSysError")
    public String sysError(){
        return "sysError";
    }

    @GetMapping("/toUpdate")
    public String toUpdate(@RequestParam Integer uid,Model model){
        logger.debug("跳转到修改用户信息页面");
        SysUserService sysUserService = (SysUserService) ctx.getBean("sysUserService");
        SysRoleService sysRoleService = (SysRoleService) ctx.getBean("SysRoleService");
        List<SysUser> sysUsersList = sysUserService.getUserList(null,null,0,1,uid);
        SysUser sysUser = new SysUser();
        for (SysUser sysUsers : sysUsersList){
            sysUser.setId(sysUsers.getId());
            sysUser.setAccount(sysUsers.getAccount());
            sysUser.setSex(sysUsers.getSex());
            sysUser.setBirthday(sysUsers.getBirthday());
            sysUser.setPhone(sysUsers.getPhone());
            sysUser.setAddress(sysUsers.getAddress());
            sysUser.setRealName(sysUsers.getRealName());
            sysUser.setRoleId(sysUsers.getRoleId());
        }
        List<SysRole> roleList = null;
        roleList = sysRoleService.getRoleList(null);
        model.addAttribute("roleList",roleList);
        model.addAttribute("sysUser",sysUser);
        return "sysUser/update";
    }

    //修改用户
    @PostMapping("/toUpdate")
    public String Update(SysUser sysUser,HttpServletRequest request,MultipartFile[] attachs){

        String idPicPath = null;
        String workPicPath = null;
        String errorInfo = null;
        boolean flag = true;
        String path = request.getSession().getServletContext().getRealPath("statics"+ File.separator+"uploadfiles");
        logger.info("文件存储路径："+path);
        for (int i=0;i<attachs.length;i++){
            MultipartFile attach = attachs[i];
            //判断文件是否为空
            if(!attach.isEmpty()){
                if(i == 0){
                    errorInfo = "uploadFileError";
                }else if(i == 1){
                    errorInfo = "uploadWpError";
                }
                String originalFile = attach.getOriginalFilename();//原文件名
                logger.info("原文件名名："+originalFile);
                String prefix = FilenameUtils.getExtension(originalFile);//原文件后缀
                logger.debug("原文件后缀为："+prefix);
                int filesize = 500000;
                logger.debug("文件大小："+attach.getSize());


                if(attach.getSize()>filesize){//上传大小不得超过500kb
                    request.setAttribute("uploadFileError","*上传大小不得超过500kb");
                    flag = false;
                }else if(prefix.equalsIgnoreCase("jpg")
                        || prefix.equalsIgnoreCase("png")
                        || prefix.equalsIgnoreCase("jpeg")
                        || prefix.equalsIgnoreCase("pneg")){//上传图片不正确
                    String fileName = System.currentTimeMillis()+ RandomUtils.nextInt(1000000)+"_Personal."+prefix;
                    logger.debug("新文件名称："+fileName);
                    File targetFIle = new File(path);
                    if(!targetFIle.exists()){
                        targetFIle.mkdirs();
                    }
                    //保存
                    try {
                        attach.transferTo(new File(targetFIle,fileName));
                    }catch (Exception e){
                        e.printStackTrace();
                        request.setAttribute(errorInfo,"* 上传失败！");
                        flag = false;
                    }
                    if(i == 0){
                        idPicPath = File.separator+"statics"+File.separator+"uploadfiles"+File.separator+fileName;
                    }else if(i == 1){
                        workPicPath = File.separator+"statics"+File.separator+"uploadfiles"+File.separator+fileName;
                    }
                    logger.debug("idPicPath:"+idPicPath);
                    logger.debug("workPicPath:"+workPicPath);
                }else {
                    request.setAttribute(errorInfo,"* 上传图片格式不正确");
                }
            }
        }

        sysUser.setIdPicPath(idPicPath);
        sysUser.setWorkPicPath(workPicPath);
        SysUserService sysUserService = (SysUserService) ctx.getBean("sysUserService");
        sysUserService.modifyUser(sysUser);

        System.out.println(sysUser.getRealName());
        System.out.println(sysUser.getBirthday());
        System.out.println(sysUser.getSex());
        System.out.println(sysUser.getPhone());
        System.out.println(sysUser.getAddress());
        System.out.println("角色"+sysUser.getRoleId());

        return "redirect:/user/list";
    }

    //退出登录
    @RequestMapping("/logout")
    public String logout(HttpSession session){
        //清除登录用户信息
        session.removeAttribute(Constants.USER_SESSION);
        try{
            Thread.sleep(500);
        }catch(InterruptedException ex){
            Thread.currentThread().interrupt();
        }
        return "login";
    }

    ///查询用户是否存在
    @ResponseBody
    @GetMapping("/userExist")
    public Object userExist(@RequestParam String account){
        SysUserService sysUserService = (SysUserService) ctx.getBean("sysUserService");
        logger.debug("验证用户名account="+account+"的用户是否存在");
        HashMap<String,Object> resultMap = new HashMap<String, Object>();
        if(account==null || account == ""){
            resultMap.put("exist",1);
        }else {
            SysUser sysUser = sysUserService.getUser(account);
            if(sysUser != null){
                resultMap.put("exist",1);
            }else {
                resultMap.put("exist",0);
            }
        }
        return JSON.toJSONString(resultMap);
    }

    //根据id删除用户
    @ResponseBody
    @DeleteMapping("/del/{id}")
    public Object delUser(@PathVariable Integer id){
        SysUserService sysUserService = (SysUserService) ctx.getBean("sysUserService");
        HashMap<String,Object> resultMap = new HashMap<String, Object>();
        int delResult = sysUserService.delUser(id);
        System.out.println(delResult);
        if(delResult==1){
            resultMap.put("delResult","true");
        }else if(delResult==0){
            resultMap.put("delResult","notexist");
        }else {
            resultMap.put("delResult","false");
        }
        return JSON.toJSONString(resultMap);
    }

    //查询角色集合
    @ResponseBody
    @GetMapping(value = "/getRole",produces = {"aplication/json;charset=UTF-8"})
    public Object getRole(){
        SysRoleService sysRoleService = (SysRoleService) ctx.getBean("SysRoleService");
        HashMap<String,Object> resultMap = new HashMap<String, Object>();
        List<SysRole> sysRoleList = sysRoleService.getRoleList(null);
        return JSON.toJSONString(sysRoleList);
    }

    //跳转至修改密码页面
    @GetMapping("/toUpdatePwd")
    public String toUpdatePwd(){
        return "sysUser/updatePassword";
    }

    //修改密码执行
    @PostMapping("/savePassword")
    public String UpdatePwd(Integer id,String newPassword,HttpSession session){
        SysUserService sysUserService = (SysUserService) ctx.getBean("sysUserService");

        System.out.println(id);
        System.out.println(newPassword);
        sysUserService.modipwd(id,newPassword);

        SysUser sysUser = (SysUser) session.getAttribute(Constants.USER_SESSION);
        sysUser.setPassword(newPassword);
        session.setAttribute(Constants.USER_SESSION,sysUser);
        return "sysUser/updatePassword";
    }

    //验证旧密码是否正确
    @ResponseBody
    @PostMapping(value = "/checkOldPwd",produces = {"aplication/json;charset=UTF-8"})
    public Object checkOldPwd(HttpSession session,String oldPassword){
        String result;
        SysUser sysUser = (SysUser) session.getAttribute(Constants.USER_SESSION);

        HashMap<String,Object> map = new HashMap<String, Object>();
        if(sysUser.getPassword().equals(oldPassword)){
            map.put("result","true");
        }else if(!(sysUser.getPassword().equals(oldPassword))){
            map.put("result","false");
        }else if(sysUser.getPassword().equals(null)){
            map.put("result","error");
        }
        return JSON.toJSONString(map);
    }
}
