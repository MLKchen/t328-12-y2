package com.ccb.controller;

import com.alibaba.fastjson.JSON;
import com.ccb.pojo.StorageRecord;
import com.ccb.pojo.SysSupplier;
import com.ccb.pojo.SysUser;
import com.ccb.service.SysStorageRecordService;
import com.ccb.service.SysSupplierService;
import com.ccb.utils.Constants;
import com.ccb.utils.PageUtil;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.math.RandomUtils;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.HashMap;
import java.util.List;

/**
 * @author CCB
 * date: 14/10/2022 下午7:03
 * Description: 描述
 */
@Controller
@RequestMapping("/supplier")
public class SysSupplierController {
    private Logger logger = Logger.getLogger(SysSupplierController.class);
    ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
    SysSupplierService supplierService = (SysSupplierService) ctx.getBean("SysSupplierService");

    @ResponseBody
    @GetMapping(value = "/simpleList",produces = {"aplication/json;charset=UTF-8"})
    public String simpleList(){

        List<SysSupplier> sysSupplierList = supplierService.getSupplierList(null);
        return JSON.toJSONString(sysSupplierList);
    }

    /**
     * 进入供应商信息列表
     *
     * @return
     */
    @GetMapping("/list")
    public String list(Model model, String querySupCode, String querySupName, @RequestParam(defaultValue = "1") Integer pageIndex) {
        List<SysSupplier> supplierList = null;
        System.out.println(1);
        try {
            //设置页面容量
            int pageSize = Constants.pageSize;
            //总数量
            int totalCount = supplierService.getSupplierCount(querySupCode, querySupName);
            System.out.println("querySupCode"+querySupCode);
            System.out.println("querySupName"+querySupName);
            System.out.println("totalCount"+totalCount);
            //总页数
            PageUtil pages = new PageUtil();
            pages.setNowPage(pageIndex);
            pages.setShowSize(pageSize);
            pages.setCountPage(totalCount);
            int totalPageCount = pages.getTotalPageCount();
            logger.debug("totalPageCount"+totalPageCount);
            logger.debug("pageIndex"+pageIndex);
            System.out.println("totalPageCount"+totalPageCount);
            System.out.println("pageIndex"+pageIndex);
            //控制首页和尾页
            if (pageIndex > totalPageCount && totalPageCount!=0) {
                pageIndex = totalPageCount;
                throw new RuntimeException("页码不正确");
            }

            supplierList = supplierService.getList(querySupCode, querySupName, pages.getOffset(), pages.getShowSize());

            model.addAttribute("supplierList", supplierList);
            model.addAttribute("querySupCode", querySupCode);
            model.addAttribute("querySupName", querySupName);
            model.addAttribute("totalPageCount", totalPageCount);
            model.addAttribute("totalCount", totalCount);
            model.addAttribute("currentPageNo", pageIndex);
            logger.info("供应商列表页码：" + pageIndex);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info("供应商列表接口访问失败");
            return "sysError";
        }
        return "/supplier/list";
    }

    /**
     * 进入新增页面
     *
     * @return
     */
    @RequestMapping("/toAdd")
    public String toAdd() {
        logger.info("进入新增页面");
        return "/supplier/add";
    }

    /**
     * 添加供应商
     *
     * @param supplier
     * @param session
     * @return
     */
    @PostMapping("/add")
    public String add(SysSupplier supplier, HttpSession session, HttpServletRequest request, @RequestParam(value = "attachs", required = false) MultipartFile[] attachs) {
        String companyLicPicPath = null;
        String orgCodePicPath = null;
        String errorInfo = null;
        boolean flag = true;
        String path = session.getServletContext().getRealPath("statics" + File.separator + "uploadfiles");
        logger.info("文件存储路径：" + path);
        for (int i = 0; i < attachs.length; i++) {
            MultipartFile attach = attachs[i];
            if (!attach.isEmpty()) {
                if (i == 0) {
                    errorInfo = "uploadFileError";
                } else if (i == 1) {
                    errorInfo = "uploadOcError";
                }
                String oldFileName = attach.getOriginalFilename();//原文件名
                logger.info("原文件名：" + oldFileName);//原文件后缀
                String prefix = FilenameUtils.getExtension(oldFileName);
                logger.info("原文件名后缀：" + prefix);
                int filesize = 15000000;
                logger.info("上传文件大小：" + attach.getSize());
                if (attach.getSize() > filesize) {//上传大小不得超过500KB
                    request.setAttribute(errorInfo, " * 上传大小不得超过1500000KB");
                    flag = false;
                } else if (prefix.equalsIgnoreCase("jpg")
                        || prefix.equalsIgnoreCase("png")
                        || prefix.equalsIgnoreCase("jpeg")
                        || prefix.equalsIgnoreCase("pneg")) {//上传图片格式不正确
                    String fileName = System.currentTimeMillis() + RandomUtils.nextInt(150000000) + "_Personal." + prefix;
                    logger.info("新文件名称：" + fileName);
                    File targetFile = new File(path);
                    if (!targetFile.exists()) {
                        targetFile.mkdirs();
                    }
                    //保存
                    try {
                        attach.transferTo(new File(targetFile, fileName));
                    } catch (Exception e) {
                        e.printStackTrace();
                        request.setAttribute(errorInfo, " * 上传失败！");
                        flag = false;
                    }
                    if (i == 0) {
                        companyLicPicPath = File.separator + "statics" + File.separator + "uploadfiles" + File.separator + fileName;
                    } else if (i == 1) {
                        orgCodePicPath = File.separator + "statics" + File.separator + "uploadfiles" + File.separator + fileName;
                    }
                    logger.info("companyLicPicPath：" + companyLicPicPath);
                    logger.info("orgCodePicPath：" + orgCodePicPath);
                } else {
                    request.setAttribute(errorInfo, " * 上传图片格式不正确");
                    flag = false;
                }
            }
        }
        if (flag) {
            logger.info("进入添加供应商方法，参数：" + supplier);
            supplier.setCreatedUserId(((SysUser) session.getAttribute(Constants.USER_SESSION)).getId());
            supplier.setCompanyLicPicPath(companyLicPicPath);
            supplier.setOrgCodePicPath(orgCodePicPath);
            if (supplierService.add(supplier)) {
                return "redirect:/supplier/list";
            }
        }
        return "supplier/add";
    }

    /**
     * 进入供应商详情页面
     *
     * @return
     */
    @RequestMapping("/view/{supId}")
    public String view(@PathVariable Integer supId, Model model) {
        logger.info("进入进入供应商详情页面");
        SysSupplier supplier = supplierService.getSupplierById(supId);
        model.addAttribute("supplier", supplier);
        return "/supplier/view";
    }

    /**
     * 进入修改页面
     *
     * @return
     */
    @RequestMapping("/toUpdate/{supId}")
    public String toUpdate(@PathVariable Integer supId, Model model) {
        SysSupplier supplier = supplierService.getSupplierById(supId);
        model.addAttribute("supplier", supplier);
        return "/supplier/update";
    }

    /**
     * 修改供应商信息
     *
     * @param supplier
     * @param session
     * @return
     */
    @PostMapping("/update")
    public String update(SysSupplier supplier, HttpSession session, HttpServletRequest request, @RequestParam(value = "attachs", required = false) MultipartFile[] attachs) {
        String companyLicPicPath = null;
        String orgCodePicPath = null;
        String errorInfo = null;
        boolean flag = true;
        String path = session.getServletContext().getRealPath("statics" + File.separator + "uploadfiles");
        logger.info("文件存储路径：" + path);
        for (int i = 0; i < attachs.length; i++) {
            MultipartFile attach = attachs[i];
            if (!attach.isEmpty()) {
                if (i == 0) {
                    errorInfo = "uploadFileError";
                } else if (i == 1) {
                    errorInfo = "uploadOcError";
                }
                String oldFileName = attach.getOriginalFilename();//原文件名
                logger.info("原文件名：" + oldFileName);//原文件后缀
                String prefix = FilenameUtils.getExtension(oldFileName);
                logger.info("原文件名后缀：" + prefix);
                int filesize = 150000000;
                logger.info("上传文件大小：" + attach.getSize());
                if (attach.getSize() > filesize) {//上传大小不得超过500KB
                    request.setAttribute(errorInfo, " * 上传大小不得超过1500000KB");
                    flag = false;
                } else if (prefix.equalsIgnoreCase("jpg")
                        || prefix.equalsIgnoreCase("png")
                        || prefix.equalsIgnoreCase("jpeg")
                        || prefix.equalsIgnoreCase("pneg")) {//上传图片格式不正确
                    String fileName = System.currentTimeMillis() + RandomUtils.nextInt(150000000) + "_Personal." + prefix;
                    logger.info("新文件名称：" + fileName);
                    File targetFile = new File(path);
                    if (!targetFile.exists()) {
                        targetFile.mkdirs();
                    }
                    //保存
                    try {
                        attach.transferTo(new File(targetFile, fileName));
                    } catch (Exception e) {
                        e.printStackTrace();
                        request.setAttribute(errorInfo, " * 上传失败！");
                        flag = false;
                    }
                    if (i == 0) {
                        companyLicPicPath = File.separator + "statics" + File.separator + "uploadfiles" + File.separator + fileName;
                    } else if (i == 1) {
                        orgCodePicPath = File.separator + "statics" + File.separator + "uploadfiles" + File.separator + fileName;
                    }
                    logger.info("companyLicPicPath：" + companyLicPicPath);
                    logger.info("orgCodePicPath：" + orgCodePicPath);
                } else {
                    request.setAttribute(errorInfo, " * 上传图片格式不正确");
                    flag = false;
                }
            }
        }
        if (flag) {
            logger.info("进入修改供应商方法，参数：" + supplier);
            supplier.setUpdatedUserId(((SysUser) session.getAttribute(Constants.USER_SESSION)).getId());
            supplier.setCompanyLicPicPath(companyLicPicPath);
            supplier.setOrgCodePicPath(orgCodePicPath);
            if (supplierService.updateSupplier(supplier)) {
                return "redirect:/supplier/list";
            }
        }
        return "supplier/update";
    }

    /**
     * Ajax删除供应商信息
     *
     * @param supId
     */
    @ResponseBody
    @DeleteMapping("/del/{supId}")
    public Object del(@PathVariable Integer supId) {
        SysStorageRecordService sysStorageRecordService = (SysStorageRecordService) ctx.getBean("SysStorageRecordService");
        logger.info("进入删除供应商信息方法，参数" + supId);
        HashMap<String, Object> resultMap = new HashMap<String, Object>();
        StorageRecord storageRecordList = sysStorageRecordService.getRecord(supId);
        if (storageRecordList != null) {//判断该角色对应的用户是否存在
            resultMap.put("delResult", "storageRecordList != null");
        } else {
            if (supplierService.delSupplier(supId)) {//判断是否删除成功
                resultMap.put("delResult", "true");
            } else {
                resultMap.put("delResult", "false");
            }
        }
        return JSON.toJSONString(resultMap);
    }


}
