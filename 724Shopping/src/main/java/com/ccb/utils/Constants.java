package com.ccb.utils;

/**
 * 常量类
 */
public class Constants {
	public final static String USER_SESSION = "userSession";
	public final static String SYS_MESSAGE = "message";
	public final static int pageSize = 5;
}
