package com.ccb.dao;

import com.ccb.pojo.StorageRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CCB
 * date: 14/10/2022 下午2:49
 * Description: 入库信息接口
 */
public interface SysStorageRecordMapper {

    /**
     * 获取入库信息记录
     * @return
     */
    List<StorageRecord> getRecordList(@Param("goodsName") String goodsName, @Param("supplierId") String supplierId,
                                      @Param("payStatus")  String payStatus,@Param("pageIndex") Integer pageIndex,
                                      @Param("pageSize") Integer pageSize);

    /**
     * 获取总数量
     * @param goodsName
     * @param supplierId
     * @param payStatus
     * @return
     */
    Integer getRecordCount(@Param("goodsName") String goodsName, @Param("supplierId") String supplierId,
                           @Param("payStatus")  String payStatus);

    /**
     * 添加供货商
     * @param storageRecord
     * @return
     */
    int addRecordCount(StorageRecord storageRecord);

    /**
     * 根据id获取入库信息
     * @param id
     * @return
     */
    StorageRecord getRecord(@Param("id") Integer id);

    /**
     * 修改入库信息
     * @param storageRecord
     * @return
     */
    int modifyRecord(StorageRecord storageRecord);

    /**
     * 根据id删除入库信息
     * @param id
     * @return
     */
    int del(Integer id);

    /**
     * 根据供应商编号查询入库记录
     * @return
     */
    List<StorageRecord> getStorageRecordBySupplierId(@Param("supplierId") Integer supplierId);
}
