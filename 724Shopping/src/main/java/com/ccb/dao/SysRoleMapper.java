package com.ccb.dao;

import com.ccb.pojo.SysRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CCB
 * date: 12/10/2022 上午10:19
 * Description: 角色
 */
public interface SysRoleMapper {
    /**
     * 获取角色ID
     * @param roleId
     * @return
     */
    List<SysRole> getRoleList(@Param("roleId")Integer roleId);

    /**
     * 查询角色表信息
     * @return
     */
    List<SysRole> selectSysRoleList();

    /**
     * 根据角色编码查询角色
     * @param code
     * @return
     */
    SysRole getSysRoleCode(@Param("code")String code);

    /**
     * 新增角色表信息
     * @param sysRole
     * @return
     */
    int add(SysRole sysRole);

    /**
     * 根据角色编号查询角色
     * @param id
     * @return
     */
    SysRole getSysRoleId(@Param("id")Integer id);

    /**
     * 修改角色信息
     * @param sysRole
     * @return
     */
    int update(SysRole sysRole);

    /**
     * 删除角色信息
     * @param id
     * @return
     */
    int delete(@Param("id")Integer id);
}
