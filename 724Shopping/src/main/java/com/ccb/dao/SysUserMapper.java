package com.ccb.dao;

import com.ccb.pojo.SysUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author CCB
 * date: 11/10/2022 下午7:01
 * Description: 描述
 */
public interface SysUserMapper {
    /**
     * 登录
     *
     * @param account
     * @param password
     * @return
     */
    SysUser login(@Param("account") String account, @Param("password") String password);

    /**
     * 获取总数量
     *
     * @param realName
     * @param roleId
     * @return
     */
    Integer getUserCount(@Param("realName") String realName, @Param("roleId") Integer roleId);

    /**
     * 查询用户列表
     *
     * @param queryRealName
     * @param queryRoleId
     * @param pageIndex
     * @param pageSize
     * @return
     */
    List<SysUser> getUserList(@Param("queryRealName") String queryRealName, @Param("queryRoleId") Integer queryRoleId,
                              @Param("pageIndex") Integer pageIndex, @Param("pageSize") Integer pageSize, @Param("userid") Integer userid);

    /**
     * 新增用户
     *
     * @param sysUser
     * @return
     */
    int addUser(SysUser sysUser);

    /**
     * 修改用户
     *
     * @param sysUser
     * @return
     */
    int modifyUser(SysUser sysUser);

    /**
     * 根据用户查询用户信息
     *
     * @param account
     * @return
     */
    SysUser getUser(@Param("account") String account);

    /**
     * 根据id删除用户
     *
     * @param id
     * @return
     */
    int delUser(@Param("id") Integer id);

    /**
     * 根据id修改密码
     *
     * @param id
     * @param password
     * @return
     */
    int modipwd(@Param("id") Integer id, @Param("password") String password);

    /**
     * 根据id获取用户
     *
     * @param id
     * @return
     */
    List<SysUser> getUserByRoleId(@Param("id") Integer id);
}
