package com.ccb.pojo;

import java.util.List;

/**
 * @author CCB
 * date: 25/8/2022 下午3:16
 * Description: 商家表实体类
 */
public class SysSupplier {
    private Integer id;//主键ID
    private String supCode;//供货商编号
    private String supName;//供货商名称
    private String supDesc;//供货商描述
    private String supContact;//供货商联系人
    private String supPhone;//联系电话
    private String supAddress;//供货商地址
    private String supFax;//传真
    private Integer createdUserId;//创建人id
    private String createdTime;//创建时间
    private String updatedTime;//修改时间
    private Integer updatedUserId;//修改人id

    private String companyLicPicPath;
    private String orgCodePicPath;

    private List<StorageRecord> storageRecordList;//商品图库集合

    public SysSupplier() {
    }

    public SysSupplier(Integer id, String supCode, String supName, String supDesc, String supContact, String supPhone, String supAddress, String supFax, Integer createdUserId, String createdTime, String updatedTime, Integer updatedUserId) {
        this.id = id;
        this.supCode = supCode;
        this.supName = supName;
        this.supDesc = supDesc;
        this.supContact = supContact;
        this.supPhone = supPhone;
        this.supAddress = supAddress;
        this.supFax = supFax;
        this.createdUserId = createdUserId;
        this.createdTime = createdTime;
        this.updatedTime = updatedTime;
        this.updatedUserId = updatedUserId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSupCode() {
        return supCode;
    }

    public void setSupCode(String supCode) {
        this.supCode = supCode;
    }

    public String getSupName() {
        return supName;
    }

    public void setSupName(String supName) {
        this.supName = supName;
    }

    public String getSupDesc() {
        return supDesc;
    }

    public void setSupDesc(String supDesc) {
        this.supDesc = supDesc;
    }

    public String getSupContact() {
        return supContact;
    }

    public void setSupContact(String supContact) {
        this.supContact = supContact;
    }

    public String getSupPhone() {
        return supPhone;
    }

    public void setSupPhone(String supPhone) {
        this.supPhone = supPhone;
    }

    public String getSupAddress() {
        return supAddress;
    }

    public void setSupAddress(String supAddress) {
        this.supAddress = supAddress;
    }

    public String getSupFax() {
        return supFax;
    }

    public void setSupFax(String supFax) {
        this.supFax = supFax;
    }

    public Integer getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Integer createdUserId) {
        this.createdUserId = createdUserId;
    }

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(String updatedTime) {
        this.updatedTime = updatedTime;
    }

    public Integer getUpdatedUserId() {
        return updatedUserId;
    }

    public void setUpdatedUserId(Integer updatedUserId) {
        this.updatedUserId = updatedUserId;
    }

    public List<StorageRecord> getStorageRecordList() {
        return storageRecordList;
    }

    public void setStorageRecordList(List<StorageRecord> storageRecordList) {
        this.storageRecordList = storageRecordList;
    }

    public String getCompanyLicPicPath() {
        return companyLicPicPath;
    }

    public void setCompanyLicPicPath(String companyLicPicPath) {
        this.companyLicPicPath = companyLicPicPath;
    }

    public String getOrgCodePicPath() {
        return orgCodePicPath;
    }

    public void setOrgCodePicPath(String orgCodePicPath) {
        this.orgCodePicPath = orgCodePicPath;
    }
}
