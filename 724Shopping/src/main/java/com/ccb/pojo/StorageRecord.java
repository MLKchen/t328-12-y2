package com.ccb.pojo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author CCB
 * date: 25/8/2022 下午3:53
 * Description: 入库记录实体类
 */
public class StorageRecord {
    private Integer id;//入库记录ID
    private String srCode;//入库记录编码
    private String goodsName;//商品名称
    private String goodsDesc;//商品描述
    private String goodsUnit;//商品单位
    private BigDecimal goodsCount;//入库数量
    private BigDecimal totalAmount;//商品总额
    private Integer payStatus;//支付状态
    private Integer supplierId;//供应商ID
    private Integer createdUserId;//创建人ID
    private Date createdTime;//创建时间
    private Integer updatedUserId;//修改人ID
    private Date updatedTime;//修改时间
//    private String sipName;

    private SysSupplier sysSupplier;//入库实体类

    public StorageRecord() {
    }

    public StorageRecord(Integer id, String srCode, String goodsName, String goodsDesc, String goodsUnit, BigDecimal goodsCount, BigDecimal totalAmount, Integer payStatus, Integer supplierId, Integer createdUserId, Date createdTime, Integer updatedUserId, Date updatedTime) {
        this.id = id;
        this.srCode = srCode;
        this.goodsName = goodsName;
        this.goodsDesc = goodsDesc;
        this.goodsUnit = goodsUnit;
        this.goodsCount = goodsCount;
        this.totalAmount = totalAmount;
        this.payStatus = payStatus;
        this.supplierId = supplierId;
        this.createdUserId = createdUserId;
        this.createdTime = createdTime;
        this.updatedUserId = updatedUserId;
        this.updatedTime = updatedTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSrCode() {
        return srCode;
    }

    public void setSrCode(String srCode) {
        this.srCode = srCode;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsDesc() {
        return goodsDesc;
    }

    public void setGoodsDesc(String goodsDesc) {
        this.goodsDesc = goodsDesc;
    }

    public String getGoodsUnit() {
        return goodsUnit;
    }

    public void setGoodsUnit(String goodsUnit) {
        this.goodsUnit = goodsUnit;
    }

    public BigDecimal getGoodsCount() {
        return goodsCount;
    }

    public void setGoodsCount(BigDecimal goodsCount) {
        this.goodsCount = goodsCount;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public Integer getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(Integer supplierId) {
        this.supplierId = supplierId;
    }

    public Integer getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Integer createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Integer getUpdatedUserId() {
        return updatedUserId;
    }

    public void setUpdatedUserId(Integer updatedUserId) {
        this.updatedUserId = updatedUserId;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public SysSupplier getSysSupplier() {
        return sysSupplier;
    }

    public void setSysSupplier(SysSupplier sysSupplier) {
        this.sysSupplier = sysSupplier;
    }

//    public String getSipName() {
//        return sipName;
//    }
//
//    public void setSipName(String sipName) {
//        this.sipName = sipName;
//    }
}
