package com.ccb.pojo;

import java.util.Date;

/**
 * @author CCB
 * date: 26/8/2022 上午8:47
 * Description: 角色表实体类
 */
public class SysRole {
    private Integer id;//主键ID
    private String code;//角色编码
    private String roleName;//角色名称
    private Integer createdUserId;//创建者ID
    private Date createdTime;//创建时间
    private Integer updatedUserId;//修改者
    private Date updatedTime;//修改时间

    public SysRole() {
    }

    public SysRole(Integer id, String code, String roleName, Integer createdUserId, Date createdTime, Integer updatedUserId, Date updatedTime) {
        this.id = id;
        this.code = code;
        this.roleName = roleName;
        this.createdUserId = createdUserId;
        this.createdTime = createdTime;
        this.updatedUserId = updatedUserId;
        this.updatedTime = updatedTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public Integer getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Integer createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Integer getUpdatedUserId() {
        return updatedUserId;
    }

    public void setUpdatedUserId(Integer updatedUserId) {
        this.updatedUserId = updatedUserId;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }
}
