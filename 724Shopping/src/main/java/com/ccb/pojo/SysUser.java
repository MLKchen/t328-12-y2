package com.ccb.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author CCB
 * date: 2022/8/24 17:48
 * Description: 用户表实体类
 */
public class SysUser implements java.io.Serializable {
    /*字段*/
    private Integer id;//ID
    @NotEmpty(message = "用户编码不能为空")
    private String account;//账号
    @NotEmpty(message = "密码不能为空")
    @Length(min = 6,max = 10,message = "用户密码长度为6-10")
    private String password;//用户密码
    @NotEmpty(message = "用户名称不能为空")
    private String realName;//真实姓名
    private Integer sex;//性别
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Past(message = "必须是一个过去的时间")
    @JSONField(format = "yyyy-MM-dd")
    private Date birthday;//出生日期
    private String address;//地址
    private String phone;//电话
    @Min(1)
    private Integer roleId;//用户角色
    private Integer createdUserId;//创建者
    private Date createdTime;//创建时间
    private Integer updatedUserId;//更新者
    private Date updatedTime;//更新时间

    private  String roleName;//角色名称

    private SysRole sysRole;//系统角色实体类

    private List<Address> addressesList;//用户地址列表

    private String idPicPath;//证件照
    private String workPicPath;

    private Integer age;//年龄

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(Integer createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Integer getUpdatedUserId() {
        return updatedUserId;
    }

    public void setUpdatedUserId(Integer updatedUserId) {
        this.updatedUserId = updatedUserId;
    }

    public Date getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(Date updatedTime) {
        this.updatedTime = updatedTime;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public SysRole getSysRole() {
        return sysRole;
    }

    public void setSysRole(SysRole sysRole) {
        this.sysRole = sysRole;
    }

    public List<Address> getAddressesList() {
        return addressesList;
    }

    public void setAddressesList(List<Address> addressesList) {
        this.addressesList = addressesList;
    }

    public String getIdPicPath() {
        return idPicPath;
    }

    public void setIdPicPath(String idPicPath) {
        this.idPicPath = idPicPath;
    }

    public String getWorkPicPath() {
        return workPicPath;
    }

    public void setWorkPicPath(String workPicPath) {
        this.workPicPath = workPicPath;
    }

    public Integer getAge() {
        Date date = new Date(String.valueOf(this.getBirthday()));
        Long timeStamp = System.currentTimeMillis();//获取当前时间戳
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy");//设置格式
        String oldyears = sdf2.format(new Date(Long.parseLong(String.valueOf(date.getTime()))));//获取出生年份
        String newyears = sdf2.format(new Date(Long.parseLong(String.valueOf(timeStamp))));//获取出生年份
        Integer oldyear = Integer.valueOf(oldyears);
        Integer newyear = Integer.valueOf(newyears);
        return newyear-oldyear;
    }


}
